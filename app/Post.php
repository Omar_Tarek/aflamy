<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'operator_id',
        'product_id' ,
        'show_date'  ,
        'active',
        'featured'
    ];

    public function operator()
    {
        return $this->belongsTo('App\Operator');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\User;
use App\Notify;
use App\Msisdn;
use App\AdvertisingUrl;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Route;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\File;
use Artisan;
use thiagoalessio\TesseractOCR\TesseractOCR;

class HomeController extends Controller {

    public function test(Request $request) {

        $result = array();
        // get client ip
        $ip = $_SERVER["REMOTE_ADDR"];

        if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
            $ip = $_SERVER['HTTP_CLIENT_IP'];

        // HE

        if (isset($_SERVER['HTTP_MSISDN'])) {
            $MSISDN = str_replace("965", "", $_SERVER['HTTP_MSISDN']);
        } else {
            $MSISDN = "";
        }


        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $deviceModel = $_SERVER['HTTP_USER_AGENT'];
        } else {
            $deviceModel = "";
        }


        $country_from_ip = $this->ip_info("Visitor", "Country");
        $result['date'] = Carbon::now()->format('Y-m-d H:i:s');
        $result['ip'] = $ip;
        $result['country'] = $country_from_ip;
        $result['HeadeEnriched'] = $MSISDN;
        $result['deviceModel'] = $deviceModel;
        $result['AllHeaders'] = $_SERVER;

        // make log
        $actionName = "Hit page";
        $URL = $request->fullUrl();
        $parameters_arr = $result;
        $this->log($actionName, $URL, $parameters_arr);


        print_r($_SERVER);
        die;
    }

    public function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city" => @$ipdat->geoplugin_city,
                            "state" => @$ipdat->geoplugin_regionName,
                            "country" => @$ipdat->geoplugin_countryName,
                            "country_code" => @$ipdat->geoplugin_countryCode,
                            "continent" => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }

    public function HEDetect() {
        $arr = array();
        $HE = 0;
        // header inrichemnt DETECT
        if (isset($_SERVER['HTTP_MSISDN'])) {
            $MSISDN = str_replace("965", "", $_SERVER['HTTP_MSISDN']);
            $HE = 1;
        } else {
            $MSISDN = "";
        }

        $arr['HE'] = $HE;
        $arr['msisdn'] = $MSISDN;

        return json_encode($arr);
    }

    public function index(Request $request) {

        // clear files cashe
        Artisan::call('cache:clear');
        Artisan::call('view:clear');


        if (Session::has('MSISDN') && Session::get('Status') == 'active') {
            $MSISDN = Session::get('MSISDN');
            return redirect(url('front'))->with('MSISDN');
        }


        if ($request->input('MSISDN') != NULL) {
            $MSISDN = $request->input('MSISDN');
        } else {
            $MSISDN = "";
        }
        session::put('MSISDN', $MSISDN);



        date_default_timezone_set('Asia/Beirut');
        session::forget('message');
        // session::forget('adv_params');
        session::forget('transaction_id');
        session::forget('publisherId_macro');




        session::put('adv_params', $_SERVER['QUERY_STRING']);


        // make check on transaction_id ( clickid_macro ) for headwar ads company
        if (isset($_REQUEST['transaction_id']) && $_REQUEST['transaction_id'] != "") {
            $transaction_id = $_REQUEST['transaction_id'];
        } else {
            $transaction_id = "";
        }
        session::put('transaction_id', $transaction_id);


        if (isset($_REQUEST['affiliateId']) && $_REQUEST['affiliateId'] != "") {
            $affiliateId = $_REQUEST['affiliateId'];
        } else {
            $affiliateId = "";
        }
        session::put('affiliateId', $affiliateId);


        if (isset($_REQUEST['publisherId_macro']) && $_REQUEST['publisherId_macro'] != "") {
            $publisherId_macro = $_REQUEST['publisherId_macro'];
        } else {
            $publisherId_macro = "";
        }
        session::put('publisherId_macro', $publisherId_macro);


        // make log with all parameters
        $result = array();
        // get client ip
        $ip = $_SERVER["REMOTE_ADDR"];

        if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
            $ip = $_SERVER['HTTP_CLIENT_IP'];


        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $deviceModel = $_SERVER['HTTP_USER_AGENT'];
        } else {
            $deviceModel = "";
        }
        session::put('deviceModel', $deviceModel);

        $company = $this->detectCompnay();
        session::put('company', $company);




        $country_from_ip = $this->ip_info("Visitor", "Country");
        $result['date'] = Carbon::now()->format('Y-m-d H:i:s');
        $result['ip'] = $ip;
        $result['country'] = $country_from_ip;
        $result['HeadeEnriched'] = $MSISDN;
        $result['adsCompnayName'] = $company;
        $result['deviceModel'] = $deviceModel;
        $result['AllHeaders'] = $_SERVER;

        // var_dump($_SERVER) ; die;
        // make log
        $actionName = "Hit page";
        $URL = $request->fullUrl();
        $parameters_arr = $result;
        $this->log($actionName, $URL, $parameters_arr);
        session::put('fullUrl', $URL);


        $AdvertisingUrl = new AdvertisingUrl();
        $AdvertisingUrl->adv_url = session::get('adv_params');
        $AdvertisingUrl->transaction_id = $transaction_id;  // for Headway ads
        $AdvertisingUrl->publisherId_macro = $publisherId_macro;  // for Headway  ads
        $AdvertisingUrl->ads_compnay_name = $company;  //  intech  or headway
        $AdvertisingUrl->msisdn = $MSISDN;
        $AdvertisingUrl->operatorId = 50;  //  ooredoo kuwait
        $AdvertisingUrl->save();


        //   return view('home.index', compact('MSISDN'));

        return view('home.new_landing_template', compact('MSISDN'));
    }

    public function indexHE(Request $request, $MSISDN) {

        date_default_timezone_set('Asia/Beirut');
        session::forget('message');
        session::forget('adv_params');
        session::forget('transaction_id');
        session::forget('publisherId_macro');


        // setting all sessions
        session::put('adv_params', $_SERVER['QUERY_STRING']);

        // make check on transaction_id ( clickid_macro ) for headwar ads company
        if (isset($_REQUEST['transaction_id']) && $_REQUEST['transaction_id'] != "") {
            $transaction_id = $_REQUEST['transaction_id'];
            session::put('transaction_id', $transaction_id);
        } else {
            $transaction_id = "";
        }


        if (isset($_REQUEST['publisherId_macro']) && $_REQUEST['publisherId_macro'] != "") {
            $publisherId_macro = $_REQUEST['publisherId_macro'];
            session::put('publisherId_macro', $publisherId_macro);
        } else {
            $publisherId_macro = "";
        }


        // make log with all parameters
        $result = array();
        // get client ip
        $ip = $_SERVER["REMOTE_ADDR"];

        if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
            $ip = $_SERVER['HTTP_CLIENT_IP'];

        // HE
        /*
          if (isset($_SERVER['HTTP_MSISDN'])) {
          $MSISDN = str_replace("965", "", $_SERVER['HTTP_MSISDN']);

          } else {
          $MSISDN = "";
          }
         */

        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $deviceModel = $_SERVER['HTTP_USER_AGENT'];
        } else {
            $deviceModel = "";
        }


        $company = $this->detectCompnay();

        $country_from_ip = $this->ip_info("Visitor", "Country");
        $result['date'] = Carbon::now()->format('Y-m-d H:i:s');
        $result['ip'] = $ip;
        $result['country'] = $country_from_ip;
        $result['HeadeEnriched'] = $MSISDN;
        $result['adsCompnayName'] = $company;
        $result['deviceModel'] = $deviceModel;
        $result['AllHeaders'] = $_SERVER;

        // make log
        $actionName = "Hit page";
        $URL = $request->fullUrl();
        $parameters_arr = $result;
        $this->log($actionName, $URL, $parameters_arr);


        $AdvertisingUrl = new AdvertisingUrl();
        $AdvertisingUrl->adv_url = session::get('adv_params');
        $AdvertisingUrl->transaction_id = $transaction_id;  // for Headway ads
        $AdvertisingUrl->publisherId_macro = $publisherId_macro;  // for Headway  ads
        $AdvertisingUrl->ads_compnay_name = $company;  //  intech  or headway
        $AdvertisingUrl->save();


        return view('home.index', compact('MSISDN', 'request'));
    }

    public function log($actionName, $URL, $parameters_arr) {
        date_default_timezone_set("Africa/Cairo");
        $date = date("Y-m-d");
        $log = new Logger($actionName);
        // to create new folder with current date  // if folder is not found create new one
        if (!File::exists(storage_path('logs/' . $date . '/' . $actionName))) {
            File::makeDirectory(storage_path('logs/' . $date . '/' . $actionName), 0775, true, true);
        }

        $log->pushHandler(new StreamHandler(storage_path('logs/' . $date . '/' . $actionName . '/logFile.log', Logger::INFO)));
        $log->addInfo($URL, $parameters_arr);
    }

    public function subscribeZain(request $request) {

        session::forget('message');

        if (isset($_SERVER['HTTP_MSISDN'])) {
            $msisdn = $_SERVER['HTTP_MSISDN'];
        } else {
            $request->session()->flash('error', 'يجب الدخول من جوال زين داتا');
            return back();
        }

        return view('home.subscribeZain');
    }

    public function subscribeZainConfirm(request $request) {
        date_default_timezone_set("Africa/Cairo");

        if (isset($_SERVER['HTTP_MSISDN'])) {
            $msisdn = $_SERVER['HTTP_MSISDN'];
        } else {
            $request->session()->flash('error', 'يجب الدخول من جوال زين داتا');
            return back();
        }


        //  Zain Check Status Before Try To Subs
        $URL = "http://62.150.213.170:1001/ivasafasy/status.asp?mob=" . $msisdn;
        $result = preg_replace('/\s+/', '', file_get_contents($URL));

        // make log
        $company = $this->detectCompnay();
        $actionName = "Zain Check Status Before Try To Subs";
        $parameters_arr = array(
            'link' => $URL,
            'date' => Carbon::now()->format('Y-m-d H:i:s'),
            'company' => $company,
            'result' => (array) $result
        );
        $this->log($actionName, $URL, $parameters_arr);


        if ($result == "<html><body>Deletion" || $result == "<html><body>SubscriberNotFound") {  // initiate susbcription
            $URL = "http://62.150.213.170:1001/ivasafasy/sendmsg.asp?mob=" . $msisdn . "&mesg=A";
            $result = preg_replace('/\s+/', '', file_get_contents($URL));

            // make log
            $company = $this->detectCompnay();
            $actionName = "Zain Hit Url";
            $parameters_arr = array(
                'MSISDN' => $msisdn,
                'link' => $URL,
                'date' => Carbon::now()->format('Y-m-d H:i:s'),
                'company' => $company,
                'result' => (array) $result
            );
            $this->log($actionName, $URL, $parameters_arr);


            if ($result == "<html><body>Success") {   // response deliver to azeem
                // sleep 20 seconds to can delay request ... THEN check status
                //  sleep(20);

                /*
                  // Zain Check Status After Try To Subs
                  $URL = "http://62.150.213.170:1001/ivasafasy/status.asp?mob=" . $msisdn;
                  $result = preg_replace('/\s+/', '', file_get_contents($URL));

                  // make log
                  $company = $this->detectCompnay();
                  $actionName = "Zain Check Status After Try To Subs";
                  $parameters_arr = array(
                  'MSISDN' => $msisdn,
                  'link' => $URL,
                  'date' => Carbon::now()->format('Y-m-d H:i:s'),
                  'company' => $company,
                  'result' => (array)$result
                  );
                  $this->log($actionName, $URL, $parameters_arr);
                 */


                //  if ($result == "<html><body>Addition") {  // success billing
                session::flash('message', 'تم تسجيلك بنجاح في خدمة أفلامي');


                if ($company == "headway") {  // HEADWAY integration
                    // call Headway api to notify that msisdn is subscribe successfully
                    // https://lead.mobra.in/18020607a4a0700ab706ec07?token=7c97db9
                    $HeadWay_URL = "https://lead.mobra.in/" . session::get('transaction_id') . "?token=7c97db9";
                    $HeadWay_result = file_get_contents($HeadWay_URL);

                    if ($HeadWay_result != "Click already converted") {
                        $AdvertisingUrl = new AdvertisingUrl();
                        $AdvertisingUrl->adv_url = session::get('adv_params');
                        $AdvertisingUrl->msisdn = $msisdn;
                        $AdvertisingUrl->operatorId = 8;
                        $AdvertisingUrl->operatorName = "zain_kuwait";
                        $AdvertisingUrl->status = 1;  // subscribe success
                        $AdvertisingUrl->ads_compnay_name = $company;
                        if (session::get('publisherId_macro') !== NULL && session::get('publisherId_macro') != "") {
                            $AdvertisingUrl->publisherId_macro = session::get('publisherId_macro');
                            $AdvertisingUrl->transaction_id = session::get('transaction_id');
                        }
                        $AdvertisingUrl->save();

                        // make log
                        $company = $this->detectCompnay();
                        $actionName = "headway Zain Subscribe Success";
                        $URL = $HeadWay_URL;
                        $parameters_arr = array(
                            'MSISDN' => $msisdn,
                            'link' => $HeadWay_URL,
                            'date' => Carbon::now()->format('Y-m-d H:i:s'),
                            'result' => (array) $HeadWay_result
                        );
                        $this->log($actionName, $URL, $parameters_arr);
                    }
                } elseif ($company == "intech") {  // intech integration
                    // call intech  api to notify that msisdn is subscribe successfully
                    $ADV_URL = "http://ict.intech-mena.com/Universal/v1.0/UET?msisdn=" . $msisdn . "&operaterName=zain_kuwait&operatorId=8&" . session::get('adv_params');
                    $adv_result = file_get_contents($ADV_URL);
                    $adv_result = (array) json_decode($adv_result);


                    if ($adv_result['UET Offer Log'] == "SUCCESS") {
                        $AdvertisingUrl = new AdvertisingUrl();
                        $AdvertisingUrl->adv_url = session::get('adv_params');
                        $AdvertisingUrl->msisdn = $msisdn;
                        $AdvertisingUrl->operatorId = 8;
                        $AdvertisingUrl->operatorName = "zain_kuwait";
                        $AdvertisingUrl->status = 1;  // subscribe success
                        $AdvertisingUrl->ads_compnay_name = $company;
                        $AdvertisingUrl->save();

                        // make log
                        $company = $this->detectCompnay();
                        $actionName = "Intech Zain Subscribe Success";
                        $URL = $ADV_URL;
                        $parameters_arr = array(
                            'MSISDN' => $msisdn,
                            'link' => $ADV_URL,
                            'date' => Carbon::now()->format('Y-m-d H:i:s'),
                            'result' => $adv_result
                        );
                        $this->log($actionName, $URL, $parameters_arr);
                    }
                } elseif ($company == "DF") {
                    $AdvertisingUrl = new AdvertisingUrl();
                    $AdvertisingUrl->adv_url = NULL;
                    $AdvertisingUrl->msisdn = $msisdn;
                    $AdvertisingUrl->operatorId = 8;
                    $AdvertisingUrl->operatorName = "zain_kuwait";
                    $AdvertisingUrl->status = 1;  // subscribe success
                    $AdvertisingUrl->ads_compnay_name = $company;
                    $AdvertisingUrl->save();

                    // make log
                    $company = $this->detectCompnay();
                    $actionName = "DF Zain Subscribe Success";
                    $URL = $request->fullUrl();
                    $parameters_arr = array(
                        'MSISDN' => $msisdn,
                        'date' => Carbon::now()->format('Y-m-d H:i:s'),
                    );
                    $this->log($actionName, $URL, $parameters_arr);
                } else {
                    session::flash('message', "حدث خطأ");
                }


                // msisdn last update
                $Msisdn = Msisdn::where('msisdn', '=', $msisdn)->orderBy('id', 'DESC')->first();
                if ($Msisdn) {
                    $Msisdn->operator_id = 8;  // zain
                    $Msisdn->ads_ur_id = $AdvertisingUrl->id;
                    $Msisdn->ad_company = $company;
                    if (session::get('transaction_id') !== NULL && session::get('transaction_id') != "") {
                        $Msisdn->transaction_id = session::get('transaction_id');
                    }
                    $Msisdn->final_status = 1;
                    $Msisdn->save();
                } else {
                    $Msisdn = new Msisdn();
                    $Msisdn->msisdn = $msisdn;
                    $Msisdn->operator_id = 8;  // zain
                    $Msisdn->ads_ur_id = $AdvertisingUrl->id;
                    $Msisdn->ad_company = $company;
                    if (session::get('transaction_id') !== NULL && session::get('transaction_id') != "") {
                        $Msisdn->transaction_id = session::get('transaction_id');
                    }
                    $Msisdn->final_status = 1;
                    $Msisdn->save();
                }


                /* } else {
                  session::flash('message', "حدث خطأ");
                  } */
            } else {
                session::flash('message', "حدث خطأ");
            }
        } elseif ($result == "<html><body>Addition") {
            session::flash('message', 'انت مشترك بالفعل');
        } else {
            session::flash('message', 'حدث خطأ');
        }


        return view('home.subscribeZainConfirmation', compact('msisdn'));
    }

    public function detectCompnay() {
        $company = "";
        if (session::get('adv_params') != "") {
            if (session::get('publisherId_macro') != "" && session::get('transaction_id') != "") {
                $company = "headway";
            } elseif (session::get('affiliateId') != "" && session::get('affiliateId') != "") {

                $company = "intech";
            } else {
                $company = "DF";
            }
        } else {
            $company = "DF";
        }

        return $company;
    }

    public function unsubZain() {
        // header inrichemnt for zain kuwait
        session::forget('message');
        if (isset($_SERVER['HTTP_MSISDN'])) {
            $MSISDN = str_replace("965", "", $_SERVER['HTTP_MSISDN']);
        } else {
            $MSISDN = "";
        }


        return view('home.unsubZain', compact('MSISDN'));
    }

    public function unsubscribeZain(request $request) {
        date_default_timezone_set("Africa/Cairo");
        if ($request->input('number') !== NULL) {
            $msisdn = "965" . $request->input('number');
        } else {
            $msisdn = "";
        }


        if (!preg_match('/^[0-9]{8}$/', $request->input('number'))) {

            $request->session()->flash('error', 'هذا الرقم غير صحيح');
            return redirect('');
        }

        $URL = "http://62.150.213.170:1001/ivasafasy/sendmsg.asp?mob=" . $msisdn . "&mesg=UA";
        $result = preg_replace('/\s+/', '', file_get_contents($URL));

        if ($result == "<html><body>Success") {  //   InvalidAccess   or   Success
            //  Zain Check Status AFTER unsub
            sleep(20);
            $URL = "http://62.150.213.170:1001/ivasafasy/status.asp?mob=" . $msisdn;
            $result = preg_replace('/\s+/', '', file_get_contents($URL));


            // make log
            $company = $this->detectCompnay();
            $actionName = "Zain check status after unsub";
            $parameters_arr = array(
                'MSISDN' => $msisdn,
                'link' => $URL,
                'date' => Carbon::now()->format('Y-m-d H:i:s'),
                'result' => (array) $result
            );
            $this->log($actionName, $URL, $parameters_arr);


            if ($result == "<html><body>Deletion") {  // User unsubscribe from service.
                $status = "Success";
                session::flash('message', 'تم الغاء الخدمه');
            } else {
                $status = "Success";
                session::flash('message', "سوف يتم الغاء الخدمة");
            }
        } else {
            $status = "Error";
            session::flash('message', "حدث خطأ");
        }


        // make log
        $actionName = "Zain UnSubscribe";
        $parameters_arr = array(
            'MSISDN' => $msisdn,
            'Status' => $status,
            'date' => Carbon::now()->format('Y-m-d H:i:s'),
            'result' => (array) $result
        );
        $this->log($actionName, $URL, $parameters_arr);


        // msisdn last update
        $Msisdn = Msisdn::where('msisdn', '=', $msisdn)->where('operator_id', '=', 8)->orderBy('id', 'DESC')->first();
        if ($Msisdn) {
            $Msisdn->final_status = 0;
            $Msisdn->save();
        }


        return view('home.unsubZainResult', compact('msisdn'));
    }

    public function subscribeOreedo(request $request) {
        $msisdn = $request->input('number');


        session::forget('message');
        if (isset($_SERVER['HTTP_MSISDN'])) {
            $msisdn = str_replace("965", "", $_SERVER['HTTP_MSISDN']);
        } else {
            $msisdn = $request->input('number');
        }


        if (!preg_match('/^[0-9]{8}$/', $msisdn)) {
            $request->session()->flash('error', 'هذا الرقم غير صحيح');
            return back();
        }


        $Msisdn = Msisdn::where('msisdn', '=', "965" . $msisdn)->where('final_status', '=', 1)->where('operator_id', '=', 50)->orderBy('id', 'DESC')->first();
        if ($Msisdn) {
            //  session::flash('message', "انت مشترك بالفعل ");
            //  return view('home.subscribeOreedoConfirmation', compact('msisdn'));
            //  $request->session()->flash('success', 'انت مشترك بالفعل يرجي الضغط علي دخول');
            //  return redirect(url("/login?msisdn=" . $msisdn));


            session(['MSISDN' => $msisdn, 'Status' => 'active', 'operator_id' => OOREDOO_OP_ID]);
            return redirect(url("front?operator_id=" . OOREDOO_op_id_database));
        } else {
            return view('home.subscribeOreedo', compact('msisdn'));
        }
    }

    public function subscribeOreedoConfirm(request $request) {
        date_default_timezone_set("Africa/Cairo");
        session::forget('message');
        $msisdn = $request->input('number');
        $msisdn = "965" . $msisdn;

        $Msisdn = Msisdn::where('msisdn', '=', $msisdn)->where('final_status', '=', 1)->orderBy('id', 'DESC')->first();
        if ($Msisdn) {
            session::flash('message', "انت مشترك بالفعل ");
            return view('home.subscribeOreedoConfirmation', compact('msisdn'));
        }


        $company = $this->detectCompnay();
        // $URL = "http://ikwm-appvas.isys.mobi:2009/OoredooConsentInit/Consent.aspx?MSISDN=" . $msisdn . "&Scode=1526&ServiceID=S-IvsVidEwMY2&ServiceType=P-tQX2zvEwMY2&AdsCompany=" . $company;  // old Ooredoo CG

        $ClientId = "1935";
        $UserId = "O@IV";
        $Password = "O@iv@s";
        $TranId = time();

        $URL = "http://ikwm-appvas.isys.mobi:2025/consentrequest.aspx?MSISDN=" . $msisdn . "&Scode=1526&ServiceId=S-IvsVidEwMY2&ClientId=$ClientId&UserId=$UserId&Password=$Password&TranId=$TranId";


        http://ikwm-appvas.isys.mobi:2025/consentrequest.aspx?MSISDN=&Scode=&ServiceId=&ClientId=&UserId=&Password=&TranId=
        // make log
        $actionName = "Oreedo Subscribe Track";
        $parameters_arr = array(
            'MSISDN' => $msisdn,
            'date' => Carbon::now()->format('Y-m-d H:i:s'),
            'company' => $company
        );
        $this->log($actionName, $URL, $parameters_arr);


        // insert log in our database
        $AdvertisingUrl = new AdvertisingUrl();
        $AdvertisingUrl->adv_url = session::get('adv_params');
        $AdvertisingUrl->msisdn = $msisdn;
        $AdvertisingUrl->operatorId = 50;
        $AdvertisingUrl->operatorName = "ooredoo_kuwait";
        $AdvertisingUrl->ads_compnay_name = $company;
        if (session::get('publisherId_macro') !== NULL && session::get('publisherId_macro') != "") {
            $AdvertisingUrl->publisherId_macro = session::get('publisherId_macro');
            $AdvertisingUrl->transaction_id = session::get('transaction_id');
        }
        $AdvertisingUrl->save();


        // msisdn last update
        $Msisdn = Msisdn::where('msisdn', '=', $msisdn)->orderBy('id', 'DESC')->first();
        if ($Msisdn) {
            $Msisdn->operator_id = 50;  // ooredoo
            $Msisdn->ads_ur_id = $AdvertisingUrl->id;
            $Msisdn->ad_company = $company;
            if (session::get('transaction_id') !== NULL && session::get('transaction_id') != "") {
                $Msisdn->transaction_id = session::get('transaction_id');
            }
            $Msisdn->save();
        } else {
            $Msisdn = new Msisdn();
            $Msisdn->msisdn = $msisdn;
            $Msisdn->operator_id = 50; // ooredoo
            $Msisdn->ads_ur_id = $AdvertisingUrl->id;
            $Msisdn->ad_company = $company;
            if (session::get('transaction_id') !== NULL && session::get('transaction_id') != "") {
                $Msisdn->transaction_id = session::get('transaction_id');
            }
            $Msisdn->save();
        }


        return redirect($URL);
    }

    public function unsubOroodo() {
        session::forget('message');
        if (isset($_REQUEST['msisdn'])) {
            $msisdn = $_REQUEST['msisdn'];
        } else {
            $msisdn = "";
        }
        return view('home.unsubOroodo', compact('msisdn'));
    }

    public function unsubscribeOoredoo(request $request) {
        date_default_timezone_set("Africa/Cairo");
        session::forget('message');
        $msisdn = $request->input('number');
        $msisdn = "965" . $msisdn;

        if (!preg_match('/^[0-9]{8}$/', $request->input('number'))) {
            //   session::flash('message', 'هذا الرقم غير صحيح');
            $request->session()->flash('error', 'هذا الرقم غير صحيح');
            return redirect('subscribe_landing');
        }


        $Msisdn = Msisdn::where('msisdn', '=', $msisdn)->orderBy('id', 'DESC')->first();
        if (!$Msisdn) {
            $status = "Error";
            session::flash('message', 'هذا الرقم غير مسجل بالخدمة');
        }


        // $URL = "http://dev.ivashosting.com/landing/soup_api/ooreoo_kuwait_unsub.php?MSISDN=" . $msisdn;
        $URL = url("http://dev.ivashosting.com/landing_aflamy/soup_api/ooreoo_kuwait_unsub.php?MSISDN=" . $msisdn);

        $result = preg_replace('/\s+/', '', file_get_contents($URL));

        if ($result == "S0") {
            $status = "Success";
            session::flash('message', 'تم الغاء الخدمه');

            // update msisdn
            $Msisdn = Msisdn::where('msisdn', '=', $msisdn)->orderBy('id', 'DESC')->first();
            if ($Msisdn) {
                $Msisdn->final_status = 0;
                $Msisdn->save();
                // clear login
                Session::forget('MSISDN');
                Session::forget('Status');
            } else {
                $status = "Error";
                session::flash('message', 'هذا الرقم غير مسجل بالخدمة');
            }
        } elseif ($result == "NotEligible") {
            $status = "Error";
            session::flash('message', 'يجب ادخال رقم اودريو صحيح');
        } else {
            $status = "Error";
            session::flash('message', "حدث خطأ");
        }


        // make log
        $actionName = "Oreedo UnSubscribe";
        $parameters_arr = array(
            'MSISDN' => $msisdn,
            'Status' => $status,
            'date' => Carbon::now()->format('Y-m-d H:i:s'),
            'result' => (array) $result
        );
        $this->log($actionName, $URL, $parameters_arr);

        return view('home.unsubOroodoResult', compact('msisdn'));
    }

    // http://localhost/landing_laravel/redirect?msisdn=96565867860&mnc=102&opsid=2&mnc=102&action=1&status=S21
    public function redirect(request $request) {

        date_default_timezone_set("Africa/Cairo");
        session::forget('message');
        // fixed paramters
        $msisdn = $request->input('msisdn');
        $mnc = $request->input('mnc');
        $opsid = $request->input('opsid');
        // changable parameters
        $action = $request->input('action');
        $status = $request->input('status');
        $ad_company = $request->input('AdsCompany');


        /* Teja and Azeem final states for Oreedo :
          - action 2  :   SS, SP, SF -- subscription success, pending, failedaction
          - action 3 :  RF, RS Renewal Success and Renewal FailedAction
          - action 4 : U. unsubi

          - action 1 :
          // S21 - ConsentStatus=ConsentSuccess&BillingStatus=SUCCESS

          Error Codes in case of issue for =[ConsentStatus AND BillingStatus]
          //E11 - ConsentStatus=Invalid MSISDN
          //E12 - ConsentStatus=Issue in getting Operator side Consent.Please contact One Global Team
          //E13 - ConsentStatus= Service Not Configured. Please contact One Global Team
          //E14 - ConsentStatus = MSISDN Not Eligible
          //E15 - ConsentStatus= Missing Parameters
          //E16 - ConsentStatus = Unexpected Issue.Please contact One Global Team
          // E22 - ConsentStatus=ConsentSuccess & BillingStatus = FAILED
          //  E23 - ConsentStatus=ConsentSuccess&BillingStatus=Subscription Incomplete
          //  E24 - ConsentStatus=ConsentFailure
          //  E25 - ConsentStatus=Improper Consent

         */
        // renew
        if (isset($msisdn) && !is_null($msisdn) && isset($status) && !is_null($status)) {
            // to can see all history for the same number So we remove this check
            $notify = new Notify();
            $notify->complete_url = \Request::fullUrl();
            $notify->msisdn = $msisdn;
            $notify->action = $action;
            $notify->status = $status;
            $notify->save();


            // msisdn last update
            $Msisdn = Msisdn::where('msisdn', '=', $msisdn)->orderBy('id', 'DESC')->first();
            if ($Msisdn) {
                $Msisdn->operator_id = 50;  // ooredoo
                $Msisdn->ooredoo_notify_id = $notify->id;
                if ($action == "2" && $status == "SS" || ($action == "3" && $status == "RS") || ($action == "1" && $status == "S21")) {  // subscribe success or  renew success
                    $Msisdn->final_status = 1;
                    $Msisdn->save();
                    $ad_company = $Msisdn->ad_company;
                } elseif ($action == "4" && $status == "U") {  // unsubscribe notification
                    $Msisdn->final_status = 0;
                    $Msisdn->save();
                }
            } else {
                if (isset($ad_company)) {
                    $ad_company = $request->input('AdsCompany');
                } else {
                    $ad_company = "DF";
                }

                if ($action == "3" && $status == "RS") {
                    // create new msisdn for old msisdn
                    $Msisdn = new Msisdn();
                    $Msisdn->msisdn = $msisdn;
                    $Msisdn->operator_id = 50; // ooredoo
                    $Msisdn->ooredoo_notify_id = $notify->id;
                    $Msisdn->final_status = 1;
                    $Msisdn->ad_company = $ad_company;
                    $Msisdn->save();
                } elseif ($action == "1" && $status == "S21") {   // initail subscribe
                    // create new msisdn for old msisdn
                    $Msisdn = new Msisdn();
                    $Msisdn->msisdn = $msisdn;
                    $Msisdn->operator_id = 50; // ooredoo
                    $Msisdn->ooredoo_notify_id = $notify->id;
                    $Msisdn->final_status = 1;
                    $Msisdn->ad_company = $ad_company;
                    $Msisdn->save();
                }
            }
        } else {
            $msisdn = "";
            $message = "رقم غير صحيح";
        }

        // subscribe for the first time
        if ($action == "2" && $status == "SS" && $msisdn != "") {

            $message = "تم الاشتراك بنجاح ";   //  SS
            // make log for only SS ( subscription success )
            $actionName = "Oreedo Subscription Success";
            $URL = \Request::fullUrl();
            $parameters_arr = array(
                'action' => $action,
                'status' => $status,
                'msisdn' => $msisdn,
                'link' => $URL,
                'date' => Carbon::now()->format('Y-m-d H:i:s'),
                'company' => $ad_company
            );
            $this->log($actionName, $URL, $parameters_arr);

            // STOP here on notify advertising companies with subscribe success
            // msisdn last update
            $Msisdn = Msisdn::where('msisdn', '=', $msisdn)->orderBy('id', 'DESC')->first();
            if ($Msisdn) {
                $ad_company = $Msisdn->ad_company;
                $transaction_id = $Msisdn->transaction_id;
                $ads_ur_id = $Msisdn->ads_ur_id;

                $AdvertisingUrlOld = AdvertisingUrl::findOrFail($ads_ur_id);


                if ($ad_company == "headway") {  // HEADWAY integration
                    // call Headway api to notify that msisdn is subscribe successfully
                    // https://lead.mobra.in/18020607a4a0700ab706ec07?token=7c97db9
                    $HeadWay_URL = "https://lead.mobra.in/" . $transaction_id . "?token=7c97db9";
                    $HeadWay_result = file_get_contents($HeadWay_URL);

                    if ($HeadWay_result != "Click already converted") {
                        $AdvertisingUrl = new AdvertisingUrl();
                        $AdvertisingUrl->adv_url = $AdvertisingUrlOld->adv_url;
                        $AdvertisingUrl->msisdn = $msisdn;
                        $AdvertisingUrl->operatorId = 50;
                        $AdvertisingUrl->operatorName = "ooredoo_kuwait";
                        $AdvertisingUrl->status = 1;  // subscribe success
                        $AdvertisingUrl->ads_compnay_name = $ad_company;
                        $AdvertisingUrl->publisherId_macro = $AdvertisingUrlOld->publisherId_macro;
                        $AdvertisingUrl->transaction_id = $transaction_id;

                        $AdvertisingUrl->save();

                        // make log
                        $company = $this->detectCompnay();
                        $actionName = "headway Ooredoo Subscribe success";
                        $URL = $HeadWay_URL;
                        $parameters_arr = array(
                            'MSISDN' => $msisdn,
                            'link' => $HeadWay_URL,
                            'date' => Carbon::now()->format('Y-m-d H:i:s'),
                            'result' => (array) $HeadWay_result
                        );
                        $this->log($actionName, $URL, $parameters_arr);
                    }
                } elseif ($ad_company == "intech") {  // intech integration
                    // call intech  api to notify that msisdn is subscribe successfully
                    // new link :   http://ict.intech-mena.com/Universal/v2.0/API/Postback
                    // old link : http://ict.intech-mena.com/Universal/v1.0/UET
                    $ADV_URL = "http://ict.intech-mena.com/Universal/v1.0/UET?msisdn=" . $msisdn . "&operaterName=ooredoo_kuwait&operatorId=50&" . $AdvertisingUrlOld->adv_url;
                    $adv_result = file_get_contents($ADV_URL);
                    $adv_result = (array) json_decode($adv_result);


                    if ($adv_result['UET Offer Log'] == "SUCCESS") {
                        $AdvertisingUrl = new AdvertisingUrl();
                        $AdvertisingUrl->adv_url = $AdvertisingUrlOld->adv_url;
                        $AdvertisingUrl->msisdn = $msisdn;
                        $AdvertisingUrl->operatorId = 50;
                        $AdvertisingUrl->operatorName = "ooredoo_kuwait";
                        $AdvertisingUrl->status = 1;  // subscribe success
                        $AdvertisingUrl->ads_compnay_name = $ad_company;
                        $AdvertisingUrl->save();

                        // make log
                        $company = $this->detectCompnay();
                        $actionName = "Intech Ooredoo Subscribe Success";
                        $URL = $ADV_URL;
                        $parameters_arr = array(
                            'MSISDN' => $msisdn,
                            'link' => $ADV_URL,
                            'date' => Carbon::now()->format('Y-m-d H:i:s'),
                            'result' => $adv_result
                        );
                        $this->log($actionName, $URL, $parameters_arr);
                    } else { // to see not Recorded for intech
                        // make log
                        $company = $this->detectCompnay();
                        $actionName = "Intech Ooredoo Subscribe Not Recorded";
                        $URL = $ADV_URL;
                        $parameters_arr = array(
                            'MSISDN' => $msisdn,
                            'link' => $ADV_URL,
                            'date' => Carbon::now()->format('Y-m-d H:i:s'),
                            'result' => $adv_result
                        );
                        $this->log($actionName, $URL, $parameters_arr);
                    }
                } elseif ($ad_company == "DF") {
                    $AdvertisingUrl = new AdvertisingUrl();
                    $AdvertisingUrl->adv_url = "";
                    $AdvertisingUrl->msisdn = $msisdn;
                    $AdvertisingUrl->operatorId = 50;
                    $AdvertisingUrl->operatorName = "ooredoo_kuwait";
                    $AdvertisingUrl->status = 1;  // subscribe success
                    $AdvertisingUrl->ads_compnay_name = $ad_company;
                    $AdvertisingUrl->save();

                    // make log
                    $company = $this->detectCompnay();
                    $actionName = "DF Ooredoo Subscribe Success";
                    $URL = $request->fullUrl();
                    $parameters_arr = array(
                        'MSISDN' => $msisdn,
                        'date' => Carbon::now()->format('Y-m-d H:i:s'),
                    );
                    $this->log($actionName, $URL, $parameters_arr);
                }
            } else {
                $ad_company = "DF";
            }
        } elseif ($action == "2" && $status == "SP") {
            $message = "susbcription  Pending";
        } elseif ($action == "2" && $status == "SF") {
            $message = "susbcription  Failed";
        } elseif ($action == "3" && $status == "RS") {
            $message = "Renewal Success";
        } elseif ($action == "3" && $status == "RF") {
            $message = "Renewal  Failed";
        } elseif ($action == "4" && $status == "U") {
            $message = "Unsubscription  Success";
        } elseif ($action == "1" && $status == "E11") {
            $message = "Invalid MSISDN";
        } elseif ($action == "1" && $status == "E12") {
            $message = "Issue in getting Operator side Consent";
        } elseif ($action == "1" && $status == "E13") {
            $message = "Service Not Configured";
        } elseif ($action == "1" && $status == "E14") {
            $message = "من فضلك ادخل رقم اوريدو صحيح";
        } elseif ($action == "1" && $status == "S21" && $msisdn != "") {
            // $message = "تم تسجيلك بنجاح في خدمة أفلامي";
            $request->session()->flash('success', 'تم تسجيلك بنجاح في خدمة أفلامي');
            $msisdn = str_replace("965", "", $request->input('msisdn'));
            return redirect(url("/login?msisdn=" . $msisdn));
        } else {
            $message = "حدث خطأ";
            $ad_company = "DF";
        }


        // make log for all Notification api
        $actionName = "Oreedo Notification Api";
        $URL = \Request::fullUrl();
        $parameters_arr = array(
            'action' => $action,
            'status' => $status,
            'msisdn' => $msisdn,
            'link' => $URL,
            'date' => Carbon::now()->format('Y-m-d H:i:s'),
            'company' => $ad_company
        );
        $this->log($actionName, $URL, $parameters_arr);


        \Session::flash('message', $message);
        return view('home.subscribeOreedoConfirmation', compact('msisdn'));
    }

    public function checkOoredooNumber(request $request) {
        $arr = array();
        $msisdn = $request->input('msisdn');

        $Msisdn = Msisdn::where('msisdn', '=', $msisdn)->where('final_status', '=', 1)->orderBy('id', 'DESC')->first();
        if ($Msisdn)
            $status = 1;
        else
            $status = 0;

        $arr['statue'] = $status;
        $arr['msisdn'] = $msisdn;

        return json_encode($arr);
    }

    public function header(Request $request) {
        //  Zain Check Status Before Try To Subs
        /* $URL = "http://ivascloud.com/header/";
          $result =  file_get_contents($URL);
          var_dump($result); */
        $service = $request['service'];



        return view('home.header', compact('service'));
    }

    // ============= zain jordo integration ==============================//


    public function zainjordan(Request $request) {

        // clear files cashe
        Artisan::call('cache:clear');
        Artisan::call('view:clear');


        if (Session::has('MSISDN') && Session::get('Status') == 'active') {
            $MSISDN = Session::get('MSISDN');
            return redirect(url('front'))->with('MSISDN');
        }


        if ($request->input('MSISDN') != NULL) {
            $MSISDN = $request->input('MSISDN');
        } else {
            $MSISDN = "";
        }
        session::put('MSISDN', $MSISDN);



        date_default_timezone_set('Asia/Beirut');
        session::forget('message');
        // session::forget('adv_params');
        session::forget('transaction_id');
        session::forget('publisherId_macro');




        session::put('adv_params', $_SERVER['QUERY_STRING']);


        // make check on transaction_id ( clickid_macro ) for headwar ads company
        if (isset($_REQUEST['transaction_id']) && $_REQUEST['transaction_id'] != "") {
            $transaction_id = $_REQUEST['transaction_id'];
        } else {
            $transaction_id = "";
        }
        session::put('transaction_id', $transaction_id);


        if (isset($_REQUEST['affiliateId']) && $_REQUEST['affiliateId'] != "") {
            $affiliateId = $_REQUEST['affiliateId'];
        } else {
            $affiliateId = "";
        }
        session::put('affiliateId', $affiliateId);


        if (isset($_REQUEST['publisherId_macro']) && $_REQUEST['publisherId_macro'] != "") {
            $publisherId_macro = $_REQUEST['publisherId_macro'];
        } else {
            $publisherId_macro = "";
        }
        session::put('publisherId_macro', $publisherId_macro);


        // make log with all parameters
        $result = array();
        // get client ip
        $ip = $_SERVER["REMOTE_ADDR"];

        if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
            $ip = $_SERVER['HTTP_CLIENT_IP'];


        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $deviceModel = $_SERVER['HTTP_USER_AGENT'];
        } else {
            $deviceModel = "";
        }
        session::put('deviceModel', $deviceModel);

        $company = $this->detectCompnay();
        session::put('company', $company);




        $country_from_ip = $this->ip_info("Visitor", "Country");
        $result['date'] = Carbon::now()->format('Y-m-d H:i:s');
        $result['ip'] = $ip;
        $result['country'] = $country_from_ip;
        $result['HeadeEnriched'] = $MSISDN;
        $result['adsCompnayName'] = $company;
        $result['deviceModel'] = $deviceModel;
        $result['AllHeaders'] = $_SERVER;

        // var_dump($_SERVER) ; die;
        // make log
        $actionName = "Hit page";
        $URL = $request->fullUrl();
        $parameters_arr = $result;
        $this->log($actionName, $URL, $parameters_arr);
        session::put('fullUrl', $URL);


        $AdvertisingUrl = new AdvertisingUrl();
        $AdvertisingUrl->adv_url = session::get('adv_params');
        $AdvertisingUrl->transaction_id = $transaction_id;  // for Headway ads
        $AdvertisingUrl->publisherId_macro = $publisherId_macro;  // for Headway  ads
        $AdvertisingUrl->ads_compnay_name = $company;  //  intech  or headway
        $AdvertisingUrl->msisdn = $MSISDN;
        $AdvertisingUrl->operatorId = 50;  //  ooredoo kuwait
        $AdvertisingUrl->save();


        //   return view('home.index', compact('MSISDN'));

        return view('home.zainjo_landing', compact('MSISDN'));
    }

    public function subscribeZainJordon(request $request) {
        $msisdn = $request->input('number');


        session::forget('message');
        if (isset($_SERVER['HTTP_MSISDN'])) {
            $msisdn = str_replace("962", "", $_SERVER['HTTP_MSISDN']);
        } else {
            $msisdn = $request->input('number');
        }


        if (!preg_match('/^[0-9]{9}$/', $msisdn)) {
            $request->session()->flash('error', 'هذا الرقم غير صحيح');
            return back();
        }


        $Msisdn = Msisdn::where('msisdn', '=', "962" . $msisdn)->where('final_status', '=', 1)->where('operator_id', '=', 8)->orderBy('id', 'DESC')->first();
        if ($Msisdn) {
            session(['MSISDN' => $msisdn, 'Status' => 'active', 'operator_id' => ZAIN_JORDON_OP_ID]);
            return redirect(url("front?operator_id=3"));
        } else {
            return view('home.subscribeZainJordon', compact('msisdn'));
        }
    }

    public function subscribeZainJoConfirm(request $request) {
        date_default_timezone_set("Africa/Cairo");
        $msisdn = $request->input('number');
        session::forget('message');
        if (isset($_SERVER['HTTP_MSISDN'])) {
            $msisdn = str_replace("962", "", $_SERVER['HTTP_MSISDN']);
        } else {
            $msisdn = $request->input('number');
        }


        if (!preg_match('/^[0-9]{9}$/', $msisdn)) {
            // session::flash('message', "رقم الجوال غير صحيح");
            $request->session()->flash('error', 'هذا الرقم غير صحيح');
            return back();
        }


        $Msisdn = Msisdn::where('msisdn', '=', "962" . $msisdn)->where('final_status', '=', 1)->where('operator_id', '=', 8)->orderBy('id', 'DESC')->first();
        if ($Msisdn) {
            session(['MSISDN' => $msisdn, 'Status' => 'active', 'operator_id' => ZAIN_JORDON_OP_ID]);
            return redirect(url("front?operator_id=3"));
        } else {
            // send pincode
            $result = $this->sendPinCode("962" . $msisdn);  // comment for test

            if ($result == "Pincode success") {
                return view('home.pinCode', compact('msisdn'));
            } else {
                $request->session()->flash('error', 'حدث خطأ');
                return back();
            }
        }
    }

    public function sendPinCode($msisdn) {
        $res = "";
        set_time_limit(1000000000000);
        $dataToComputeHash = username . "username" . password . "password" . $msisdn . "msisdn" . service . "service";
        $hash = strtoupper(hash_hmac('sha256', $dataToComputeHash, decryptedOriginal));

        $JSON = '{
            "input": {

             "username": "' . username . '",
             "password": "' . password . '",
             "service": "' . service . '",
             "authkey": "' . authkey . '",
             "hash": "' . $hash . '",
             "msisdn": "' . $msisdn . '",
             "language": "en",
             "template": "subscription"
         }
     }';



        $URL = 'isysapp.com/dob/api/One/pin';
        $ReqResponse = $this->SendRequest($URL, $JSON);
        $result = json_decode($ReqResponse);

        // make log
        $actionName = "ZainJordon_pinCode";
        $URL = url() . "/subscribeZainJoConfirm";
        $parameters_arr = (array) $result;
        $this->log($actionName, $URL, $parameters_arr);


        if ($result->status->code == 0) {
            $res = "Pincode success";
        } else {
            $res = "Pincode fail";
        }

        return $res;
    }

    public function SendRequest($sUrl, $xml) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sUrl);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('POST', 'Content-Type: application/json; charset=utf-8'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $sOutput = curl_exec($ch);
        curl_close($ch);

        return $sOutput;
    }

    public function subscribeZainJordonPincodeConfirm(request $request) {
        $pincode = $request->input('pincode');
        $msisdn = $request->input('msisdn');

        date_default_timezone_set("Africa/Cairo");
        $msisdn = $request->input('msisdn');

        if (!preg_match('/^[0-9]{9}$/', $msisdn)) {
            $request->session()->flash('error', 'هذا الرقم غير صحيح');
            return view('home.pinCode', compact('msisdn'));
        }

        $company = $this->detectCompnay();
        $result = $this->verifyPincode("962" . $msisdn, $pincode);

        if ($result == "Subscribe success") {
            // create new msisdn

            $Msisdn = Msisdn::where('msisdn', '=', "962" . $msisdn)->orderBy('id', 'DESC')->first();
            if ($Msisdn) {

            } else {
                $Msisdn = new Msisdn();
            }
            $Msisdn->msisdn = "962" . $msisdn;
            $Msisdn->operator_id = 8;
            $Msisdn->pincode = $pincode;
            $Msisdn->final_status = 1;
            $Msisdn->ad_company = $company;
            $Msisdn->save();


            // adds
            if ($company == "headway") {  // HEADWAY integration
                // call Headway api to notify that msisdn is subscribe successfully
                // https://lead.mobra.in/18020607a4a0700ab706ec07?token=7c97db9
                $HeadWay_URL = "https://lead.mobra.in/" . session::get('transaction_id') . "?token=7c97db9";
                $HeadWay_result = file_get_contents($HeadWay_URL);

                if ($HeadWay_result != "Click already converted") {
                    $AdvertisingUrl = new AdvertisingUrl();
                    $AdvertisingUrl->adv_url = session::get('adv_params');
                    $AdvertisingUrl->msisdn = ZAIN_JORDON_COUNTRY_PREFIX . $msisdn;
                    $AdvertisingUrl->operatorId = 8;
                    $AdvertisingUrl->operatorName = "zain_kuwait";
                    $AdvertisingUrl->status = 1;  // subscribe success
                    $AdvertisingUrl->ads_compnay_name = $company;
                    if (session::get('publisherId_macro') !== NULL && session::get('publisherId_macro') != "") {
                        $AdvertisingUrl->publisherId_macro = session::get('publisherId_macro');
                        $AdvertisingUrl->transaction_id = session::get('transaction_id');
                    }
                    $AdvertisingUrl->save();

                    // make log
                    $company = $this->detectCompnay();
                    $actionName = "headway Zain Jordon Subscribe Success";
                    $URL = $HeadWay_URL;
                    $parameters_arr = array(
                        'MSISDN' => ZAIN_JORDON_COUNTRY_PREFIX . $msisdn,
                        'link' => $HeadWay_URL,
                        'date' => Carbon::now()->format('Y-m-d H:i:s'),
                        'result' => (array) $HeadWay_result
                    );
                    $this->log($actionName, $URL, $parameters_arr);
                }
            } elseif ($company == "intech") {  // intech integration
                // call intech  api to notify that msisdn is subscribe successfully
                $ADV_URL = "http://ict.intech-mena.com/Universal/v1.0/UET?msisdn=" . ZAIN_JORDON_COUNTRY_PREFIX . $msisdn . "&operaterName=zain_jordan&operatorId=8&" . session::get('adv_params');
                $adv_result = file_get_contents($ADV_URL);
                $adv_result = (array) json_decode($adv_result);


                if ($adv_result['UET Offer Log'] == "SUCCESS") {
                    $AdvertisingUrl = new AdvertisingUrl();
                    $AdvertisingUrl->adv_url = session::get('adv_params');
                    $AdvertisingUrl->msisdn = ZAIN_JORDON_COUNTRY_PREFIX . $msisdn;
                    $AdvertisingUrl->operatorId = 8;
                    $AdvertisingUrl->operatorName = "zain_jordon";
                    $AdvertisingUrl->status = 1;  // subscribe success
                    $AdvertisingUrl->ads_compnay_name = $company;
                    $AdvertisingUrl->save();

                    // make log
                    $company = $this->detectCompnay();
                    $actionName = "Intech Zain jordon  Subscribe Success";
                    $URL = $ADV_URL;
                    $parameters_arr = array(
                        'MSISDN' => ZAIN_JORDON_COUNTRY_PREFIX . $msisdn,
                        'link' => $ADV_URL,
                        'date' => Carbon::now()->format('Y-m-d H:i:s'),
                        'result' => $adv_result
                    );
                    $this->log($actionName, $URL, $parameters_arr);
                }
            } elseif ($company == "DF") {
                $AdvertisingUrl = new AdvertisingUrl();
                $AdvertisingUrl->adv_url = NULL;
                $AdvertisingUrl->msisdn = ZAIN_JORDON_COUNTRY_PREFIX . $msisdn;
                $AdvertisingUrl->operatorId = 8;
                $AdvertisingUrl->operatorName = "zain_jordon";
                $AdvertisingUrl->status = 1;  // subscribe success
                $AdvertisingUrl->ads_compnay_name = $company;
                $AdvertisingUrl->save();

                // make log
                $company = $this->detectCompnay();
                $actionName = "DF Zain jordon Subscribe Success";
                $URL = $request->fullUrl();
                $parameters_arr = array(
                    'MSISDN' => ZAIN_JORDON_COUNTRY_PREFIX . $msisdn,
                    'date' => Carbon::now()->format('Y-m-d H:i:s'),
                );
                $this->log($actionName, $URL, $parameters_arr);
            } else {
                session::flash('message', "حدث خطأ");
            }

            //   $request->session()->flash('success', 'تم الاشتراك بنجاح');
            session(['MSISDN' => $msisdn, 'Status' => 'active', 'operator' => ZAIN_JORDON_OP_ID]);
            return redirect(url("front?operator_id=3"));
        } elseif ($result == "INSUFFICIENT_FUNDS") {
            $request->session()->flash('failed', 'رصيدك غير كافي');
            return view('home.pinCode', compact('msisdn'));
        } else {
            $request->session()->flash('failed', 'الكود الذي ادخلته غير صحيح.حاول مرة اخري');
            return view('home.pinCode', compact('msisdn'));
        }
    }

    public function verifyPincode($msisdn, $pincode) {
        $res = "";
        set_time_limit(1000000000000);
        $dataToComputeHash = username . "username" . password . "password" . $msisdn . "msisdn" . service . "service";
        $hash = strtoupper(hash_hmac('sha256', $dataToComputeHash, decryptedOriginal));

        $JSON = '{
            "input": {
               "username": "' . username . '",
               "password": "' . password . '",
               "service": "' . service . '",
               "authkey": "' . authkey . '",
               "hash": "' . $hash . '",
               "msisdn": "' . $msisdn . '",
               "pin": "' . $pincode . '",
               "trial": "100"
           }
       }';


        $URL = 'isysapp.com/dob/api/one/CreateSubscription';
        $ReqResponse = $this->SendRequest($URL, $JSON);
        $result = json_decode($ReqResponse);

        // make log
        $actionName = "ZainJordon_pinCodeVerify";
        $URL = url() . "/subscribeZainJordonPincodeConfirm";
        $arr = (array) $result;
        $arr['JSON'] = $JSON;
        $parameters_arr = $arr;
        $this->log($actionName, $URL, $parameters_arr);

        if ($result->status->code == 0 && $result->response->result == "CHARGED") {
            $res = "Subscribe success";
            // send success message with portal link
            $this->sendMessage($msisdn);
        } elseif ($result->response->result == "INSUFFICIENT_FUNDS") {
            $res = "INSUFFICIENT_FUNDS";
        } else {
            $res = "Subscribe fail";
        }

        return $res;
    }

    public function sendMessage($msisdn) {
        $res = "";
        set_time_limit(1000000000000);
        $dataToComputeHash = username . "username" . password . "password" . $msisdn . "msisdn" . service . "service";
        $hash = strtoupper(hash_hmac('sha256', $dataToComputeHash, decryptedOriginal));

        $smstext = "To start enjoying Aflamy, visit: ";
        $smstext .= url() . "/zainjordan";
        $smstext .= " للاستمتاع بمحتوى افلامي يرجى الضغط على الرابط أدناه ";
        $smstext .= url() . "/zainjordan";

        $trackid = $this->randomNumber(10);

        $JSON = '{
            "input": {

             "username": "' . username . '",
             "password": "' . password . '",
             "service": "' . service . '",
             "authkey": "' . authkey . '",
             "hash": "' . $hash . '",
             "msisdn": "' . $msisdn . '",
             "trackid": "' . $trackid . '",
             "smstext": "' . $smstext . '"
         }
     }';


        $URL = 'isysapp.com/dob/api/one/sendsms';
        $ReqResponse = $this->SendRequest($URL, $JSON);
        $result = json_decode($ReqResponse);


        // make log
        $actionName = "ZainJordon_sendMessage";
        $URL = url() . "/subscribeZainSendMessage";
        $parameters_arr = (array) $result;
        $this->log($actionName, $URL, $parameters_arr);
    }

    public function randomNumber($length) {
        $result = '';

        for ($i = 0; $i < $length; $i++) {
            $result .= mt_rand(0, 9);
        }

        return $result;
    }

    public function unsubZainJordan() {
        session::forget('message');
        if (isset($_REQUEST['msisdn'])) {
            $msisdn = $_REQUEST['msisdn'];
        } else {
            $msisdn = "";
        }
        return view('home.unsubZainJordan', compact('msisdn'));
    }

    public function unsubscribeZainJordanAction(request $request) {
        date_default_timezone_set("Africa/Cairo");
        session::forget('message');
        $msisdn = $request->input('number');
        $msisdn = "962" . $msisdn;

        if (!preg_match('/^[0-9]{9}$/', $request->input('number'))) {
            //   session::flash('message', 'هذا الرقم غير صحيح');
            $request->session()->flash('error', 'هذا الرقم غير صحيح');
            return redirect('subscribe_landing');
        }


        $Msisdn = Msisdn::where('msisdn', '=', $msisdn)->orderBy('id', 'DESC')->first();
        if (!$Msisdn) {
            $status = "Error";
            session::flash('message', 'هذا الرقم غير مسجل بالخدمة');
        } else {
            // delete api
            $result = $this->zain_jordon_delete_subscription($msisdn);

            if ($result == "delete success") {
                $Msisdn->final_status = 0;
                $Msisdn->save();

                $status = "Success";
                session::flash('message', 'تم الغاء الخدمه');
                session::forget('MSISDN');
                session::forget('active');
                session::forget('operator_id');
            } else {
                $status = "Error";
                session::flash('message', 'حدث خطأ');
            }
        }
        return view('home.unsubZainJoResult', compact('msisdn'));
    }

    public function zain_jordon_delete_subscription($msisdn) {
        $res = "";
        set_time_limit(1000000000000);
        $dataToComputeHash = username . "username" . password . "password" . $msisdn . "msisdn" . service . "service";
        $hash = strtoupper(hash_hmac('sha256', $dataToComputeHash, decryptedOriginal));


        $JSON = '{
        "input": {

            "username": "' . username . '",
            "password": "' . password . '",
            "service": "' . service . '",
            "authkey": "' . authkey . '",
            "hash": "' . $hash . '",
            "msisdn": "' . $msisdn . '"
        }
    }';

        $URL = 'isysapp.com/dob/api/One/DeleteSubscription';
        $ReqResponse = $this->SendRequest($URL, $JSON);
        $result = json_decode($ReqResponse);

        // make log
        $actionName = "ZainJordon_delete_subscription";
        $URL = url() . "/zain_jordon_delete_subscription";
        $parameters_arr = (array) $result;
        $this->log($actionName, $URL, $parameters_arr);


        if ($result->status->code == 0) {
            $res = "delete success";
        } else {
            $res = "delete fail";
        }

        return $res;
    }

// http://localhost/aflamy/zainJordonNotification?MSISDN=962795877070&NotificationType=1&Status=success
    public function zainJordonNotification(request $request) {

        date_default_timezone_set("Africa/Cairo");

        /*  2018-11-27
          string Username // empty
          string Password // empty
          string ServiceID // empty
          int OperatorID // empty
          string ActivityTime // charged timestamp
          string Description // transaction message if any
          string ExpiryTime // NextPaymentTime or next billing time
          string MSISDN // user mobile number
          long NotificationType // 1 for subscription. 2 for unsubscription
          string NotificationTypeDes // subscription or unsubscription
          string ShortCode // empty
          string Status // "success" if successfully billed or "Fail"
          string TransactionID // transaction id if any
          string isFreePeriod // false

         */
        $URL = \Request::fullUrl();

        $parameters_arr = array(
            'link' => $URL,
            'date' => Carbon::now()->format('Y-m-d H:i:s')
        );



        $parameters_arr['post_parameters'] = $request->all();

        // wanted paramters
        $Username = $request->input('Username');
        $Password = $request->input('Password');
        $ServiceID = $request->input('ServiceID');
        $Description = $request->input('Description');

        if ($Username == username && $Password == password && $ServiceID == service) {

        } else {
            $result = array();
            $result['status'] = "Fail";
            $result['message'] = "Auth Failed";
            return Response(array('result' => $result));
        }


        $msisdn = $request->input('MSISDN');
        $NotificationType = $request->input('NotificationType');  // // 1 for subscription. 2 for unsubscription
        $Status = $request->input('Status');  // // "success" if successfully billed or "Fail"

        if ($NotificationType == 1 && $Status == "success") {
            $action = "sub";
        } elseif ($NotificationType == 2) {
            $action = "usub";
        } else {
            $action = "usub";
        }



        // log for all history
        $actionName = "Zain_jordon_notification_url";
        $this->log($actionName, $URL, $parameters_arr);





        if (isset($msisdn) && !is_null($msisdn) && isset($NotificationType) && !is_null($NotificationType) && isset($Status) && !is_null($Status)) {

            //  zain jordon notify = all notification history
            $notify = new Notify();
            $notify->complete_url = \Request::fullUrl();
            $notify->msisdn = $msisdn;
            $notify->action = $action;
            $notify->status = $Status;
            $notify->save();


            // update msisdn status
            $Msisdn = Msisdn::where('msisdn', '=', $msisdn)->orderBy('id', 'DESC')->first();
            if ($Msisdn) {
                if ($NotificationType == 1 && $Status == "success") {  // subscription success
                    $Msisdn->final_status = 1;
                } elseif ($NotificationType == 2) { //  unsubscription
                    $Msisdn->final_status = 0;
                }
                $Msisdn->save();
            } else {

                $Msisdn = new Msisdn();
                $Msisdn->msisdn = $msisdn;
                $Msisdn->operator_id = 8; // zain jordan
                $Msisdn->ad_company = "SC"; // new subscribe by short code
                if ($NotificationType == 1 && $Status == "success") {  // subscription success
                    $Msisdn->final_status = 1;
                } elseif ($NotificationType == 2) { //  unsubscription
                    $Msisdn->final_status = 0;
                }
                $Msisdn->save();
            }
        }

        $result = array();
        $result['status'] = "Success";
        $result['type'] = "Zain_jordon_notification_url";
        $result['url'] = $URL;

        return Response(array('result' => $result));
    }

    public function test22() {

        echo (new TesseractOCR('text.png'))->run();
    }

    public function genernalLogin(request $request) {
        $msisdn = "12345678";
        session(['MSISDN' => $msisdn, 'Status' => 'active', 'operator_id' => OP_ID_General]);
        return redirect(url("front?operator_id=" . OP_ID_General));
    }

}

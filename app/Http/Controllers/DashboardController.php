<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Operator;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Artisan;
class DashboardController extends Controller {

    protected $databases_base_path;

    public function __construct() {
        $this->databases_base_path = base_path() . "/database/backups/";
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $products = count(Product::all());
        $brands = count(Brand::all());
        $operators = count(Operator::all());
        return view('dashboard.index', compact('products', 'brands', 'operators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function download_backup(Request $request) {
        $file = $this->databases_base_path . $request['path'];
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($file) . '"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            readfile($file);
            exit;
        }
    }

    public function list_backups() {
        $path = $this->file_build_path("database", "backups");
        if (!file_exists($path))
            mkdir($path);
        $files = scandir($path);
        $databases = array();
        foreach ($files as $file)
            if (strpos($file, ".sql"))
                array_push($databases, $file);

        $full_path = $this->databases_base_path;
        $full_path = str_replace("\\", "/", $full_path);
        return view('dashboard.list_backups', compact('databases', 'full_path'));
    }

    public function delete_backup(Request $request) {
        $path = $this->databases_base_path . $request['path'];
        if (file_exists($path))
            unlink($path);
        \Session::flash('success', 'Back up deleted');
        return back();
    }

    public function clear_cache() {
        Artisan::call('cache:clear');
        Artisan::call('view:clear');
        \Session::flash('success', 'Cashe Cleared successfully');
        return redirect('dashboard');
    }

}

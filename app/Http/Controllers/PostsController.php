<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Operator;
use App\Post;
use App\Product;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DataTables;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PostsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('posts.post');
    }

    public function allData(Request $request)
    {

        if ($request->productID) {
            $posts = Post::with('product')->with('operator')->where('product_id', $request->productID)->selectRaw('posts.*,REPLACE(REPLACE(active, "1", "Yes") , "0", "No") AS active,REPLACE(REPLACE(featured, "1", "Yes") , "0", "No") AS featured')->get();
        } else {
            $posts = Post::with('product')->with('operator')->selectRaw('posts.*,REPLACE(REPLACE(active, "1", "Yes") , "0", "No") AS active,REPLACE(REPLACE(featured, "1", "Yes") , "0", "No") AS featured')->get();
        }

        return DataTables::of($posts)
            ->editColumn('active', '@if($active == "Yes")
                                    <span class="label label-success">{{$active}}</span>
                                @else
                                    <span class="label label-danger">{{$active}}</span>
                                @endif')
            ->editColumn('featured', '@if($featured == "Yes")
                                    <span class="label label-success">{{$featured}}</span>
                                @else
                                    <span class="label label-danger">{{$featured}}</span>
                                @endif')
            ->addColumn('product', function (Post $post) {
                return $post->product->title;
            })
            ->addColumn('operator', function (Post $post) {
                return $post->operator->name;
            })
            ->addColumn('link', ' <input type="text" value="{{url("get_product?product_id=".$product_id."&operator_id=".$operator_id)}}" />')
            ->addColumn('action', function (Post $post) {
                return view('posts.post_partial', compact('post'))->render();
            })
            ->escapeColumns([])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        for ($i = 0; $i < count($request['operator_id']); $i++) {
            $post = new Post($request->all());
            $post['operator_id'] = $request['operator_id'][$i];
            $post['show_date'] = date('Y-m-d', strtotime($request['show_date']));
            /* $check = Post::where('operator_id',$request['operator_id'][$i])->where('product_id',$request['product_id'])->get();
              if (count($check)>0)
              {
              \Session::flash('failed','post already exists');
              }
              else{ */
            \Session::flash('success', 'post added successfully');
            $post->save();
            /* } */
        }
        return redirect('posts');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        $operators = Operator::all();
        return view('posts.addpost', compact('product', 'operators'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $products = Product::pluck('title', 'id');
        $operators = Operator::pluck('name', 'id');
        return view('posts.editpost', compact('post', 'operators', 'products'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        $oldPost = Post::findOrFail($id);
        $newPost = $request->all();

        /*   if ($oldPost->operator_id != $newPost['operator_id'] || $oldPost->product_id != $newPost['product_id'])
          {
          $check = Post::where('operator_id',$request['operator_id'])->where('product_id',$request['product_id'])->get();
          if (count($check)>0)
          {
          \Session::flash('failed','post already exists');
          return redirect('posts');
          }
          } */
        $newPost['show_date'] = date('Y-m-d', strtotime($request['show_date']));
        $oldPost->update($newPost);
        \Session::flash('success', 'post updated successfully');
        return redirect('posts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Post::destroy($id);
        \Session::flash('success', 'post deleted successfully');
        return redirect('posts');
    }

    public function changeDateFormate($date)
    {
        $newFormat = "";
        if ($date[2] == '/' || $date[2] == '-') { // ya3ny low el format ele gai kda 01/01/2017  fana hab2a me7tag a8ayar el format lel format bta3 el DB ele hoa kda 2017/01/01
            $date = explode('/', $date); // ba3d ma ba3ml explode bey return array kda [ 01 , 01 , 2017 ]
            $newFormat .= $date[2] . "-";
            $newFormat .= $date[0] . "-";
            $newFormat .= $date[1];
        } else { // kda el format gai mazboot 2017-01-01
            $newFormat = $date;
        }
        return $newFormat;
    }

}

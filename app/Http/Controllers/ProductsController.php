<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Product;
use Illuminate\Http\Request;
use Datatables;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class ProductsController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $products = Product::all();
        $categories = Category::where('category_name', 'id');
        $brands = Brand::where('brand_name', 'id');
        return view('products.listproducts', compact('products', 'categories', 'brands'));
    }

    public function allData() {
        $products = Product::with('category')->with('brand')->get();
        return Datatables::of($products)
                       // ->editColumn('title', '<a href="{{url("products/".$id."/posts")}}">{{$title}}</a>')
                        ->editColumn('product_image', '<img src="{{$product_image}}" class="img-circle" width="160px" height="160px">')
                        ->addColumn('category', function(Product $products) {
                            return $products->category->category_name;
                        })
                        ->addColumn('brand', function(Product $products) {
                            return $products->brand->brand_name;
                        })
                        ->addColumn('action', function(Product $product) {
                            return view('products.product_partial', compact('product'))->render();
                        })
                        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $categories = Category::pluck('category_name', 'id');
        $brands = Brand::pluck('brand_name', 'id');
        return view('products.product', compact('categories', 'brands'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRequest $request) {
        $product = new Product($request->all());

        $file = $request->file('product_image');
        $destinationFolder = "products_images/";
        $uniqID = uniqid();
        $product['product_image'] = $destinationFolder . $uniqID . "." . $file->getClientOriginalExtension();
        $file->move($destinationFolder, $uniqID . "." . $file->getClientOriginalExtension());

        $video = $request->file('video');
        $destinationFolder = "videos/";
        $uniqID = uniqid();
        $product['video'] = $destinationFolder . $uniqID . "." . $video->getClientOriginalExtension();
        $video->move($destinationFolder, $uniqID . "." . $video->getClientOriginalExtension());

        \Session::flash('success', 'Product added successfully');
        $product->save();
        return redirect('products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $product = Product::findOrFail($id);
        $categories = Category::pluck('category_name', 'id');
        $brands = Brand::pluck('brand_name', 'id');
        return view('products.editproduct', compact('product', 'categories', 'brands'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRequest $request, $id) {
        $check = Product::where('title', $request['title'])->where('id', '!=', $id)->get();

		/*
        if (count($check) > 0) {
            \Session::flash('failed', 'this product already exists');
            return redirect('products');
        }
		*/
        $imgExtensions = array("png", "jpeg", "jpg");
        $vidExtensions = array("mp4", "wmv", "avi", "flv", "webm");
        $newProduct = $request->all();
        $oldProduct = Product::findOrFail($id);
        $destinationFolder = "products_images/";
        if ($request->hasFile('product_image')) {
            $file = $request->file('product_image');
            if (!in_array($file->getClientOriginalExtension(), $imgExtensions)) {
                \Session::flash('failed', 'Image must be jpg, png, or jpeg only !! No updates takes place, try again with that extensions please..');
                return redirect('products');
            }
            $uniqueID = uniqid();
            $file->move($destinationFolder, $uniqueID . "." . $file->getClientOriginalExtension());
            $newProduct['product_image'] = $destinationFolder . $uniqueID . "." . $file->getClientOriginalExtension();
            if (file_exists($oldProduct['product_image'])) {
                Storage::delete($oldProduct['product_image']);
            }
        } else {
            $newProduct['product_image'] = $oldProduct['product_image'];
        }

        if ($request->hasFile('video')) {
            $destinationFolder = "videos/";
            $file = $request->file('video');
            if (!in_array($file->getClientOriginalExtension(), $vidExtensions)) {
                \Session::flash('failed', 'Video must be mp4, wemp, flv or wmv !! No updates takes place, try again with that extensions please..');
                return redirect('products');
            }
            $uniqueID = uniqid();
            $file->move($destinationFolder, $uniqueID . "." . $file->getClientOriginalExtension());
            $newProduct['video'] = $destinationFolder . $uniqueID . "." . $file->getClientOriginalExtension();
            if (file_exists($oldProduct['video'])) {
                Storage::delete($oldProduct['video']);
            }
        } else {
            $newProduct['video'] = $oldProduct['video'];
        }

        $oldProduct->update($newProduct);
        \Session::flash('success', 'Product Updated successfully');
        return redirect('products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $product = Product::find($id);
        if (file_exists($product['product_image']))
            Storage::delete($product['product_image']);
        if (file_exists($product['video']))
            Storage::delete($product['video']);
        Product::destroy($id);
        \Session::flash('success', 'post deleted successfully');
        return redirect('products');
    }

    public function restore($id) {
        $product = Product::onlyTrashed()->find($id);
        $product->restore();
        \Session::flash('success', 'Product Restored');
        return redirect('products');
    }

    public function deleteForever($id) {
        $product = Product::onlyTrashed()->find($id);
        Storage::delete($product['product_image']);
        $product->forceDelete();
        \Session::flash('success', 'Product Deleted');
        return redirect('products');
    }

    public function get_posts($id) {
        $product = Product::findOrFail($id);
        return view('posts.post', compact('id'));
    }

}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ads ; 
use App\Operator ; 


class AdsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ads = Ads::all() ; 
        return view('ads.index',compact('ads')) ; 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ad = NULL ;
        $operators = Operator::all() ; 
        return view('ads.input',compact('ad','operators')) ; 
    }

    public function is_url($uri){
        if(preg_match( '/^(http|https):\\/\\/[a-z0-9]+([\\-\\.]{1}[a-z0-9]+)*\\.[a-z]{2,5}'.'((:[0-9]{1,5})?\\/.*)?$/i' ,$uri)){
          return $uri;
        }
        else{
            return false;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ad = new Ads($request->all()) ; 
        $destination_folder = "ads" ;  

        $filename=time(); //Uniqe Id 
        $imgurl=$request->file('image');
        $destinationPath='ads/';
        $extension = $imgurl->getClientOriginalExtension();    
        $ad->image=$destinationPath.$filename.".".$extension;

        $date = $request['published_date'] ; 
        $date = date('Y-m-d',strtotime($date)) ;  
        $ad->published_date = $date ; 

        $url = $request['link'] ;  
        $check = $this->is_url($url) ; 
        if(! $check) 
        {
            \Session::flash('failed','Link must be in http/https format ex: http://www.ivas.mobi ');
            return back() ; 
        }

        $ad->save() ; 
        $imgurl->move($destinationPath,$filename.".".$extension);

        \Session::flash('success','Ad added successfully');
        return redirect('advertisments') ; 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ad = Ads::findOrFail($id) ;
        $operators = Operator::all() ; 
        return view('ads.input',compact('ad','operators')) ; 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $oldAd = Ads::findOrFail($id) ; 

        $newAd = $request->all()  ; 
        $destination_folder = "ads" ; 
        
        if($request->hasFile('image'))
        {
            $filename=time(); //Uniqe Id 
            $imgurl=$request->file('image');
            $destinationPath='ads/';
            $extension = $imgurl->getClientOriginalExtension();    
            $newAd['image'] = $destinationPath.$filename.".".$extension;
             
            $imgurl->move($destinationPath,$filename.".".$extension);

            if(file_exists($oldAd->image))
                unlink($oldAd->image) ; 
        }
        $date = $request['published_date'] ; 
        $date = date('Y-m-d',strtotime($date)) ;  
        $newAd['published_date'] = $date ; 

        $oldAd->update($newAd) ; 

        \Session::flash('success','Ad updated successfully');
        return redirect('advertisments') ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ad = Ads::findOrFail($id) ; 
        if(file_exists($ad->image))
            unlink($ad->image) ; 
        
        $ad->delete() ;
        \Session::flash('success','Ad deleted successfully');
        return back() ;
    }
}

<?php

namespace App\Http\Controllers\API;

use App\Brand;
use App\Post;
use App\Product;
use App\Setting;
use Carbon\Carbon;
use Hamcrest\Core\Set;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Category;
use App\Operator;
use Illuminate\Support\Facades\DB;
use App\Ads;

class CategoryController extends Controller
{
    private $_PAGINATION =  8 ;
    private $FRONT_TESTING =  0 ; 
    private $sign , $date, $active_flag , $active_sign ;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $settings = Setting::where('key','LIKE','%front test%')->first() ; 
        if($settings)
            $this->FRONT_TESTING = $settings->value;  

        if($this->FRONT_TESTING=="true")
        {
            $this->FRONT_TESTING = 1; 
            $this->sign = ">=" ; 
            $this->date = 0 ;
            $this->active_flag = 0 ;
            $this->active_sign = ">=" ;  
        }
        else {
            $this->FRONT_TESTING = 0 ; 
            $this->sign = "<=" ; 
            $this->date = Carbon::now()->format("Y-m-d")  ; 
            $this->active_flag = 1 ; 
            $this->active_sign = "=" ;  
        } 
    }

    public function get_latest_featured_for_slider(Request $request)
    {
        if (!empty($request['operator_id']))
        {
            try {
                $operator = Operator::findOrFail($request->input('operator_id'));
                $products = $operator->featured()->orderBy('posts.updated_at', 'DESC')->where('posts.show_date',$this->sign,$this->date)->where('posts.active',$this->active_sign,$this->active_flag)->get()->take(3);  // here we list products BUT you make orderBt('updated_at', 'DESC')  :  this order in products table but we want to order in posts table
                $counter =   count($products) ;
                for($i = 0 ; $i < $counter ; $i++)
                {
                    $category = Category::where('id',$products[$i]->category_id)->first();
                    $products[$i]->category_name = $category->category_name ;
                    $products[$i]->category_id = $category->id ;

                    $brand = Brand::where('id',$products[$i]->brand_id)->first() ;
                    $products[$i]->brand_name = $brand->brand_name ;
                    $products[$i]->brand_id = $brand->id ;

                    $products[$i]->operator_id = $products[$i]->pivot->operator_id;
                    unset($products[$i]->pivot);
                }

            } catch (ModelNotFoundException $e) {
                return [] ;
            }
        }
        else{ // here we list posts and we get posts regardless of operator id  SO we reset operator_id = null
            if($this->FRONT_TESTING)
            {
                $products = Post::where('featured',1)->where('show_date',$this->sign,$this->date)->where('active',$this->active_sign,$this->active_flag)->orderBy('updated_at','DESC')->get()->take(3) ;
                foreach ($products as $product)
                {
                    $category = Category::where('id',$product->product->category->id)->first() ;
                    $product->category_name = $category->category_name ;
                    $product->category_id = $category->id ;
    
                    $brand = Brand::where('id',$product->product->brand->id)->first() ;
                    $product->brand_name = $brand->brand_name ;
                    $product->brand_id = $brand->id ;
                    $product->product_image = $product->product->product_image ;
                    $product->product_id = $product->product->id ;  // as we list posts not products
                    $product->operator_id = null ;
                    $product->title = $product->product->title ;
                    unset($product->product); // remove useless data
                }
            }
            else 
                $products = [] ; 
        }

        return $products ;
    }

    public function get_all_categories_with_latest(Request $request)
    {
        $operator = null ;
        if (!empty($request['operator_id'])) {
            try {
                $operator = Operator::findOrFail($request->input('operator_id'));
            } catch (ModelNotFoundException $e) {
                return [] ;
            }
            $categories = Category::all();  
            
            $categories->each(function ($category) use ($operator) {
                $category->products = $category->products()->whereIn('products.id',$operator->products()->get()->pluck('id'))->orderBy('products.created_at','DESC')->take(4)->get();
            });           
        }
        else{
            $categories = Category::all();
            
            $categories->each(function ($category) {
                $category->products = $category->products()->orderBy('created_at', 'DESC')->take(4)->get();
            });
        }         
         
        $result = array() ;  
     //   $x = 0 ;  // emad : there is no need for this
        $counter = count($categories) ; 
        for($i = 0 ; $i< $counter ; $i++)
        {
            if (count($categories[$i]['products'])==0)
            { // to remove category if it not has any products
                unset($categories[$i]);
            }
            else{
                $product = $categories[$i]['products'] ; // each category products
                $checker = false ; 
                
                for($j = 0 ; $j < count($product) ; $j++)
                {
                    $brand = $product[$j]->brand_id ;
                    
                    // to get show_date from posts BUT this not needed 
                    if ($operator != null)
                    {
                        $post = Post::where('product_id',$product[$j]->id)
                        ->where('operator_id','=',$operator->id)
                        ->where('show_date',$this->sign,$this->date)
                        ->where('active',$this->active_sign,$this->active_flag)
                        ->first() ;

                        if($post)
                        {
                            $categories[$i]['products'][$j]['brand_name'] = Brand::find($brand)->brand_name ;  
                            $categories[$i]['products'][$j]['show_date'] = $post['show_date'] ; 
                            $categories[$i]['products'][$j]['operator_id'] = $operator->id ; 
                            $categories[$i]['products'][$j]['loop'] = $i ;  
                        } 
                        else {
                            continue ;
                        }
                    }
                    else{
                        if($this->FRONT_TESTING)
                        { 
                            $categories[$i]['products'][$j]['brand_name'] = Brand::find($brand)->brand_name ;  
                            $categories[$i]['products'][$j]['operator_id'] = null ;
                        }
                        else{
                            continue ;
                        }
                    } 
                    $checker = true ;   
                }
                if($checker) // low kan kammel el loops kda hoa mada5alsh fel continue , kda yeb2a fe data la2aha fel categories w el posts (products)
                    array_push($result,$categories[$i]) ;                 
            }
        }

        return $result ;
    }

//    public function get_categories_by_brand(Request $request)
//    {
//        try{
//            if (!empty($request['operator_id']))
//                $sql = "SELECT DISTINCT categories.id , categories.*,brands.id AS brand_id, brands.brand_name,products.*, products.id AS product_id FROM posts
//                    JOIN products ON posts.product_id = products.id
//                    JOIN brands ON products.brand_id = brands.id
//                    JOIN categories ON products.category_id = categories.id
//                    WHERE posts.active = 1 AND posts.show_date <= '".Carbon::now()->format("Y-m-d")."' AND posts.operator_id = ".$request['operator_id']. " AND brands.id=".$request['brand_id'] ;
//            else
//                $sql = "SELECT DISTINCT categories.id , categories.*,brands.id AS brand_id, brands.brand_name FROM products
//                    JOIN brands ON products.brand_id = brands.id
//                    JOIN categories ON products.category_id = categories.id
//                    WHERE brands.id=".$request['brand_id'] ;
//            $brands = \DB::select($sql) ;
//            if (count($brands)>0)
//                return $brands ;
//            else
//                return response(json_encode("No result found"));
//        }catch (ModelNotFoundException $e) {
//            return response()->json("Not Found brands", 200);
//        }
//    }
// ,$this->sign,$this->date)->where('active',1)
    public function get_products_by_brandV3(Request $request)
    {
        if (empty($request['brand_id']))
        {
            return -1 ;
        }
        if (!empty($request['operator_id']))
            $sql = "SELECT DISTINCT products.id AS product_id, products.* ,brands.brand_name AS brand_name, brands.id AS brand_id,categories.id AS category_id , 
                    categories.category_name AS category_name , operators.name AS operator_name, operators.id AS operator_id,products.title AS prod_title
                    FROM posts
                    JOIN products ON posts.product_id = products.id
                    JOIN brands ON products.brand_id = brands.id
                    JOIN operators ON posts.operator_id = operators.id 
                    JOIN categories ON products.category_id = categories.id
                    WHERE posts.active $this->active_sign $this->active_flag AND posts.show_date $this->sign '".$this->date."' AND posts.operator_id = ".$request['operator_id']. " AND brands.id=".$request['brand_id'] ;
        else
        {
            if($this->FRONT_TESTING)
                $sql = "SELECT DISTINCT products.id AS product_id,brands.brand_name AS brand_name, brands.id AS brand_id, products.*,categories.id AS category_id , 
                        categories.category_name AS category_name,products.title AS prod_title 
                        FROM products
                        JOIN brands ON products.brand_id = brands.id
                        JOIN categories ON products.category_id = categories.id
                        WHERE brands.id=".$request['brand_id'] ;
            else 
                $sql = NULL; 
        }
        if($sql)
            $products = \DB::select($sql) ;
        else return -1 ;
        if (empty($products))
            return -1 ;
        return  $products ;
    }

    public function get_products_by_categories_V2(Request $request)
    {

        if ($request['operator_id']&&$request['category_id'])
        {
            $sql = "SELECT products.updated_at AS update_date, products.product_image AS product_image, brands.brand_name AS brand_name, 
                    brands.id AS brand_id, categories.category_name AS category_name, products.id AS product_id,posts.show_date,products.title AS prod_title
                    FROM posts 
                    JOIN products ON posts.product_id=products.id 
                    JOIN operators ON posts.operator_id = operators.id 
                    JOIN brands ON products.brand_id=brands.id 
                    JOIN categories ON products.category_id = categories.id
                    WHERE posts.operator_id=".$request['operator_id']." 
                    AND products.category_id=".$request['category_id']."
                    AND posts.show_date $this->sign '".$this->date."' 
                    AND posts.active $this->active_sign $this->active_flag " ;
        }
        elseif ($request['category_id'] && $this->FRONT_TESTING){
            $sql = "SELECT products.updated_at AS update_date, products.product_image AS product_image, brands.brand_name AS brand_name,
                    brands.id AS brand_id, categories.category_name AS category_name , products.id AS product_id,products.title AS prod_title
                    FROM products 
                    JOIN brands ON products.brand_id=brands.id
                    JOIN categories ON products.category_id = categories.id
                    WHERE products.category_id=".$request['category_id'] ;
        }
        else{
            return -1 ;
        }
        $products = \DB::select($sql);
        foreach ($products as $product)
        {
            if (isset($request['operator_id']))
                $product->operator_id = $request['operator_id'] ;
            else
                $product->operator_id = null ;
        }
        return $products ;
    }


    public function get_products_by_categories(Request $request)
    {
        // dd($request->input('category_id'));
        try {
            $operator = Operator::findOrFail($request->input('operator_id'));
        } catch (ModelNotFoundException $e) {
            return [] ;
        }

        try {
            $category = Category::findOrFail($request->input('category_id'));
        } catch (ModelNotFoundException $e) {
            return [];
        }

        $products = $category->products()->paginate($this->_PAGINATION) ;
        $filteredProducts = array();
        $x = 0 ;
        for($i = 0 ; $i< count($products) ; $i++)
        {
            $post = null ;
            $post = Post::where('product_id',$products[$i]->id)->where('operator_id',$operator->id)->where("show_date",$this->sign,$this->date)->where('active',$this->active_sign,$this->active_flag)->get() ;
            if (count($post)>0)
            {
                $filteredProducts[$x] = $products[$i] ;
                $filteredProducts[$x]['brand_name'] = Brand::find($products[$i]->brand_id)->brand_name ;
                $filteredProducts[$x]['category_name'] = Category::find($products[$i]->category_id)->category_name ;
                $x++ ;
            }
        }

        return response(json_encode($filteredProducts));

    }


    public function get_products_by_brand(Request $request)
    {
        // dd($request->input('category_id'));
        try {
            $operator = Operator::findOrFail($request->input('operator_id'));
        } catch (ModelNotFoundException $e) {
            return [] ;
        }

        try {
            $brand = Brand::findOrFail($request->input('brand_id'));
        } catch (ModelNotFoundException $e) {
            return [] ;
        }

        try {
            $category = Category::findOrFail($request->input('category_id'));
        } catch (ModelNotFoundException $e) {
            return [] ;
        }

        $products = $brand->products()->whereIn('id',$operator->products->pluck('id'))->orderBy('created_at', 'DESC')->get();
        $products->each(function($product) use ($operator){
            $product->category_name = Category::find($product->category_id)->category_name ;
        });
        return response(json_encode($products));
    }


    public function get_products_by_brandV2(Request $request)
    {
        try {
            $operator = Operator::findOrFail($request->input('operator_id'));
        } catch (ModelNotFoundException $e) {
            return response()->json("Operator Not Found", 200);
        }

        try {
            $brand = Brand::findOrFail($request->input('brand_id'));
        } catch (ModelNotFoundException $e) {
            return response()->json("Brand Not Found", 200);
        }

        $products = $brand->products()->paginate($this->_PAGINATION) ; // el edit 7asal hena , kanet kda $brand->products
        $filteredProducts = array();
        $x = 0 ;
        for($i = 0 ; $i< count($products) ; $i++)
        {
            $post = null ;
            $post = Post::where('product_id',$products[$i]->id)->where('operator_id',$operator->id)->where('show_date',$this->sign,$this->date)->where('active',$this->active_sign,$this->active_flag)->get()  ;
            if (count($post)>0)
            {
                $filteredProducts[$x] = $products[$i] ;
                $filteredProducts[$x]['brand_name'] = Brand::find($products[$i]->brand_id)->brand_name ;
                $filteredProducts[$x]['category_name'] = Category::find($products[$i]->category_id)->category_name ;
                $x++ ;
            }
        }

        return response(json_encode($filteredProducts));
    }


    public function view_prodcut_details(Request $request)
    {
        if (empty($request['product_id']))
            return -1 ;
        if (!empty($request['operator_id']))
        {
            $sql = "SELECT products.*,brands.brand_name AS brand_name, categories.category_name AS category_name ,posts.operator_id as operator_id
                    FROM posts 
                    JOIN products ON posts.product_id = products.id 
                    JOIN categories ON products.category_id=categories.id 
                    JOIN brands ON products.brand_id = brands.id
                    WHERE posts.operator_id=".$request['operator_id']." AND posts.product_id=".$request['product_id']." AND posts.show_date $this->sign '".$this->date."' " ;
        }
        else
        {
            if($this->FRONT_TESTING)
            {
                $sql = "SELECT products.*,brands.brand_name AS brand_name, categories.category_name AS category_name 
                FROM products 
                JOIN categories ON products.category_id=categories.id 
                JOIN brands ON products.brand_id = brands.id 
                WHERE products.id=".$request['product_id'] ;
            }
            else{
                $sql = NULL ;
            }

        }
        if($sql!=NULL)
            $product = \DB::select($sql);
        else 
            return -1 ;
        if (empty($product))
            return -1 ;
        return $product ;

    }



    public function GetBrandsByCategory(Request $request)
    {
        try{
            $brands = DB::table('posts')
                        ->join('products','posts.product_id','=','products.id')
                        ->join('brands','products.brand_id','=','brands.id')
                        ->where('active',$this->active_sign,$this->active_flag)
                        ->where('show_date',$this->sign,$this->date)
                        ->where('operator_id',$request->get('operator_id'))
                        ->where('category_id',$request->get('category_id'))->get();
//            return response($brands);
            if (count($brands)>0)
                return response(json_encode($brands));
            else
                return response(json_encode("No result found"));
        }catch (ModelNotFoundException $e) {
            return response()->json("Not Found brands", 200);
        }
    }


    public function SearchByKeyword(Request $request)
    {
        $keyword = $request->get('keyword');
        $operator = $request->get('operator_id');
        if (isset($operator))
        {
            /*************  GET PRODUCTS BY KEYWORD ******************/
            $products = DB::table('categories')
                ->join('products','categories.id','=','products.category_id')
                ->join('posts','posts.product_id','=','products.id')
                ->join('brands','products.brand_id','=','brands.id')
                ->where('category_name','LIKE','%'.$keyword.'%')
                ->where('posts.operator_id',$operator)
                ->where('posts.active',$this->active_sign,$this->active_flag)
                ->where('posts.show_date',$this->sign,$this->date)
                ->select('categories.category_name','categories.id AS category_id','brands.brand_name','brands.id AS brand_id','products.product_image','products.id AS product_id','products.title','products.updated_at')
                ->get() ;

            /***********   Search in Brands with that keyword and operator id  *************/

            $brands = DB::table('brands')
                ->join('products','brands.id','=','products.brand_id')
                ->join('posts','posts.product_id','=','products.id')
                ->join('categories','products.category_id','=','categories.id')
                ->where('brand_name','LIKE','%'.$keyword.'%')
                ->where('posts.operator_id',$operator)
                ->where('posts.active',$this->active_sign,$this->active_flag)
                ->where('posts.show_date',$this->sign,$this->date)
                ->select('categories.category_name','categories.id AS category_id','brands.brand_name','brands.id AS brand_id','products.product_image','products.id AS product_id','products.title','products.updated_at')
                ->get() ;

            /***************   GET SPECIFIC PRODUCT WITH HIS TITLE  *****************/

            $pr = DB::table('products')
                ->join('posts','posts.product_id','=','products.id')
                ->join('categories','products.category_id','=','categories.id')
                ->join('brands','products.brand_id','=','brands.id')
                ->where('products.title','LIKE','%'.$keyword.'%')
                ->where('posts.operator_id',$operator)
                ->where('posts.active',$this->active_sign,$this->active_flag)
                ->where('posts.show_date',$this->sign,$this->date)
                ->select('categories.category_name','categories.id AS category_id','brands.brand_name','brands.id AS brand_id','products.product_image','products.id AS product_id','products.title','products.updated_at')
                ->get() ;

        }
        else{
            /*************  GET PRODUCTS BY KEYWORD ******************/
            $products = DB::table('categories')
                ->join('products','categories.id','=','products.category_id')
                ->join('brands','products.brand_id','=','brands.id')
                ->where('category_name','LIKE','%'.$keyword.'%')
                ->select('categories.category_name','categories.id AS category_id','brands.brand_name','brands.id AS brand_id','products.product_image','products.id AS product_id','products.title','products.updated_at')
                ->get() ;

            /***********   Search in Brands with that keyword and operator id  *************/

            $brands = DB::table('brands')
                ->join('products','brands.id','=','products.brand_id')
                ->join('categories','products.category_id','=','categories.id')
                ->where('brand_name','LIKE','%'.$keyword.'%')
                ->select('categories.category_name','categories.id AS category_id','brands.brand_name','brands.id AS brand_id','products.product_image','products.id AS product_id','products.title','products.updated_at')
                ->get() ;

            /***************   GET SPECIFIC PRODUCT WITH HIS TITLE  *****************/

            $pr = DB::table('products')
                ->join('categories','products.category_id','=','categories.id')
                ->join('brands','products.brand_id','=','brands.id')
                ->where('products.title','LIKE','%'.$keyword.'%')
                ->select('categories.category_name','categories.id AS category_id','brands.brand_name','brands.id AS brand_id','products.product_image','products.id AS product_id','products.title','products.updated_at')
                ->get() ;

        }

        if (count($pr)>0)
            return $pr ;
        if(count($brands)>0)
            return $brands ;
        if(count($products)>0)
            return $products ;
    }


    public function GetSocialMediaLinks(Request $request)
    {
        if ($request->input('key'))
        {
            $setting = Setting::where('key','LIKE','%'.$request->input('key').'%')->get() ;
            return $setting;
        }
        $links = Setting::all();
        return $links;
    }

    public function get_setting(Request $request)
    {
        $setting = Setting::where('key','LIKE','%'.$request->input('key').'%')->first() ;
        return $setting ;
    }


    public function comming_soon_movies(Request $request)
    {
        // 1. el aflam ele el publish date bta3ha bokra , 
        // 2. ha search fel post table , 
        // 3. operator is required 
        $op_id = $request['operator_id'] ; 

        if(!isset($op_id) || !is_numeric($op_id))
            return [] ; 
        
        $today =  new \DateTime('now');
        $today =  $today->format('Y-m-d');

        // to not get next of comming videos
        $today = date('Y-m-d');
        $comming_day =  date('Y-m-d', strtotime($today. ' + 6 days'));

        $comming_soon = Post::where('show_date','>',$today)->where('show_date','<',$comming_day)->where('operator_id',$op_id)->get()->take(3) ;

        return $comming_soon  ;
    }


    public function today_movies(Request $request)
    {
        // 1. el aflam ele el publish date bta3ha today ,
        // 2. ha search fel post table ,
        // 3. operator is required
        $op_id = $request['operator_id'] ;

        if(!isset($op_id) || !is_numeric($op_id))
            return [] ;

        $today = date('Y-m-d');
        $today_videos = Post::where('show_date',$today)->where('operator_id',$op_id)->where('product_id','!=',$request['product_id'])->orderBy('id','ASC')->get() ;

        return $today_videos  ;
    }



    public function advertisments(Request $request)
    {
        // select ads with operator from request 
        $op_id = $request['operator_id'] ; 

        if(!isset($op_id) || !is_numeric($op_id))
            return [] ; 

        $ads = Ads::where('published_date',$this->sign,$this->date)->get() ; 
        return $ads ; 
    }

    public function HEFromLanding()
    {

        print_r($_SERVER) ; die;

        $URL = "http://dev.ivashosting.com/landing/HEDetect";
        $result = file_get_contents($URL);
        print_r($result) ;

     //   $Responnse = json_decode($result);

       // echo $Responnse->HE ;

    }


    public function GetPageData($URL) {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }


}
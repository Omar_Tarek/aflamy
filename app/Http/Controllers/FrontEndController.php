<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Controllers\API\CategoryController;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Carbon\Carbon;
use App\Msisdn;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class FrontEndController extends Controller {

    public function verify(Request $request) {


        $api = $this->init()[0];
        $settings = $this->init()[1];
        $categories = $this->standards($request);
        //  print_r($categories); die;
        $featured = $api->get_latest_featured_for_slider($request);
        if (empty($categories) && empty($featured))
            return view('front_end.not-found', compact('categories', 'settings'));
        return view('front_end.verify', compact('categories', 'featured', 'settings'));
    }

    public function vod_landing(Request $request) {
        if (isset($_SERVER['HTTP_MSISDN'])) {
            $MSISDN = str_replace("965", "", $_SERVER['HTTP_MSISDN']);
            $URL_redirect = "http://dev.ivashosting.com/landing_aflamy/HE/$MSISDN";
        } else {
            $URL_redirect = "http://dev.ivashosting.com/landing_aflamy";
            $MSISDN = "";
        }


        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $deviceModel = $_SERVER['HTTP_USER_AGENT'];
        } else {
            $deviceModel = "";
        }


        // header inrichemnt DETECT
        $result = array();
        // get client ip
        $ip = $_SERVER["REMOTE_ADDR"];

        if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
            $ip = $_SERVER['HTTP_CLIENT_IP'];


        $country_from_ip = $this->ip_info("Visitor", "Country");
        $result['date'] = Carbon::now()->format('Y-m-d H:i:s');
        $result['ip'] = $ip;
        $result['country'] = $country_from_ip;
        $result['HeadeEnriched'] = $MSISDN;
        $result['deviceModel'] = $deviceModel;
        $result['AllHeaders'] = $_SERVER;

        // make log
        $actionName = "Hit page VOD Landing";
        $URL = $request->fullUrl();
        $parameters_arr = $result;
        $this->log($actionName, $URL, $parameters_arr);


        return redirect($URL_redirect);
    }

    public function index(Request $request) {


        // header inrichemnt DETECT
        $result = array();
        // get client ip
        $ip = $_SERVER["REMOTE_ADDR"];

        if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
            $ip = $_SERVER['HTTP_CLIENT_IP'];

        // HE
        if (isset($_SERVER['HTTP_MSISDN'])) {
            $msisdn = $_SERVER['HTTP_MSISDN'];
            $MSISDN = str_replace("965", "", $_SERVER['HTTP_MSISDN']);
            session(['MSISDN' => $MSISDN, 'Status' => 'active']);
        } else {
            $MSISDN = $msisdn = "";
        }


        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $deviceModel = $_SERVER['HTTP_USER_AGENT'];
        } else {
            $deviceModel = "";
        }


        $country_from_ip = $this->ip_info("Visitor", "Country");
        $result['date'] = Carbon::now()->format('Y-m-d H:i:s');
        $result['ip'] = $ip;
        $result['country'] = $country_from_ip;
        $result['HeadeEnriched'] = $MSISDN;
        $result['deviceModel'] = $deviceModel;
        $result['AllHeaders'] = $_SERVER;

        // make log
        $actionName = "Hit page";
        $URL = $request->fullUrl();
        $parameters_arr = $result;
        $this->log($actionName, $URL, $parameters_arr);


        return view('home.index', compact('MSISDN'));
    }

    public function front(Request $request) {
        // if (Session::has('MSISDN') && Session::get('Status') == 'active') {
        $api = $this->init()[0];
        $settings = $this->init()[1];
        $categories = $this->standards($request);
        //  print_r($categories); die;
        $featured = $api->get_latest_featured_for_slider($request);


        // header inrichemnt DETECT
        $result = array();
        // get client ip
        $ip = $_SERVER["REMOTE_ADDR"];

        if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
            $ip = $_SERVER['HTTP_CLIENT_IP'];

        // HE
        if (isset($_SERVER['HTTP_MSISDN'])) {
            $msisdn = $_SERVER['HTTP_MSISDN'];
            $MSISDN = str_replace("965", "", $_SERVER['HTTP_MSISDN']);
        } else {
            $MSISDN = $msisdn = "";
        }


        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $deviceModel = $_SERVER['HTTP_USER_AGENT'];
        } else {
            $deviceModel = "";
        }


        $country_from_ip = $this->ip_info("Visitor", "Country");
        $result['date'] = Carbon::now()->format('Y-m-d H:i:s');
        $result['ip'] = $ip;
        $result['country'] = $country_from_ip;
        $result['HeadeEnriched'] = $MSISDN;
        $result['deviceModel'] = $deviceModel;
        $result['AllHeaders'] = $_SERVER;

        // make log
        $actionName = "Hit page";
        $URL = $request->fullUrl();
        $parameters_arr = $result;
        $this->log($actionName, $URL, $parameters_arr);


        session::put('adv_params', $_SERVER['QUERY_STRING']);

        if (empty($categories) && empty($featured))
            return view('front_end.not-found', compact('categories', 'settings'));
        return view('front_end.index', compact('categories', 'featured', 'settings', 'MSISDN'));

        /*  } else {
          return redirect(url('login'));
          } */
    }

    public function get_content($URL) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $URL);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

    public function OoredooCheckSubscriptioo($MSISDN) {
        $URL_check = "http://dev.ivashosting.com/landing_aflamy/checkOoredooNumber?msisdn=965" . $MSISDN;
        $result_check = $this->get_content($URL_check); //    1= SUCCESS  else  not
        $result_check = json_decode(preg_replace('/\s+/', '', $result_check));  // to remove extra space created by soap request

        return $result_check->statue;
    }

    public function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city" => @$ipdat->geoplugin_city,
                            "state" => @$ipdat->geoplugin_regionName,
                            "country" => @$ipdat->geoplugin_countryName,
                            "country_code" => @$ipdat->geoplugin_countryCode,
                            "continent" => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }

    public function log($actionName, $URL, $parameters_arr) {
        date_default_timezone_set("Africa/Cairo");
        $date = date("Y-m-d");
        $log = new Logger($actionName);
        // to create new folder with current date  // if folder is not found create new one
        if (!File::exists(storage_path('logs/' . $date . '/' . $actionName))) {
            File::makeDirectory(storage_path('logs/' . $date . '/' . $actionName), 0775, true, true);
        }

        $log->pushHandler(new StreamHandler(storage_path('logs/' . $date . '/' . $actionName . '/logFile.log', Logger::INFO)));
        $log->addInfo($URL, $parameters_arr);
    }

    public function get_brand_products(Request $request) {
        if (Session::has('MSISDN') && Session::get('Status') == 'active') {
            $api = $this->init()[0];
            $settings = $this->init()[1];
            $categories = $this->standards($request);
            $products = $api->get_products_by_brandV3($request);
            if ($products == -1)
                return view('front_end.not-found', compact('categories', 'settings'));
            return view('front_end.brand', compact('categories', 'products', 'settings'));
        } else {
            return redirect(url('login'));
        }
    }

    public function get_product(Request $request) {
        if (Session::has('MSISDN') && Session::get('Status') == 'active') {
            $api = $this->init()[0];
            $settings = $this->init()[1];
            $categories = $this->standards($request);
            $product = $api->view_prodcut_details($request);
            $comming_soons = $api->comming_soon_movies($request);
            $today_movies = $api->today_movies($request);

            // print_r($today_movies); die;

            $ads = $api->advertisments($request);
            if ($product == -1)
                return view('front_end.not-found', compact('categories', 'settings'));
            return view('front_end.product', compact('categories', 'product', 'settings', 'comming_soons', 'ads', 'today_movies'));
        } else {

            if (session::has('adv_params') && count(session::get('adv_params')) > 0) {
                $adv_params = session::get('adv_params');
                return redirect(url() . "?" . $adv_params);
            } else {

                return redirect(url());
            }
        }
    }

    public function products_by_category(Request $request) {
        // if (Session::has('MSISDN') && Session::get('Status') == 'active') {
        $api = $this->init()[0];
        $settings = $this->init()[1];
        $categories = $this->standards($request);
        $products = $api->get_products_by_categories_V2($request);
        if ($products == -1)
            return view('front_end.not-found', compact('categories', 'settings'));
        return view('front_end.category', compact('categories', 'products', 'settings'));
        /* } else {
          return redirect(url('login'));
          } */
    }

    public function search_view(Request $request) {
        //  if (Session::has('MSISDN') && Session::get('Status') == 'active') {
        $settings = $this->init()[1];
        $categories = $this->standards($request);
        $results = array();
        return view('front_end.search', compact('categories', 'results', 'settings'));
        /* } else {
          return redirect(url('login'));
          } */
    }

    public function search(Request $request) {
        $api = $this->init()[0];
        $results = $api->SearchByKeyword($request);
        return $results;
    }

    public function terms(Request $request) {
        $api = $this->init()[0];
        $settings = $this->init()[1];
        $categories = $this->standards($request);
        $request['key'] = "Terms";
        $result = $api->get_setting($request);
       /* if (count($result) == 0)
            return view('front_end.not-found', compact('categories', 'settings'));*/
        return view('front_end.terms', compact('categories', 'result', 'settings'));
    }

    public function login(Request $request) {
        $msisdn = "";
        $settings = $this->init()[1];
        $categories = $this->standards($request);
        if (isset($_GET['operator_id']) && !empty($_GET['operator_id'])) {
            $operator_id = $_GET['operator_id'];
            if ($operator_id == OOREDOO_op_id_database) {
                $prefix = OOREDOO_COUNTRY_PREFIX;
            } elseif ($operator_id == ZainJordon_op_id_database) {
                $prefix = ZAIN_JORDON_COUNTRY_PREFIX;
            }
        }

        if (isset($_REQUEST['msisdn']))
            $msisdn = $_REQUEST['msisdn'];
        return view('front_end.login', compact('categories', 'settings', 'operator_id', 'msisdn', 'prefix'));
    }

    public function loginValidate(request $request) {

        $operator_prefix = $request->input('operator_prefix');
        if ($operator_prefix == OOREDOO_COUNTRY_PREFIX) {
            $regular_exp = '/^[0-9]{8}$/';
            $operator_id = OOREDOO_op_id_database;
            $zain_jo = "";
        } elseif ($operator_prefix == ZAIN_JORDON_COUNTRY_PREFIX) {
            $regular_exp = '/^[0-9]{9}$/';
            $operator_id = ZainJordon_op_id_database;
            $zain_jo = "/zainjordan";
        }

        if (!preg_match($regular_exp, $request->input('msisdn'))) {
            $request->session()->flash('failed', 'هذا الرقم غير صحيح');
            return back();
        }

        $MSISDN = $request->input('msisdn');

        $msisdn = $operator_prefix . $request->input('msisdn');
        $Msisdn_found = Msisdn::where('msisdn', '=', $msisdn)->where('final_status', '=', 1)->orderBy('id', 'DESC')->first();

        if ($Msisdn_found) {
            session(['MSISDN' => $MSISDN, 'Status' => 'active', 'operator_id' => $operator_id]);
            if ($request->input('prev_url') !== NULL) {
                return redirect($request->input('prev_url'));
            } else {
                return redirect(url("front?operator_id=" . $operator_id))->with('operator_id', $operator_id);
            }
        } else {
            $request->session()->flash('error', 'انت غير مشترك بالخدمة');
            return redirect(url("/") . $zain_jo);
        }
    }

    public function myaccount(request $request) {
        if (Session::has('MSISDN') && Session::get('Status') == 'active') {
            $api = $this->init()[0];
            $settings = $this->init()[1];
            $categories = $this->standards($request);
            if (isset($_GET['operator_id']) && !empty($_GET['operator_id']))
                $operator_id = $_GET['operator_id'];

            $op_id = Session::get('operator_id');
            if ($op_id == OOREDOO_OP_ID) {
                $msisdn = OOREDOO_COUNTRY_PREFIX . Session::get('MSISDN');
                $operatorName = "اريدو الكويت";
            } else {
                $msisdn = ZAIN_JORDON_COUNTRY_PREFIX . Session::get('MSISDN');
                $operatorName = "زين الأردن";
            }
            $Msisdn_found = Msisdn::where('msisdn', '=', $msisdn)->where('final_status', '=', 1)->orderBy('id', 'DESC')->first();


            if ($op_id == OP_ID_General) {
                $operator_id = OP_ID_General;
                $msisdn = "12345678";
                $operatorName = " عام";
                $Msisdn_found = Msisdn::where('msisdn', '=', $msisdn)->where('final_status', '=', 1)->orderBy('id', 'DESC')->first();
            }

            if ($Msisdn_found) {
                return view('front_end.myaccount', compact('categories', 'settings', 'operator_id', 'Msisdn_found', 'msisdn', 'operatorName'));
            } else {
                $request->session()->flash('error', 'انت غير مشترك بالخدمة');
                return redirect(url("/login"));
            }
        } else {
            return redirect(url('login'));
        }
    }

    public function logout() {

        if (Session::get('operator_id') == OOREDOO_OP_ID) {
            Session::flush();
            return redirect('/');
        } else {
            Session::flush();
            return redirect('/zainjordan');
        }
    }

    public function register(Request $request) {
        return redirect(url('index?operator_id=2'));
    }

    public function init() {
        $settings = array();
        $api = new CategoryController();
        $settings[0] = Setting::where('key', 'LIKE', '%website title%')->first();
        $settings[1] = Setting::where('key', 'LIKE', '%logo%')->first();
        $settings[2] = Setting::where('key', 'LIKE', '%facebook%')->first();
        $settings[3] = Setting::where('key', 'LIKE', '%twitter%')->first();
        return [$api, $settings];
    }

    public function standards(Request $request) {
        $api = $this->init()[0];
        $obj = $api->get_all_categories_with_latest($request);
        return $obj;
    }


       public function terms22(Request $request) {


        $api = $this->init()[0];
        $settings = $this->init()[1];
      //  $categories = $this->standards($request);
        return view('front_end.conditions', compact( 'settings'));
    }

}

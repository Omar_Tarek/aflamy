<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    public function categories()
    {
        $category = Category::all() ;
        $data = array();
        for($j=0 ; $j<count($category) ; $j++)
        {
            for($i=0 ; $i< count($category[$j]->products) ; $i++)
            {
                $products = Product::findOrFail($category[$j]->products[$i]->id) ;
                $brand  = $products->brand ;
                $data['category'][$j] = $category[$j] ;
                $data['category'][$j]['products'][$i]['brand'] = $brand ;
            }
        }
        $json = json_encode($data);
        return $json ;
       // return $category;
    }

    public function specificCategory($id)
    {
        $category = Category::findOrFail($id) ;
        $data = array();
        for($i=0 ; $i< count($category->products) ; $i++)
        {
            $products = Product::findOrFail($category->products[$i]->id) ;
            $brand  = $products->brand ;
            $data['category'] = $category ;
            $data['category']['products'][$i]['brand'] = $brand ;
        }

        $json = json_encode($data);
        return $json ;
    }

}

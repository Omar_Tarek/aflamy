<?php

namespace App\Http\Controllers;

use App\Http\Requests\OperatorRequest;
use App\Http\Requests\UpdateOperatorRequest;
use App\Operator;
use App\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class OperatorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $operators = Operator::all();
        return view('operators.listoperator',compact('operators'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('operators.addoperator');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OperatorRequest $request)
    {
        $operator = new Operator($request->all());
        $file = $request->file('operator_image') ;
        $destinationFolder = "operators_images/" ;
        $uniqueID = uniqid();
        $operator['operator_image'] = $destinationFolder.$uniqueID.".".$file->getClientOriginalExtension() ;
        $file->move($destinationFolder ,$uniqueID.".".$file->getClientOriginalExtension()) ;
        $operator->save() ;
        \Session::flash('success','Operator Added successsfully');
        return redirect('operators');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $operator = Operator::findOrFail($id) ;
        return view('operators.editoperator',compact('operator')) ;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(OperatorRequest $request, $id)
    {
        $imgExtensions = array("png","jpeg","jpg");
        $oldOperator = Operator::findOrFail($id);
        $newOperator = $request->all();
        $destinationFolder = "operators_images/";
        if ($request->hasFile('operator_image'))
        {
            $file = $request->file("operator_image");
            if(! in_array($file->getClientOriginalExtension(),$imgExtensions))
            {
                \Session::flash('failed','Image must be jpg, png, or jpeg only !! No updates takes place, try again with that extensions please..');
                return redirect('products');
            }
            $uniqueid = uniqid();
            $file->move($destinationFolder,$uniqueid.".".$file->getClientOriginalExtension());
            $newOperator['operator_image'] = $destinationFolder.$uniqueid.".".$file->getClientOriginalExtension();
            if (file_exists($oldOperator['operator_image']))
            {
                Storage::delete($oldOperator['operator_image']);
            }
        }
        else
        {
            $newOperator['operator_image'] = $oldOperator['operator_image'];
        }

        $oldOperator->update($newOperator);
        \Session::flash('success','Operator Updated Successfully');
        return redirect('operators') ;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $operator = Operator::find($id) ;
        if (file_exists($operator['operator_image']))
            Storage::delete($operator['operator_image']);
        $operator->destroy($id);
        return redirect('operators');
    }
}

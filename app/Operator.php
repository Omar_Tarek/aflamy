<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{
    protected $fillable = [
        'name',
        'operator_image'
    ];

    public function posts()
    {
        return $this->hasOne('App\Post');
    }

   public function products()
   {
        return $this->belongsToMany('App\Product','posts','operator_id','product_id') ;
    //    return $this->belongsToMany('App\Product','posts','operator_id','product_id')->wherePivot('active',1)->wherePivot('show_date','<=',Carbon::today());
   }

   public function featured()
   {
        return $this->belongsToMany('App\Product','posts','operator_id','product_id')->wherePivot('featured',1); 
    //    return $this->belongsToMany('App\Product','posts','operator_id','product_id')->wherePivot('active',1)->wherePivot('show_date','<=',Carbon::today())->wherePivot('featured',1);
   }
}

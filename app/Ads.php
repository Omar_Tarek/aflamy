<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    protected $table = "ads" ; 
    protected $fillable = ["link","image","operator_id","published_date"] ; 

    public function operator()
    {
        return $this->belongsTo('App\Operator');
    }

}

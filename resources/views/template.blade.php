<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>
        Aflamy
    </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <!--page specific css styles-->
    <link rel="stylesheet" type="text/css" href="{{url('assets_/chosen-bootstrap/chosen.min.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('assets_/jquery-tags-input/jquery.tagsinput.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('assets_/jquery-pwstrength/jquery.pwstrength.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('assets_/bootstrap-fileupload/bootstrap-fileupload.css')}}"/>
    <link rel="stylesheet" type="text/css"
          href="{{url('assets_/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('assets_/dropzone/downloads/css/dropzone.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('assets_/bootstrap-colorpicker/css/colorpicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('assets_/bootstrap-timepicker/compiled/timepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('assets_/clockface/css/clockface.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('assets_/bootstrap-datepicker/css/datepicker.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('assets_/bootstrap-daterangepicker/daterangepicker.css')}}"/>
    <link rel="stylesheet" type="text/css"
          href="{{url('assets_/bootstrap-switch/static/stylesheets/bootstrap-switch.css')}}"/>
    <link rel="stylesheet" type="text/css" href="{{url('assets_/bootstrap-wysihtml5/bootstrap-wysihtml5.css')}}"/>

    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>


    <!--base css styles-->
    <link rel="stylesheet" href="{{url('assets_/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets_/font-awesome/css/font-awesome.min.css')}}">
    <!--page specific css styles-->


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.bootstrap.min.css"/>
    <link rel="stylesheet" href="{{url('assets_/data-tables/bootstrap3/dataTables.bootstrap.css')}}"/>


    <!--flaty css styles-->
    <link rel="stylesheet" href="{{url('css/flaty.css')}}">
    <link rel="stylesheet" href="{{url('css/flaty-responsive.css')}}">

    <link rel="shortcut icon" href="{{url('img/favicon.png')}}">
    <script>
        function ConfirmDelete() {
            var x = confirm("Are you sure you want to delete?");
            if (x)
                return true;
            else
                return false;
        }
    </script>

</head>
<body>
<div id="theme-setting">
    <a href="#"><i class="fa fa-gears fa fa-2x"></i></a>
    <ul>
        <li>
            <span>Skin</span>
            <ul class="colors" data-target="body" data-prefix="skin-">
                <li class="active"><a class="blue" href="#"></a></li>
                <li><a class="red" href="#"></a></li>
                <li><a class="green" href="#"></a></li>
                <li><a class="orange" href="#"></a></li>
                <li><a class="yellow" href="#"></a></li>
                <li><a class="pink" href="#"></a></li>
                <li><a class="magenta" href="#"></a></li>
                <li><a class="gray" href="#"></a></li>
                <li><a class="black" href="#"></a></li>
            </ul>
        </li>
        <li>
            <span>Navbar</span>
            <ul class="colors" data-target="#navbar" data-prefix="navbar-">
                <li class="active"><a class="blue" href="#"></a></li>
                <li><a class="red" href="#"></a></li>
                <li><a class="green" href="#"></a></li>
                <li><a class="orange" href="#"></a></li>
                <li><a class="yellow" href="#"></a></li>
                <li><a class="pink" href="#"></a></li>
                <li><a class="magenta" href="#"></a></li>
                <li><a class="gray" href="#"></a></li>
                <li><a class="black" href="#"></a></li>
            </ul>
        </li>
        <li>
            <span>Sidebar</span>
            <ul class="colors" data-target="#main-container" data-prefix="sidebar-">
                <li class="active"><a class="blue" href="#"></a></li>
                <li><a class="red" href="#"></a></li>
                <li><a class="green" href="#"></a></li>
                <li><a class="orange" href="#"></a></li>
                <li><a class="yellow" href="#"></a></li>
                <li><a class="pink" href="#"></a></li>
                <li><a class="magenta" href="#"></a></li>
                <li><a class="gray" href="#"></a></li>
                <li><a class="black" href="#"></a></li>
            </ul>
        </li>
        <li>
            <span></span>
            <a data-target="navbar" href="#"><i class="fa fa-square-o"></i> Fixed Navbar</a>
            <a class="hidden-inline-xs" data-target="sidebar" href="#"><i class="fa fa-square-o"></i> Fixed Sidebar</a>
        </li>
    </ul>
</div>
<!-- BEGIN Navbar -->
<div id="navbar" class="navbar">
    <button type="button" class="navbar-toggle navbar-btn collapsed" data-toggle="collapse" data-target="#sidebar">
        <span class="fa fa-bars"></span>
    </button>
    <a class="navbar-brand" href="{{url('/')}}">
        <small>
            <i class="fa fa-user-secret"></i>
            Aflamy
        </small>
    </a>

    <!-- BEGIN Navbar Buttons -->
    <ul class="nav flaty-nav pull-right">

        <!-- BEGIN Tasks Dropdown -->

        <!-- BEGIN Button User -->
        <li class="user-profile">
            <a data-toggle="dropdown" href="#" class="user-menu dropdown-toggle">
                        <span class="hhh" id="user_info">
                            {!! Auth::user()->name !!}
                        </span>
                <i class="fa fa-caret-down"></i>
            </a>

            <!-- BEGIN User Dropdown -->
            <ul class="dropdown-menu dropdown-navbar" id="user_menu">
                <li>
                    <a href="{{url('user_profile')}}">
                        <i class="fa fa-user"></i>
                        Edit Profile
                    </a>
                </li>

                <li class="divider visible-xs"></li>

                <li class="divider"></li>

                <li>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </li>
            </ul>
            <!-- BEGIN User Dropdown -->
        </li>
        <!-- END Button User -->
    </ul>
    <!-- END Navbar Buttons -->
</div>
<!-- END Navbar -->

<!-- BEGIN Container -->
<div class="container" id="main-container">
    <!-- BEGIN Sidebar -->
    <div id="sidebar" class="navbar-collapse collapse">
        <!-- BEGIN Navlist -->
        <ul class="nav nav-list">
            @if(Auth::user()->hasRole('super_admin'))
                <li id="user">
                    <a href="#" class="dropdown-toggle">
                        <i class="glyphicon glyphicon-user"></i>
                        <span>Users</span>
                        <b class="arrow fa fa-angle-right"></b>
                    </a>

                    <!-- BEGIN Submenu -->
                    <ul class="submenu">
                        <li id="user-create"><a href="{{url('users/new')}}">Create User</a></li>
                        <li id="user-index"><a href="{{url('users')}}">Users</a></li>
                    </ul>
                    <!-- END Submenu -->
                </li>

                <li id="role">
                    <a href="#" class="dropdown-toggle">
                        <i class="glyphicon glyphicon-road"></i>
                        <span>Roles</span>
                        <b class="arrow fa fa-angle-right"></b>
                    </a>

                    <!-- BEGIN Submenu -->
                    <ul class="submenu">
                        <li id="role-create"><a href="{{url('roles/new')}}">Create Role</a></li>
                        <li id="role-index"><a href="{{url('roles')}}">Roles</a></li>
                        <li id="route-index"><a href="{{url('route')}}">Routes</a></li>
                        <li id="route-v2-index"><a href="{{url('routes_v2')}}">Routes V2</a></li>
                    </ul>
                    <!-- END Submenu -->
                </li>
            @endif
            <li id="category">
                <a href="#" class="dropdown-toggle">
                    <i class="fa fa-info"></i>
                    <span>Category</span>
                    <b class="arrow fa fa-angle-right"></b>
                </a>

                <!-- BEGIN Submenu -->
                <ul class="submenu">
                    <li id="category-create"><a href="{{url('categories/create')}}">Add Category</a></li>
                    <li id="category-manage"><a href="{{url('categories')}}">List Categories</a></li>
                </ul>
                <!-- END Submenu -->
            </li>

            <li id="product">
                <a href="#" class="dropdown-toggle">
                    <i class="glyphicon glyphicon-facetime-video"></i>
                    <span>Videos</span>
                    <b class="arrow fa fa-angle-right"></b>
                </a>

                <!-- BEGIN Submenu -->
                <ul class="submenu">
                    <li id="product-create"><a href="{{url('products/create')}}">Add Video</a></li>
                    <li id="product-index"><a href="{{url('products')}}">List Videos</a></li>
                </ul>
                <!-- END Submenu -->
            </li>

            <li id="post">
                <a href="#" class="dropdown-toggle">
                    <i class="fa fa-send"></i>
                    <span>Posts</span>
                    <b class="arrow fa fa-angle-right"></b>
                </a>

                <!-- BEGIN Submenu -->
                <ul class="submenu">
                    <li id="post-manage"><a href="{{url('posts')}}">Manage Posts</a></li>
                </ul>
                <!-- END Submenu -->
            </li>

            <li id="brand">
                <a href="#" class="dropdown-toggle">
                    <i class="glyphicon glyphicon-barcode"></i>
                    <span>Brands</span>
                    <b class="arrow fa fa-angle-right"></b>
                </a>

                <!-- BEGIN Submenu -->
                <ul class="submenu">
                    <li id="brand-create"><a href="{{url('brands/create')}}">Add brands</a></li>
                    <li id="brand-index"><a href="{{url('brands')}}">List Brands</a></li>
                </ul>
                <!-- END Submenu -->
            </li>

            <li id="operator">
                <a href="#" class="dropdown-toggle">
                    <i class="glyphicon glyphicon-star"></i>
                    <span>Operators</span>
                    <b class="arrow fa fa-angle-right"></b>
                </a>

                <!-- BEGIN Submenu -->
                <ul class="submenu">
                    <li id="operator-create"><a href="{{url('operators/create')}}">Add Operators</a></li>
                    <li id="operator-index"><a href="{{url('operators')}}">List Operators</a></li>
                </ul>
                <!-- END Submenu -->
            </li>
            <li id="setting">
                <a href="#" class="dropdown-toggle">
                    <i class="fa fa-gears"></i>
                    <span>Setting</span>
                    <b class="arrow fa fa-angle-right"></b>
                </a>

                <!-- BEGIN Submenu -->
                <ul class="submenu">
                    <li id="setting-create"><a href="{{url('setting/new')}}">Add Settings</a></li>
                    <li id="setting-index"><a href="{{url('setting')}}">Settings</a></li>
                    <li id="setting-import-DB"><a href="{{url('database_backups')}}">Database Backups</a></li>
                    <li><a href="{{url('clear_cache')}}">Clear Cashe</a></li>
                </ul>
                <!-- END Submenu -->
            </li>
            <li id="ad">
                <a href="#" class="dropdown-toggle">
                    <i class="fa fa-usd"></i>
                    <span>Advertisments</span>
                    <b class="arrow fa fa-angle-right"></b>
                </a>

                <!-- BEGIN Submenu -->
                <ul class="submenu">
                    <li id="ad-create"><a href="{{url('advertisments/create')}}">Add Ad.</a></li>
                    <li id="ad-index"><a href="{{url('advertisments')}}">Ads.</a></li>
                </ul>
                <!-- END Submenu -->
            </li>
        </ul>
        <!-- END Navlist -->

        <!-- BEGIN Sidebar Collapse Button -->
        <div id="sidebar-collapse" class="visible-lg">
            <i class="fa fa-angle-double-left"></i>
        </div>
        <!-- END Sidebar Collapse Button -->
    </div>
    <!-- END Sidebar -->

    <!-- BEGIN Content -->
    <div id="main-content">
        <!-- BEGIN Page Title -->
        <div class="page-title">
            <div>
                <h1><i class="fa fa-file-o"></i> @yield('page_title')</h1>
            </div>
        </div>
        <!-- END Page Title -->

        <!-- BEGIN Breadcrumb -->
        <div id="breadcrumbs">
            <ul class="breadcrumb">
                <li class="active"><i class="fa fa-home"></i> Home/ @yield('page_title') </li>
            </ul>
        </div>
        <!-- END Breadcrumb -->

        @include('partial.flash')
        @yield('content')
    </div>
    <footer>
        <p>2015 © FLATY Admin Template.</p>
    </footer>

    <a id="btn-scrollup" class="btn btn-circle btn-lg" href="#"><i class="fa fa-chevron-up"></i></a>
</div>
<!-- END Content -->
<!-- END Container -->

<!--basic scripts-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="assets_/jquery/jquery-2.1.4.min.js"><\/script>')</script>

<script src="{{url('assets_/bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets_/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{url('assets_/jquery-cookie/jquery.cookie.js')}}"></script>

<!--page specific plugin scripts-->
<script src="{{url('assets_/flot/jquery.flot.js')}}"></script>
<script src="{{url('assets_/flot/jquery.flot.resize.js')}}"></script>
<script src="{{url('assets_/flot/jquery.flot.pie.js')}}"></script>
<script src="{{url('assets_/flot/jquery.flot.stack.js')}}"></script>
<script src="{{url('assets_/flot/jquery.flot.crosshair.js')}}"></script>
{{--<script src="{{url('assets_/flot/jquery.flot.tooltip.min.js')}}"></script>--}}
<script src="{{url('assets_/sparkline/jquery.sparkline.min.js')}}"></script>


<script type="text/javascript" src="{{url('assets_/chosen-bootstrap/chosen.jquery.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets_/bootstrap-inputmask/bootstrap-inputmask.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets_/jquery-tags-input/jquery.tagsinput.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets_/jquery-pwstrength/jquery.pwstrength.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets_/bootstrap-fileupload/bootstrap-fileupload.min.js')}}"></script>
<script type="text/javascript"
        src="{{url('assets_/bootstrap-duallistbox/duallistbox/bootstrap-duallistbox.js')}}"></script>
<script type="text/javascript" src="{{url('assets_/dropzone/downloads/dropzone.min.js')}}"></script>
<script type="text/javascript" src="{{url('assets_/bootstrap-timepicker/js/bootstrap-timepicker.js')}}"></script>
<script type="text/javascript" src="{{url('assets_/clockface/js/clockface.js')}}"></script>
<script type="text/javascript" src="{{url('assets_/bootstrap-colorpicker/js/bootstrap-colorpicker.js')}}"></script>
<script type="text/javascript" src="{{url('assets_/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
<script type="text/javascript" src="{{url('assets_/bootstrap-daterangepicker/date.js')}}"></script>
<script type="text/javascript" src="{{url('assets_/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
<script type="text/javascript" src="{{url('assets_/bootstrap-switch/static/js/bootstrap-switch.js')}}"></script>
<script type="text/javascript" src="{{url('assets_/bootstrap-wysihtml5/wysihtml5-0.3.0.js')}}"></script>
<script type="text/javascript" src="{{url('assets_/bootstrap-wysihtml5/bootstrap-wysihtml5.js')}}"></script>
<script type="text/javascript" src="{{url('assets_/ckeditor/ckeditor.js')}}"></script>

{{--<script type="text/javascript" src="{{url('assets_/data-tables/jquery.dataTables.js')}}"></script>--}}

<script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
<!--<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.js"></script>-->
<script type="text/javascript" src="{{url('assets_/data-tables/bootstrap3/dataTables.bootstrap.js')}}"></script>

<!--flaty scripts-->
<script src="{{url('js/flaty.js')}}"></script>
<script src="{{url('js/flaty-demo-codes.js')}}"></script>
<script>
    $('#mySwitch').on('switch-change', function (e, data) {
        var $el = $(data.el)
            , value = data.value;
        // console.log(value);
        if (value == false) {
            $('input[name="featured"]').val(0);
        } else {
            $('input[name="featured"]').val(1);
        }
        // console.log(e, $el, value);
    });
</script>
<script>
    $(function () {
        $("audio").on("play", function () {
            $("audio").not(this).each(function (index, audio) {
                audio.pause();
            });
        });
    });
    $('.date-picker').datepicker({
        format: 'dd-mm-yyyy'
    });
</script>
<script>
    $(document).ready(function () {
        // $('#example').DataTable();
        $('#ui-datepicker-div').remove();
        var el = $('.chosen-rtl');
        if ("<?php echo App::getLocale(); ?>" == "ar") {
            el.chosen({
                rtl: true,
                width: '100%'
            });
        } else {
            el.addClass("chosen");
            el.removeClass("chosen-rtl");
            $(".chosen").chosen();
        }
    });
</script>
<script>

    var selected_list = [];
    var checker_list = [];

    function collect_selected(element) {
        var id;
        if (!element.value) {
            id = element;
        } else {
            id = element.value;
        }

        if (checker_list[id]) {
            var index = selected_list.indexOf(id);
            selected_list.splice(index, 1);
            checker_list[id] = false;
        } else {
            if (!selected_list.includes(id)) {
                selected_list.push(id);
                checker_list[id] = true;
            }
        }
    }

    function clear_selected() {
        selected_list = [];
        checker_list = [];
    }

</script>

<script>
    var check = false;

    function select_all(table_name, has_media) {
        if (!check) {
            $('.select-all-karim').prop("checked", !check);
            $.get("{{url('get_table_ids?table_name=')}}" + table_name, function (data, status) {
                data.forEach(function (item) {
                    collect_selected(item.id);
                });
            });
            check = true;
        } else {
            $('.select-all-karim').prop("checked", !check);
            check = false;
            clear_selected();
        }
    }

</script>

<script>


    function delete_selected(table_name) {
        var form = document.createElement("form");
        var element = document.createElement("input");
        var tb_name = document.createElement("input");
        var csrf = document.createElement("input");
        csrf.name = "_token";
        csrf.value = "{{ csrf_token() }}";
        csrf.type = "hidden";

        form.method = "POST";
        form.action = "{{url('delete_multiselect')}}";

        element.value = selected_list;
        element.name = "selected_list";
        element.type = "hidden";

        tb_name.value = table_name;
        tb_name.name = "table_name";
        tb_name.type = "hidden";

        form.appendChild(element);
        form.appendChild(csrf);
        form.appendChild(tb_name);

        document.body.appendChild(form);

        form.submit();
    }

    var initChosenWidgets = function () {
        $(".chosen").chosen();
    };
</script>
<script>
    /*
    $(document).ready(function() {
        $('#example').DataTable();



    } );
    */
</script>


<script>
    $(document).ready(function () {
        $('#example').DataTable({
            //  'lengthMenu': [5, 10, 15, 20, 25, 50, 'All']
            "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
            "pageLength": 50
        });
    });
</script>


@yield('script')

</body>
</html>

@extends('template')
@section('content')
        <!-- BEGIN Tiles -->
        <div class="row">
            <div class="col-md-7">
                <div class="row">
                    <div class="col-md-6">
                        <a class="tile tile-pink" data-stop="500" href="{{url('products')}}">
                            <div class="img img-center">
                                <i class="fa fa-qrcode"></i>
                                <p class="title text-center">+{{$products}}</p>
                                <p class="title text-center">Products</p>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a class="tile tile-blue" data-stop="700" href="{{url('operators')}}">
                            <div class="img img-center">
                            <i class="fa fa-star align-left"></i>
                                <p class="title text-center">+{{$operators}}</p>
                                <p class="title text-center">Operators</p>
                            </div>
                        </a>

                    </div>

                </div>

            </div>
            <div class="col-md-3">
                <a class="tile tile-red" data-stop="900" href="{{url('brands')}}">
                    <div class="img img-center">
                        <i class="fa fa-barcode"></i>
                        <p class="title text-center">+{{$brands}}</p>
                        <p class="title text-center">Brands</p>
                    </div>
                </a>
            </div>
        </div>
@stop
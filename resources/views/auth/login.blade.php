<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Aflamy System - Login</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

    <!--base css styles-->
    <link rel="stylesheet" href="{{url('assets_/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('assets_/font-awesome/css/font-awesome.min.css')}}">

    <!--page specific css styles-->

    <!--flaty css styles-->
    <link rel="stylesheet" href="{{url('css/flaty.css')}}">
    <link rel="stylesheet" href="{{url('css/flaty-responsive.css')}}">

    <link rel="shortcut icon" href="img/favicon.png">
    <style>
        .btn.btn-primary{
            width: 100%;

        }
    </style>
</head>
<body class="login-page">

<!-- BEGIN Main Content -->
<div class="login-wrapper">

    <form method="POST" action="{{ route('login') }}">
        @csrf
        <h3>Login to your account</h3>
        @include('errors')
        <div class="form-group">
            <div class="controls">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email"
                       value="{{ old('email') }}" required autocomplete="email" placeholder="{{ __('Email') }}"
                       autofocus>
            </div>
        </div>

        <div class="form-group">
            <div class="controls">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror"
                       name="password" required placeholder="{{ __('Password') }}" autocomplete="current-password">
            </div>
        </div>


        <div class="form-group">
            <div class="controls">
                <button type="submit" class="btn btn-primary">
                    {{ __('Login') }}
                </button>
            </div>
        </div>
        <hr/>
        <p class="clearfix">
            <a href="{{url('password/email')}}" class="goto-forgot pull-left">Forgot Password?</a>
            <!--a href="#" class="goto-register pull-right">Sign up now</a-->
        </p>
    </form>

    <!-- END Login Form -->

</div>
<!-- END Main Content -->


<!--basic scripts-->
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<script>window.jQuery || document.write('<script src="{{url('assets_/jquery/jquery-2.1.4.min.js')}}"><\/script>')</script>
<script src="{{url('assets_/bootstrap/js/bootstrap.min.js')}}"></script>

<script type="text/javascript">
    function goToForm(form) {
        $('.login-wrapper > form:visible').fadeOut(500, function () {
            $('#form-' + form).fadeIn(500);
        });
    }

    $(function () {
        $('.goto-login').click(function () {
            goToForm('login');
        });
        $('.goto-forgot').click(function () {
            goToForm('forgot');
        });
        $('.goto-register').click(function () {
            goToForm('register');
        });
    });
</script>
</body>
</html>

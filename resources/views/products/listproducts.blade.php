
@extends('template')
@section('page_title')
List of videos
@stop
@section('content')
<div id="main-content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-black">
                <div class="box-title">
                    <h3><i class="fa fa-table"></i> videos Table</h3>
                    <div class="box-tool">
                        <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                        <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    <div class="btn-toolbar pull-right">
                        <div class="btn-group">
                            <a class="btn btn-circle show-tooltip" title="" href="{{url('products/create')}}" data-original-title="Add new record"><i class="fa fa-plus"></i></a>
                        </div>
                    </div>
                    <br><br>
                    <div class="table-responsive" style="border:0">
                        <table class="table table-advance" id="table">

                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Video banner</th>
                                    {{--  <th>Video</th>--}}
                                    <th>Category</th>
                                    <th>Brand</th>
                                    <th class="visible-md visible-lg" style="width:130px">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)

                                <tr class="table-flag-orange">
                                    <td>{{$product->id}}</td>
                                    <td><a href="{{url('products/'.$product->id.'/posts')}}">{{$product->title}}</a></td>
                                    <td>
                                        <img src="{{$product->product_image}}" class="img-circle" width="160px" height="160px">
                                        {{--  </td>
                                            <td>
                                                <video id="trackId" width="70%"  controls>
                                                    <source src="{{url($product->video)}}">
                                        </video>
                                    </td>--}}
                                    <td>{{$product->category->category_name}}</td>
                                    <td>{{$product->brand->brand_name}}</td>
                                    {!! Form::close() !!}
                                    <td class="visible-md visible-lg">
                                        <div class="btn-group">
                                            <a class="btn btn-sm show-tooltip" title="" href="{{url('products/'.$product->id.'/edit')}}" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                            {!! Form::open(["url"=>"products/$product->id","method"=>"delete","onsubmit" => "return ConfirmDelete()"]) !!}
                                            {!! Form::button('<a class="show-tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>',['type'=>'submit','class'=>'btn btn-sm btn-danger show-tooltip']) !!}
                                            {!! Form::close() !!}
                                            <a class="btn btn-sm btn-success show-tooltip" title="" href="{{url("posts/$product->id")}}" data-original-title="Add Post"><i class="fa fa-plus"></i></a>
                                        </div>
                                    </td>
                                </tr>

                                @endforeach

                            </tbody>
                        </table>
                    </div>
                </div></div></div></div></div>
@stop
@section('script')
<script>
    $('#product').addClass('active');
    $('#product-index').addClass('active');
    $(document).ready(function () {
        $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            ajax: "{!! url('products/allData') !!}",
            columns: [
                {data: 'id'},
                //{data: 'title', name:'title'},
                {data: 'product_image'},           
                {data: 'category', name:'category.category_name'},
                {data: 'brand', name:'brand.brand_name'},            
                {data: 'action', searchable: false}
            ]
            , 'order' : [[0, 'asc']]
            , "pageLength": {{get_pageLength()}}

        });
    });
</script>
@stop

<div class="btn-group">
    <a class="btn btn-sm show-tooltip" title="" href="{{url('products/'.$product->id.'/edit')}}" data-original-title="Edit"><i class="fa fa-edit"></i></a>
    {!! Form::open(["url"=>"products/$product->id","method"=>"delete","onsubmit" => "return ConfirmDelete()"]) !!}
    {!! Form::button('<a class="show-tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>',['type'=>'submit','class'=>'btn btn-sm btn-danger show-tooltip']) !!}
    {!! Form::close() !!}
    <a class="btn btn-sm btn-success show-tooltip" title="" href="{{url("posts/$product->id")}}" data-original-title="Add Post"><i class="fa fa-plus"></i></a>
</div>
@extends('template')
@section('page_title')
    Products
@stop
@section('content')
@include('errors')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="fa fa-bars"></i>Add Product Form</h3>
                    <div class="box-tool">
                        <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                        <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                    </div>
                </div>
                <div class="box-content">

                    {!! Form::open(['url'=>'products','class'=>'form-horizontal','files'=>'true']) !!}
                        
                        <div class="form-group">

                            <label class="col-sm-3 col-lg-2 control-label">Name</label>
                            <div class="col-sm-9 col-lg-10 controls">
                                {!! Form::text('title',null,['class'=>'form-control','required'=>'required']) !!}
                                <span class="help-inline">Product Name</span><br><br>
                            </div>

                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Video Image</label>
                            <div class="col-sm-9 col-lg-10 controls">
                                <div class="fileupload fileupload-new" data-provides="fileupload">
                                    <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                    </div>
                                    <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                    <div>
                                                   <span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span>
                                                   <span class="fileupload-exists">Change</span>
                                                       {!! Form::file('product_image',['required'=>'required','accept'=>'image/*']) !!}</span>
                                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                    </div>
                                </div>
                                <span class="label label-important">NOTE!</span>
                                <span>Only extension supported jpg, png, and jpeg</span>
                            </div>
                        </div>

                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Video File</label>
                        <div class="col-sm-9 col-lg-10 controls">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                                   <span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span>
                                                   <span class="fileupload-exists">Change</span>
                                                       {!! Form::file('video',['required'=>'required','accept'=>'video/*']) !!}</span>
                                    <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>
                            <span class="label label-important">NOTE!</span>
                            <span>Only videos allowed</span>
                        </div>
                    </div>


                    <div class="form-group">
                                <label class="col-sm-3 col-lg-2 control-label">Select Category</label>
                                <div class="col-sm-9 col-sm-10 controls">
                                {!! Form::select('category_id',$categories,null,['class'=>'form-control chosen','required'=>'required']) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 col-lg-2 control-label">Select Brand</label>
                                <div class="col-sm-9 col-sm-10 controls">
                                    {!! Form::select('brand_id',$brands,null,['class'=>'form-control chosen','required'=>'required']) !!}
                                </div>
                            </div>


                        <div class="form-group">
                            <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                {!! Form::button('<i class="fa fa-check"></i> Save' ,['type'=>'submit','class'=>'btn btn-primary']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@stop

@section('script')
    <script>
        $('#product').addClass('active');
        $('#product-index').addClass('active');
    </script>
@stop
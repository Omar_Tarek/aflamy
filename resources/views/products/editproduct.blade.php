@extends('template')
@section('page_title')
    Videos
@stop
@section('content')
    @include('errors')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="fa fa-bars"></i>Edit Video Form</h3>
                    <div class="box-tool">
                        <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                        <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    {!! Form::open(["url"=>"products/$product->id","class"=>"form-horizontal","files"=>"true","method"=>"patch"]) !!}
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Video title</label>
                        <div class="col-sm-9 col-lg-10 controls">
                            <div class="form-group">
                                {!! Form::text('title',$product->title,["class"=>"form-control", "required"=>"required"]) !!}
                            </div>

                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                                    <img src="{{url($product->product_image)}}" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                               <span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span>
                                               <span class="fileupload-exists">Change</span>
                                               {!! Form::file('product_image') !!}</span>
                                </div>
                            </div>
                            <span class="label label-important">NOTE!</span>
                            <span>Only extension supported jpg, png, and jpeg</span>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Play video</label>
                        <div class="col-sm-9 col-lg-10 controls">
                            <video width="520" height="340" controls  poster="{{url($product->product_image)}}"  >
                                <source src="{{url($product->video)}}">
                                Your browser does not support the video tag.
                            </video>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Video File</label>
                        <div class="col-sm-9 col-lg-10 controls">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                                   <span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span>
                                                   <span class="fileupload-exists">Change</span>
                                                       {!! Form::file('video',['accept'=>'video/*']) !!}</span>
                                    <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                                </div>
                            </div>
                            <span class="label label-important">NOTE!</span>
                            <span>Only videos allowed</span>
                        </div>
                    </div>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Select Category</label>
                            <div class="col-sm-9 col-sm-10 controls">
                                {!! Form::select('category_id',$categories,$product->category->id,['class'=>'form-control chosen',"required"=>"required"]) !!}
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Select Brand</label>
                            <div class="col-sm-9 col-sm-10 controls">
                                {!! Form::select('brand_id',$brands,$product->brand->id,['class'=>'form-control chosen',"required"=>"required"]) !!}
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                            {!! Form::button('<i class="fa fa-check"></i> Save' ,['type'=>'submit','class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
           </div></div>
    @stop
@section('script')
    <script>
        $('#product').addClass('active');
        $('#product-index').addClass('active');
    </script>
@stop
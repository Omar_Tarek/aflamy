@extends('template')
@section('page_title')
    Categories
@stop
@section('content')
    @include('errors')
    <div class="row">
        <div class="col-md-12">

            <div class="row">
                <div class="col-md-12">
                    <div class="box box-black">
                        <div class="box-title">
                            <h3><i class="fa fa-table"></i>Categories Table</h3>
                            <div class="box-tool">
                                <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                                <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                            </div>
                        </div>
                        <div class="box-content">
                            <div class="btn-toolbar pull-right">
                                <div class="btn-group">
                                    <a class="btn btn-circle show-tooltip" title="" href="{{url('categories/create')}}" data-original-title="Add new record"><i class="fa fa-plus"></i></a>
                                </div>
                            </div>
                            <br><br>
                            <div class="table-responsive" style="border:0">
                                <table id="example" class="table table-striped dt-responsive" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>id</th>
                                        <th>Category Name</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($categories as $category)
                                                <tr>
                                                    <td>{{$category->id}}</td>
                                                    <td>
                                                        <label>{{$category->category_name}}</label>
                                                    </td>
                                                    <td class="visible-md visible-lg">
                                                        <div class="btn-group">
                                                            {!! Form::open(["url"=>"categories/$category->id","method"=>"delete","onsubmit" => "return ConfirmDelete()"]) !!}
                                                            <a class="btn btn-sm show-tooltip" title="" href="{{url("categories/$category->id/edit")}}" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                                            {!! Form::button('<a class="show-tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>',['type'=>'submit','class'=>'btn btn-sm btn-danger show-tooltip']) !!}
                                                            {!! Form::close() !!}
                                                        </div>
                                                    </td>
                                                </tr>
                                     @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

@stop

@section('script')
    <script>
        $('#category').addClass('active');
        $('#category-manage').addClass('active');
    </script>
@stop
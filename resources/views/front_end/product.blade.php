@extends('front_template')
@section('front_content')
        <div id="page">
            @extends('top_navbar')
            <div class="content-container">

                <div class="pages home-page">
                    <div class="maleo-card margin-bottom animated fadeInRight">

                            <div class="featured-slider">
                             @foreach($ads as $ad)
                                <div class="featured-item">
                                        <a class="thumbnail" href="{{$ad->link}}"><img src="{{url($ad->image)}}"   alt=""></a>
                                </div>
                                       @endforeach
                            </div>

                        </div>
                 </div>



                <!-- HERE IS CONTENTS -->
                <div class="pages product-page">
<!-- ================== Single-film ===================== -->
                    <div class="maleo-card product-single animated fadeInUp">
                    <div class="product-price"><h3>{{$product[0]->title}}</h3></div>
                        <div class="thumb">
                            <video width="100%"  controls  controlsList="nodownload" poster="{{url($product[0]->product_image)}}"    >
                                <source src="{{url($product[0]->video)}}">
                            </video>
                        </div>
                        <!-- Product header contain product name, price & rating-->
                        <div class="product-header">
                            @if(isset($product[0]->operator_id))
                                <div class="product-price"></div>
                                <div class="product-price"><a href="{{'get_category?category_id='.$product[0]->category_id."&operator_id=".$product[0]->operator_id}}" class="product-price_reduced">{{$product[0]->category_name}}</a></div>
                            @else
                                <div class="product-price"></div>
                                <div class="product-price"><a href="{{'get_category?category_id='.$product[0]->category_id}}" class="product-price_reduced">{{$product[0]->category_name}}</a></div>
<!--                                 <div class="product-price"><a href="" class="product-price_reduced">اسم المنتج</a></div> -->
                            @endif
                        </div>
                    </div>
<!-- ================== Single-film ===================== -->

                </div>
                <!-- //HERE IS CONTENTS -->

                @if(count( $today_movies )> 0 )
                <div class="poster_film">
                    <h4>فيدهات اليوم</h4>
                    @foreach($today_movies as $today_movie)
                        <div class="single_poster">
                         <a href="{{'get_product?product_id='.$today_movie->product_id.'&operator_id='.$today_movie->operator_id}}"  > <img src="{{url($today_movie->product->product_image)}}" alt="poster">  </a>
                            <h5>   <a href="{{'get_product?product_id='.$today_movie->product_id.'&operator_id='.$today_movie->operator_id}}" class="product-price_reduced"> {{$today_movie->product->title}} </a>      --  <a href="{{'get_category?category_id='.$today_movie->product->category_id}}" class="product-price_reduced"> {{$today_movie->product->category->category_name}}</a> </h5>

                        </div>
                    @endforeach

                </div>
                @endif


                @if(count( $comming_soons ) > 0 )
                 <div class="poster_film">
                 <h4>قريبا</h4>
                    @foreach($comming_soons as $movie)
                    <div class="single_poster">
                        <img src="{{url($movie->product->product_image)}}" alt="poster">
                        <h5>{{$movie->product->title}}</h5>
                    </div>
                    @endforeach

                </div>
                @endif

            </div>
        </div>
@stop

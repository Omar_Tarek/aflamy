@extends('front_template')
@section('front_content')

<style type="text/css">
    .myaccount {
        padding-top: 50px;
    }

    .maleo-card_title {
        padding: 30px;
        font-size: 15px !important;
    }

    .maleo-card_title span {
        float: right;
        padding-right: 10px;
        color: #00738e;

    }

    .text-center {
        text-align: left;
    }
</style>

<div id="page">
    @extends('top_navbar')
    <div class="content-container">
        <!-- HERE IS CONTENTS -->

        <div class="pages login-page myaccount">
            <div class="maleo-card signup animated fadeInUp">
                <h3 class="maleo-card_title big-title text-center">
                    <span>رقم الهاتف  :  </span>{{Session::get('MSISDN')}}
                </h3>
                   <h3 class="maleo-card_title big-title text-center">
                    <span>الشبكة  :  </span>{{$operatorName }}
                  </h3>
                
                <h3 class="maleo-card_title big-title text-center">
                    <span>تاريخ الاشتراك :  </span><?php echo date("Y-m-d", strtotime($Msisdn_found->updated_at)) ?>
                </h3>
                <h3 class="maleo-card_title big-title text-center">
                    <span>تاريخ التجديد :  </span><?php echo date("Y-m-d", strtotime("+ 1 day")) ?></h3>
                {{--<h3 class="maleo-card_title big-title text-center"><span>تاريخ الانتهاء :  </span>--------</h3>--}}
                <h3 class="maleo-card_title big-title text-center"><span>اسم الخدمة  :  </span>افلامي </h3>
                <h3 class="maleo-card_title big-title text-center"><span>نوع الخدمة :  </span>يومية</h3>
                <div class="form-content">
                    @if(isset($categories[0]) && isset($categories[0]->products[0]) && isset($categories[0]->products[0]->operator_id))
                    <a class="btn-large block margin-bottom" href="{{url('/front?operator_id='.$categories[0]->products[0]->operator_id)}}"  >الصفحه الرئيسيه</a>
                    @else
                    <a class="btn-large block margin-bottom" href="{{url('/front')}}">الصفحه الرئيسيه</a>
                    @endif

                    @if($_REQUEST['operator_id'] == 2 )
                     <a class="btn-large block margin-bottom" href="{{url('unsubOroodo?msisdn='.Session::get('MSISDN'))}}">لالغاء الاشتراك</a>
                    @elseif( $_REQUEST['operator_id'] == 3)
                     <a class="btn-large block margin-bottom" href="{{url('unsubZainJordan?msisdn='.Session::get('MSISDN'))}}">لالغاء الاشتراك</a>
                    @endif


                   
                </div>
            </div>
        </div>


        {!! Form::close() !!}
        <!-- //HERE IS CONTENTS -->
    </div>
</div>
@stop

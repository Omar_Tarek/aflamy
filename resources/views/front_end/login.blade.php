@extends('front_template')
@section('front_content')
    <div id="page">
        @extends('top_navbar')
        <div class="content-container" style="margin-top : 30px !important ;">
                @if(Session::has('success'))
                    <div class="alert alert-success alert-dismissible fade in" style="font-size: 12px;">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                       {{ Session::get('success')}}
                    </div>
                @elseif(Session::has('failed'))
                    <div class="alert alert-danger alert-dismissible fade in">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        {{ Session::get('failed')}}
                    </div>
                    
                @endif



                <!-- HERE IS CONTENTS -->
                {!! Form::open(['id'=>'login-form form', 'url'=>url('/loginValidate') ]) !!}
                <div class="pages login-page">
                    <div class="maleo-card signup animated fadeInUp">
                        <h3 class="maleo-card_title big-title text-center">تسجيل دخول</h3>
                        <div class="form-content">
                            <p class="app-desc">من فضلك! أدخل رقم التليفون. ثم أضغط دخول.</p>
                            <div class="input-field with-icon"><span class="icon"><i class="fa fa-phone"></i></span>
                                <input id="login" name="msisdn" value="{{$msisdn}}" type="number" placeholder="رقم التليفون" required>
                                <input type="hidden" name="operator_prefix" value="{{$prefix}}"   >
                           @if($operator_id == 2 )
                                <span class="operator_num"><i> 965+ </i></span></div>
                                @elseif($operator_id == 3) 
                                <span class="operator_num"><i> 962+ </i></span></div>
                                @endif 
                                
                                
                            @if(Session::has('prev_url') &&  Session::get('prev_url') !="")
                                <input type="hidden" name="prev_url" value="{{ Session::get('prev_url')}}">
                            @endif





                            <button class="btn-large block margin-bottom" type="submit">دخول</button><span class="app-desc">ليس لديك حساب؟ <a class="primary-text" href="{{url('')}}">تسجيل جديد</a></span>
                        </div>
                    </div>
                </div>
            {!! Form::close() !!}
                <!-- //HERE IS CONTENTS -->
            </div>
    </div>
@stop

@extends('front_template')
@section('front_content')
        <div id="page">
            @extends('top_navbar')
            <div class="content-container">
                <!-- HERE IS CONTENTS -->
                <div class="pages brand-page">

                    @foreach($products as $index=>$product)
                        @if($index==0)
                            <h1 class="page-title margin-bottom animated fadeIn">{{$product->brand_name}}</h1>
                        @endif

                        <div class="categories-list">
                            <div class="maleo-card maleo-blog_small margin-bottom_low animated">
                                @if(isset($product->operator_id))
                                    <a class="maleo-blog_thumb" href="{{url('get_product?product_id='.$product->product_id.'&operator_id='.$product->operator_id)}}"><img src="{{url($product->product_image)}}" alt=""></a>
                                @else
                                    <a class="maleo-blog_thumb" href="{{url('get_product?product_id='.$product->product_id)}}"><img src="{{url($product->product_image)}}"  alt=""></a>
                                @endif
                                <div class="maleo-blog_rightcontent">
                                    @if(isset($product->operator_id))
                                        <h3 class="blog-title"><a href="{{url('get_product?product_id='.$product->product_id.'&operator_id='.$product->operator_id)}}">{{$product->prod_title}}</a></h3>
                                        <h3 class="blog-title"><a href="{{url('get_category?category_id='.$product->category_id.'&operator_id='.$product->operator_id)}}">{{$product->category_name}}</a></h3>
                                    @else
                                        <h3 class="blog-title"><a href="{{url('get_product?product_id='.$product->product_id)}}">{{$product->prod_title}}</a></h3>
                                        <h3 class="blog-title"><a href="{{url('get_category?category_id='.$product->category_id)}}">{{$product->category_name}}</a></h3>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <!-- //HERE IS CONTENTS -->
            </div>
        </div>
        <!-- //HERE IS CONTENTS  no chnage -->
@stop
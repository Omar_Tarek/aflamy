@extends('front_template')
@section('front_content')
    <div id="page">
        @extends('top_navbar')
            <div class="content-container">
                <!-- HERE IS CONTENTS -->
                <div class="pages signup-page">
                    <div class="maleo-card signup animated fadeInUp">
                        <h3 class="maleo-card_title big-title text-center">تسجيل جديد</h3>
                        <div class="form-content">
                            <p class="app-desc">من فضلك! أدخل رقم التليفون. ثم أضغط تسجيل.</p>
                            <div class="input-field with-icon"><span class="icon"><i class="fa fa-phone"></i></span><input id="signup" type="text" placeholder="رقم التليفون"></div>
                            <button class="btn-large block margin-bottom" type="button">تسجيل</button><span class="app-desc">لدي حساب؟ <a class="primary-text" href="#"> دخول</a></span>
                        </div>
                    </div>
                </div>
                <!-- //HERE IS CONTENTS -->
            </div>
    </div>
@stop
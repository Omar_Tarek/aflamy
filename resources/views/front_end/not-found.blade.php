@extends('front_template')
@section('front_content')
        <div id="page">
            @extends('top_navbar')
            <div class="content-container">
                <!-- HERE IS CONTENTS -->
                <div class="pages not-found-page">
                    <div class="not-found animated fadeIn">
                        <h2>404</h2>
                        <p>محتوي هذه الصفحة غير موجودة. بأمكانك أستخدام مربع البحث.</p>
                        <div class="searchform"><input type="text" placeholder="بحث..."></div>
                        <div class="btn-to-home"><a href="{{url('index')}}" class="btn"><i class="fa fa-home"></i> الرجوع للرئيسية</a></div>
                    </div>
                </div>
                <!-- //HERE IS CONTENTS -->
            </div>
          {{--  <div class="footer">
                <div class="social-footer">
                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                </div>
                <div class="copyright">Copyright © 2017 – iVAS</div>
            </div>--}}
            <div id="to-top" class="main-bg"><i class="fa fa-chevron-up"></i></div>
        </div>

@stop
<!DOCTYPE html>
<html lang="en-US">


<head>
    <title>
        @if(!empty($settings[0]->value))
        {{$settings[0]->value}}
        @else
        خدمة أفلامي - برعاية ايفاز
        @endif
    </title>



    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="x-ua-compatible">
    <meta content="initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" name="viewport">
    <link href="https://fonts.googleapis.com/css?family=Lateef" rel="stylesheet">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="yes" name="apple-touch-fullscreen">
    <link rel="stylesheet" type="text/css" href="{{url('css/bootstrap.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/owl.carousel.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('css/style.css')}}">
    <!-- Favicon -->
    <link rel="icon" type="image/png" href="images/4.png">

    <style>
        .phone_subscriber {
        margin-right: 11%;
        font-weight: bold;
        display: block;
    }

</style>


</head>

<body>



    <div class="loader-app">
        <!--<h3>وفرلى</h3>-->
        @if(file_exists($settings[1]->value))
        <img src="{{url($settings[1]->value)}}" alt="وفرلى">
        @else
        <img src="{{url('images/logo.png')}}" alt="وفرلى">

        @endif
        <div class="ld ld-ring ld-spin-fast huge"></div>
    </div>
    <div id="main">
        <div id="slide-out-right" class="side-nav">
            <ul class="collapsible" data-collapsible="accordion">

                <span class="phone_subscriber">
                    @if(Session::Has('MSISDN') && Session::get('MSISDN')!="")
                    {{ Session::get('MSISDN') }}
                    @endif


                </span>

                @if(isset($categories[0]) && isset($categories[0]->products[0]) && isset($categories[0]->products[0]->operator_id))
                <li class="current-item"><a href="{{url('/front?operator_id='.$categories[0]->products[0]->operator_id)}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
                @if(Session::Has('MSISDN') && Session::get('MSISDN')!="")
                <li><a href="{{url('/myaccount?operator_id='.$categories[0]->products[0]->operator_id)}}"><i class="fa fa-briefcase"></i> بياناتي</a></li>
                @endif
                @else
                <li class="current-item"><a href="{{url('/front')}}"><i class="fa fa-dashboard"></i> الرئيسية</a></li>
                @if(Session::Has('MSISDN') && Session::get('MSISDN')!="")
                <li><a href="{{url('/myaccount')}}"><i class="fa fa-briefcase"></i> بياناتي</a></li>
                @endif
                @endif

                <li>
                    <div class="collapsible-header active"><i class="fa fa-shopping-bag"></i> التصنيفات <span class="angle-left fa fa-angle-left"></span></div>
                    <div class="collapsible-body">
                        <ul>
                            @foreach($categories as $category)
                            @if($category->products[0]->operator_id==null)
                            <li><a href="{{url('get_category?category_id='.$category->id)}}">{{$category->category_name}}</a></li>
                            @else
                            <li><a href="{{url('get_category?category_id='.$category->id."&operator_id=".$category->products[0]->operator_id)}}">{{$category->category_name}}</a></li>
                            @endif

                            @endforeach
                        </ul>
                    </div>
                </li>
                <li>
                    <div class="line-separator"></div>

                </li>


                 @if( $_REQUEST['operator_id'] == 3)
                <li><a href="{{url('terms?operator_id='.$_REQUEST['operator_id'])}}"><i class="fa fa-edit"></i>  الارشادات</a></li>
                @endif

                @if(Session::Has('MSISDN') && Session::get('MSISDN')!="")
                <li><a href="{{url('logout')}}"><i class="fa fa-sign-out"></i> خروج</a></li>

                @if($_REQUEST['operator_id'] == 2 )
                <li><a href="{{url('unsubOroodo?msisdn='.Session::get('MSISDN'))}}"><i class="fa fa-braille"></i> الغاء الاشتراك</a></li>
                @elseif( $_REQUEST['operator_id'] == 3)
                <li><a href="{{url('unsubZainJordan?msisdn='.Session::get('MSISDN'))}}"><i class="fa fa-braille"></i> الغاء الاشتراك</a></li>
                @endif

                @else
                <li><a href="{{url('')}}"><i class="fa fa-sign-in"></i> دخول</a></li>
                <li><a href="{{url('')}}"><i class="fa fa-user"></i> تسجيل</a></li>

                @endif


            </ul>
        </div>

        @yield('front_content')

        <div class="footer">
            <div class="social-footer">
                <a href="{{$settings[2]->value}}" class="facebook"><i class="fa fa-facebook"></i></a>
                <a href="{{$settings[3]->value}}" class="twitter"><i class="fa fa-twitter"></i></a>
            </div>
            <div class="copyright">Copyright © {{\Carbon\Carbon::now()->year}} – iVAS</div>
        </div>
        <div id="to-top" class="main-bg"><i class="fa fa-chevron-up"></i></div>

    </div>


    @yield('script')
    <script type="text/javascript" src="{{url('js/jquery.js')}}"></script>
    <script type="text/javascript" src="{{url('js/owl.carousel.js')}}"></script>
    <script type="text/javascript" src="{{url('js/materialize.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.mixitup.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/slick.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/jquery.swipebox.min.js')}}"></script>
    <script type="text/javascript" src="{{url('js/custom.js')}}"></script>
</body>




</html>

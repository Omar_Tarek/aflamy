@extends('template')
@section('page_title')
Posts
@stop
@section('content')
@include('errors')
<div class="row">
    <div class="col-md-12">
        <div class="box box-black">
            <div class="box-title">
                <h3><i class="fa fa-table"></i>Posts Table</h3>
                <div class="box-tool">
                    <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                    <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div class="box-content">
                <div class="table-responsive" style="border:0">
                    <table class="table table-advance" id="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Product title</th>
                                <th>Operators</th>
                                <th>Show Date</th>
                                <th>Active</th>
                                <th>Featured</th>
                                <th>post link</th>
                                <th class="visible-md visible-lg" style="width:130px">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


@stop
@section('script')
<script>
    $('#post').addClass('active');
    $('#post-manage').addClass('active');
    $(document).ready(function () {
        $('#table').DataTable({
            "processing": true,
            "serverSide": true,
            @if (strpos(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), 'products') !== false)
                ajax: '{!! url("posts/allData?productID=$id") !!}',
            @else
                ajax: "{!! url('posts/allData') !!}",
            @endif
            columns: [
            {data: 'id'},
            {data: 'product', name:'product.title'},
            {data: 'operator', name:'operator.name'},
            {data: 'show_date'},
            {data: 'active' , name:'active'},
            {data: 'featured' , name:'featured'},
            {data: 'link'},
            {data: 'action', searchable: false}

            ]
            , 'order' : [[0, 'asc']]
            , "pageLength": {{get_pageLength()}}

        });
    });
</script>
@stop
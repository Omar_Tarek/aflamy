@extends('template')
@section('page_title')
    Edit Post
@stop
@section('content')
    @include('errors')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="fa fa-bars"></i>Edit Post Form</h3>
                    <div class="box-tool">
                        <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                        <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    {!! Form::open(["url"=>"posts/$post->id",'class'=>'form-horizontal',"method"=>"patch","files"=>"true"]) !!}
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Products</label>
                        <div class="col-sm-9 col-lg-10 controls">
                            {!! Form::select('product_id',$products,$post->product_id,['class'=>'form-control chosen','required'=>'required']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Select Operator (s) <span class="text-danger"> *</span></label>
                        <div class="col-sm-9 col-lg-10 controls">
                            {!! Form::select('operator_id',$operators,$post->operator->id,['class'=>'form-control chosen', 'required'=>'required']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Show at</label>
                        <div class="col-sm-5 col-lg-3 controls">
                            <div class="input-group"  data-date-format="yyyy-mm-dd" >
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                {!! Form::text('show_date',date('d-m-Y',strtotime($post->show_date)),['class'=>'form-control date-picker','required' => 'required', 'size'=>'16']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Show Or Not</label>
                        <div class="col-sm-9 col-lg-10 controls">
                            <label class="radio">
                                {!! Form::select('active',array(1=>'Show',0=>'Do not show'),$post->active ) !!}
                            </label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Featured Or Not</label>
                        <div class="col-sm-9 col-lg-10 controls">
                            <label class="radio">
                                <div id="mySwitch" class="make-switch switch-large" data-on-label="YES" data-off-label="NO">
                                    <input type="checkbox" {{($post->featured) ? 'checked':''}}/>
                                </div>
                            </label>
                        </div>
                    </div>
                    <input type="hidden" name="featured" value="{{$post->featured}}">

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                            {!! Form::button('<i class="fa fa-check"></i> Save' ,['type'=>'submit','class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                 </div>
             </div>
        </div>
    </div>
@stop
@section('script')
    <script>
        $('#post').addClass('active');
        $('#post-manage').addClass('active');
    </script>
@stop
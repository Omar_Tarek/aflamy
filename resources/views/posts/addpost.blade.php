@extends('template')
@section('page_title')
    Add Post
@stop
@section('content')
    @include('errors')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="fa fa-bars"></i>Add Post Form</h3>
                    <div class="box-tool">
                        <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                        <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    {!! Form::open(['url'=>'posts','class'=>'form-horizontal']) !!}
                        {!! Form::hidden('product_id',$product->id) !!}
                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Product Image</label>
                            <div class="col-sm-9 col-lg-10 controls">
                                <img src="{{url($product->product_image)}}"  width="300px" height="300px">
                            </div>
                        </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Operators list <span class="text-danger"> *</span> </label>
                        <div class="col-sm-9 col-lg-10 controls">
                            <select data-placeholder="Select operator" class="form-control chosen" multiple="multiple" tabindex="6" name="operator_id[]">
                                @foreach($operators as $operator)
                                    <option value="{{$operator->id}}">{{$operator->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Show that post at date :</label>
                        <div class="col-sm-5 col-lg-3 controls">
                            <div class="input-group date date-picker" data-date="12-02-2012" data-date-format="YYYY-MM-DD" data-date-viewmode="years">
                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                {{--<input class="form-control date-picker" size="16" type="text" value="{{\Carbon\Carbon::now()->format('Y-m-d')}}" name="show_date">--}}
                                {!! Form::text('show_date',\Carbon\Carbon::now()->format('d-m-Y'),['class'=>'form-control date-picker','required' => 'required', 'size'=>'16']) !!}
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Do you want to show that post ?</label>
                        <div class="col-sm-9 col-sm-10 controls">
                            {!! Form::select('active',array(1=>'YES',0=>'NO'),null,['class'=>'form-control chosen','required'=>'required']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 col-lg-2 control-label">Featured Or Not</label>
                        <div class="col-sm-9 col-lg-10 controls">
                            <label class="radio">
                                <div id="mySwitch" class="make-switch switch-large" data-on-label="YES" data-off-label="NO">
                                    <input type="checkbox"   />
                                </div>
                            </label>
                        </div>
                    </div>
                    <input type="hidden" name="featured" value="0">

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                            {!! Form::button('<i class="fa fa-check"></i> Save' ,['type'=>'submit','class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@stop


<div class="btn-group">
    {!! Form::open(["url"=>"posts/$post->id","method"=>"delete","onsubmit" => "return ConfirmDelete()"]) !!}
    <a class="btn btn-sm show-tooltip" title="" href="{{url("posts/$post->id/edit")}}" data-original-title="Edit"><i class="fa fa-edit"></i></a>
    {!! Form::button('<a class="show-tooltip" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>',['type'=>'submit','class'=>'btn btn-sm btn-danger show-tooltip']) !!}
    {!! Form::close() !!}
</div>    

@extends('template')
@section('page_title')
    Settings
@stop
@section('content')
    @include('errors')
<!-- BEGIN Content -->
<div id="main-content">

    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-title">
                    <h3><i class="fa fa-bars"></i>Setting</h3>
                    <div class="box-tool">
                        <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                        <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                    </div>
                </div>
                <div class="box-content">


                    <form action="{{url('setting')}}" method="post" class="form-horizontal form-bordered form-row-stripped" enctype="multipart/form-data"  novalidate>
              			{!! csrf_field() !!}
                        <div class="form-group">
                            <label for="textfield5" class="col-sm-3 col-lg-2 control-label">setting type</label>
                            <div class="col-sm-9 col-lg-10 controls">
                                <select class="form-control chosen">
                                    <option value="1">Advanced Editor</option>
                                    <option value="2">Normal Editor</option>
                                    <option value="3">Image</option>
                                </select>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="textfield5" class="col-sm-3 col-lg-2 control-label">Key</label>
                            <div class="col-sm-9 col-lg-10 controls">
                                <input type="text" name="key" id="key" placeholder="key" class="form-control" required>
                            </div>
                          </div>

                        <div class="form-group">
                            <label class="col-sm-3 col-lg-2 control-label">Value</label>
                            <div class="col-sm-9 col-lg-10 controls" id="valueOfOption">
                                <textarea class="form-control col-md-12 ckeditor" name="TxtValue" rows="6" required></textarea>
                            </div>
                        </div>
                        <div class="form-group last">
                            <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                               <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Save</button>
                            </div>
                        </div>
                     </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@section('script')
    <script>
        var key ;
        $( document ).ready(function() {
            var someVarName = localStorage.getItem("someVarName");
            $('#key').val(someVarName);
        });
        $('select').on('change', function() {
            var myNode = document.getElementById("valueOfOption");
            myNode.innerHTML = '';
            if (this.value==1)
            {
                var someVarName = $('#key').val() ;
                localStorage.setItem("someVarName", someVarName);
                location.reload();
            }
            else if (this.value==2)
            {
               myNode.innerHTML = " <textarea class='form-control col-md-12 ' name='TxtValue' rows='6' required></textarea> ";
            }
            else if(this.value==3)
            {
                myNode.innerHTML = "<div class='col-sm-9 col-lg-10 controls'>\
                                    <div class='fileupload fileupload-new' data-provides='fileupload'>\
                                    <div class='fileupload-new img-thumbnail' style='width: 200px; height: 150px;'>\
                                    <img src='http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image' alt='' />\
                                    </div>\
                                    <div class='fileupload-preview fileupload-exists img-thumbnail' style='max-width: 200px; max-height: 150px; line-height: 20px;'></div>\
                                    <div>\
                                    <span class='btn btn-default btn-file'><span class='fileupload-new'>Select image</span>\
                                    <span class='fileupload-exists'>Change</span>\
                                    <input type='file' name='TxtValue'></span>\
                                    <a href='#' class='btn btn-default fileupload-exists' data-dismiss='fileupload'>Remove</a>\
                                    </div>\
                                    </div>\
                                    <span class='label label-important'>NOTE!</span>\
                                    <span>Only extension supported jpg, png, and jpeg</span>\
                                    </div>" ;
            }
        });
        $('#setting').addClass('active');
        $('#setting-create').addClass('active');
    </script>
@stop
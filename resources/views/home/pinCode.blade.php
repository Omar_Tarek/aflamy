@extends('home/home_template')

@section('ar_css')
    <link rel="stylesheet" type="text/css" href="{{url('home/css/bootstrap-ar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('home/css/thanks.css')}}">
@stop


@section('content')



    <div class="container">

    <div class="row">

        <div class="well col-xs-10 col-sm-10 col-md-6 col-xs-offset-1 col-sm-offset-1 col-md-offset-3">
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('success')}}
                </div>
            @elseif(Session::has('failed'))
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('failed')}}
                </div>
            @endif

            <div class="row">
                <div class="text-center">
                    <h3> ادخل الكود المرسل لرقم هاتفك لتاكيد الاشتراك في خدمة افلامي </h3>
                </div>
              
                

                    {!! Form::open(['url'=>'subscribeZainJordonPincodeConfirm','method'=>'post','class'=>'form','files'=>'true']) !!}

                    <table class="table table-hover" align="text-center">
                        <tbody>
                        <tr>
                            <td class="text-center">
                                {{--  <p class="error_message">الكود الذي ادخلته خطا يرجع اعاده الكود !!</p>--}}
                                <input type="number" name="pincode"   class="form-control" id="pincode" required>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                    <input type="hidden"  name="msisdn" value="{{$msisdn}}">


                <button type="submit" class="btn btn-success btn-lg btn-block" > اشتراك</button>

            {!! Form::close() !!}






            
            <div style="text-align: center;color: #111;font-size: 17px;font-family: auto;" >                    
                   <p class="text-center">عندالاشتراك يحصل المشترك علي عدد كبير من الافلام </p>
                    <p class="text-center">سعر الخدمة 100 فلس يوميا</p>
                       <p class="text-center">لإلغاء الاشتراك يرجى إرسال unsub A1 إلى 97970</p>
                </div> 
            </div>
        </div>
    </div>
    </div>
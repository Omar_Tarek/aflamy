<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Aflamy Landing Page</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{url('/')}}/new_landing/css/bootstrap.min.css">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{url('/')}}/new_landing/css/font-awesome.min.css">

        <!-- Owl Carousel -->
        <link rel="stylesheet" href="{{url('/')}}/new_landing/css/owl.carousel.min.css">
        <link rel="stylesheet" href="{{url('/')}}/new_landing/css/owl.theme.default.min.css">

        <!-- custom css -->
        <link rel="stylesheet" href="{{url('/')}}/new_landing/css/style.css">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->


    </head>

    <body>

        <div class="main">
            <div class="logo">
                <img src="{{url('/')}}/new_landing/images/logo-png.png" alt="">
                <span class="white_color">Aflamy By iVAS-Video </span>
            </div>


            <!-- Slider for Images -->
            <div class="strip-layer">

                <img src="{{url('/')}}/new_landing/images/strip-2.png" alt="">

                <div class="aflam">

                    <div class="owl-carousel owl-theme">

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{url('/')}}/new_landing/images/new/1.jpg" alt="">
                            </a>
                        </div>

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{url('/')}}/new_landing/images/new/2.jpg" alt="">
                            </a>
                        </div>

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{url('/')}}/new_landing/images/new/3.jpg" alt="">
                            </a>
                        </div>



                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{url('/')}}/new_landing/images/new/5.jpg" alt="">
                            </a>
                        </div>

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{url('/')}}/new_landing/images/new/6.jpg" alt="">
                            </a>
                        </div>

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{url('/')}}/new_landing/images/new/7.jpg" alt="">
                            </a>
                        </div>

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{url('/')}}/new_landing/images/new/8.jpg" alt="">
                            </a>
                        </div>

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{url('/')}}/new_landing/images/new/9.jpg" alt="">
                            </a>
                        </div>

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{url('/')}}/new_landing/images/new/10.jpg" alt="">
                            </a>
                        </div>

                    </div><!-- ./owl-carousel -->

                </div><!-- ./aflam -->

            </div>


            <!-- subscribtion div  -->
            <div class="sub">
                {!! Form::open(array('url' => 'subscribeOreedo', 'method' => 'post','id'=>'oreedo_kuwait')) !!}
                <label for="">يرجى إدخال رقم الهاتف</label>

                <select class="custom-select d-block mx-auto mb-1" id="state" required="" placeholder="اختار شيكتك" onchange="changeFormAction($(this).val())"  >
                    <option value=""> اختار شبكتك </option>
                    <option value="+965">اوريدوالكويت </option>
                    <option value="+962">زين الاردن</option>
                </select>

                <div>
                    {!! Form::number('number',$MSISDN,['class'=>'form-control num','required'=>'required','min'=>1,'id'=>'number']) !!}

                    <span class="span-code"></span>

                </div>

                <input class="subscribe" type="submit" value="دخول">

                {!! Form::close() !!}
                <p>لإلغاء الاشتراك اريدوا الكويت يرجى إرسال STOP 1 إلى 1526</p>
<!--                <p>لإلغاء الاشتراك زين الاردن يرجى إرسال STOP 1 إلى 1526</p>-->
            </div>
        </div>

        <!-- The Modal -->
        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title m-auto">مشترك بالفعل</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body text-center">
                        عزيزى المستخدم، أنت بالفعل مشترك فى الخدمة ويمكنك الاستمتاع بها من خلال الرابط المرسل سابقا الى هاتفك
                        الخاص ، ويمكنك إعادة إرسالة مرة أخرى من خلال الرابط التالى
                        <p><a href="#" style="font-weight: bold">إعادة إرسال رابط الخدمة</a></p>
                    </div>
                </div>
            </div>
        </div>










        <!-- ========================= Scripts Files ========================= -->
        <!-- jQuery -->
        <script src="{{url('/')}}/new_landing/js/jquery-3.2.1.min.js"></script>


        <!-- tether -->
        <script src="{{url('/')}}/new_landing/js/tether.min.js"></script>

        <!-- Bootstrap -->
        <script src="{{url('/')}}/new_landing/js/bootstrap.min.js"></script>

        <!-- owl Carousel -->
        <script src="{{url('/')}}/new_landing/js/owl.carousel.min.js"></script>

        <!-- custom js -->
        <script src="{{url('/')}}/new_landing/js/script.js"></script>



        <script>

                        function changeFormAction(value) {
                            if (value == "+962") {
                                $("#oreedo_kuwait").attr('action', "{{ url('/').'/subscribeZainJordon' }}");
                            } else if (value == "+965") {
                                $("#oreedo_kuwait").attr('action', "{{ url('/').'/subscribeOreedo' }}");
                            } else {
                                $("#oreedo_kuwait").attr('action', "{{ url('/').'/subscribeOreedo' }}");
                            }

                        }

                
        </script>


        @if(Session::has('error'))

        <script>
            $(".modal-title").html("خطأ");
            $(".modal-body ").html("{{  Session::get('error')   }} ");
            $("#myModal").modal("show");

        </script>

        @endif

    </body>

</html>

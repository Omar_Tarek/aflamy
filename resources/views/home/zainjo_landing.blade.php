<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-131069436-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag() {
                dataLayer.push(arguments);
            }
            gtag('js', new Date());

            gtag('config', 'UA-131069436-1');
        </script>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Aflamy Landing Page</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="{{{asset('/new_landing/css/bootstrap.min.css')}}}">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{{asset('')}}}/new_landing/css/font-awesome.min.css">

        <!-- Owl Carousel -->
        <link rel="stylesheet" href="{{{asset('')}}}/new_landing/css/owl.carousel.min.css">
        <link rel="stylesheet" href="{{asset('/new_landing/css/owl.theme.default.min.css')}}">

        <!-- custom css -->
        <link rel="stylesheet" href="{{asset('/new_landing/css/style.css')}}">

        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->


    </head>

    <body>

        <div class="main">
            <div class="logo">
                <img src="{{{asset('')}}}/new_landing/images/logo-png.png" alt="">
                <span class="white_color">Aflamy By iVAS-Video </span>
            </div>


            <!-- Slider for Images -->
            <div class="strip-layer">

                <img src="{{{asset('')}}}/new_landing/images/strip-2.png" alt="">

                <div class="aflam">

                    <div class="owl-carousel owl-theme">

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{{asset('')}}}/new_landing/images/new/1.jpg" alt="">
                            </a>
                        </div>

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{{asset('')}}}/new_landing/images/new/2.jpg" alt="">
                            </a>
                        </div>

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{{asset('')}}}/new_landing/images/new/3.jpg" alt="">
                            </a>
                        </div>



                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{{asset('')}}}/new_landing/images/new/5.jpg" alt="">
                            </a>
                        </div>

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{{asset('')}}}/new_landing/images/new/6.jpg" alt="">
                            </a>
                        </div>

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{{asset('')}}}/new_landing/images/new/7.jpg" alt="">
                            </a>
                        </div>

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{{asset('')}}}/new_landing/images/new/8.jpg" alt="">
                            </a>
                        </div>

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{{asset('')}}}/new_landing/images/new/9.jpg" alt="">
                            </a>
                        </div>

                        <div class="item">
                            <a href="" class="aflam-item--img">
                                <img src="{{{asset('')}}}/new_landing/images/new/10.jpg" alt="">
                            </a>
                        </div>

                    </div><!-- ./owl-carousel -->

                </div><!-- ./aflam -->

            </div>





            <div class="sub">
                {!! Form::open(array('url' => 'subscribeZainJoConfirm', 'method' => 'post','id'=>'zain_jordon')) !!}
                <label for=""  class="terms_color">يرجى ادخال رقم الهاتف للحصول على رمز تفعيل الاشتراك بخدمة أفلامي</label>
                <div>
                    {!! Form::number('number',$MSISDN,['class'=>'form-control num','required'=>'required','min'=>1,'id'=>'number']) !!}

                    <span>+962</span>
                </div>
                <input  style="font-size:15px !important;" class="subscribe" type="submit"  value="أحصل على رمز التفعيل">

                {!! Form::close() !!}

                <p class="terms_color">عندالاشتراك يحصل المشترك علي عدد كبير من الافلام </p>
                    <p class="terms_color">سعر الخدمة 100 فلس يوميا</p>
                       <p class="terms_color">لإلغاء الاشتراك يرجى إرسال unsub A1 إلى 97970</p>
            </div>


        </div>

        <!-- The Modal -->
        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title m-auto">مشترك بالفعل</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body text-center">
                        عزيزى المستخدم، أنت بالفعل مشترك فى الخدمة ويمكنك الاستمتاع بها من خلال الرابط المرسل سابقا الى هاتفك
                        الخاص ، ويمكنك إعادة إرسالة مرة أخرى من خلال الرابط التالى
                        <p><a href="#" style="font-weight: bold">إعادة إرسال رابط الخدمة</a></p>
                    </div>
                </div>
            </div>
        </div>










        <!-- ========================= Scripts Files ========================= -->
        <!-- jQuery -->
        <script src="{{{asset('')}}}/new_landing/js/jquery-3.2.1.min.js"></script>


        <!-- tether -->
        <script src="{{{asset('')}}}/new_landing/js/tether.min.js"></script>

        <!-- Bootstrap -->
        <script src="{{{asset('')}}}/new_landing/js/bootstrap.min.js"></script>

        <!-- owl Carousel -->
        <script src="{{{asset('')}}}/new_landing/js/owl.carousel.min.js"></script>

        <!-- custom js -->
        <script src="{{{asset('')}}}/new_landing/js/script.js"></script>





        @if(Session::has('error'))

        <script>
            $(".modal-title").html("خطأ");
            $(".modal-body ").html("{{  Session::get('error')   }} ");
            $("#myModal").modal("show");

        </script>

        @endif

    </body>

</html>

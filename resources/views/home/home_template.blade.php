<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <!--IE Compatibility Meta-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--Mobile Meta-->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Landing</title>


    <link rel="stylesheet" type="text/css"  href="{{url('home/font.css')}}" >

    @yield('main_css')

    @yield('ar_css')


    <!--[if lt IE 9]>
    <script src="{{url('home/js/html5shiv.min.js')}}"></script>
    <script src="{{url('home/js/respond.min.js')}}"></script>
    <![endif]-->
    <script type="text/javascript">
        function showOperatorForm(operator_name){

           if(operator_name == "oreedo"){
              // alert("oredo");
               $("#zain_kuwait").hide();
               $("#oreedo_kuwait").fadeIn(500);
               $("#orze").hide();
               $("#changeOP").hide();
           }else{
               // alert("zain");
               $("#oreedo_kuwait").hide();
               $("#zain_kuwait").fadeIn(500);
               $("#orze").hide();
               $("#changeOP").hide();
           }


        }

    </script>
</head>
<body>

@yield('content')




<!--=============================== End-Term================================-->
<script src="{{url('home/js/jquery-2.0.2.min.js')}}"  type="text/javascript"></script>
<script src="{{url('home/js/bootstrap.min.js')}}"  type="text/javascript"></script>
<script src="{{url('home/js/main.js')}}"  type="text/javascript"></script>
<script src="{{url('home/js/wow.min.js')}}"  type="text/javascript"></script>
<script src="{{url('home/js/unsub.js')}}"  type="text/javascript"></script>

      <script>
         wow = new WOW(
           {
             animateClass: 'animated',
             offset:       100,
             callback:     function(box) {
               console.log("WOW: animating <" + box.tagName.toLowerCase() + ">")
             }
           }
         );
         wow.init();



      </script>


<script>
   //  fullUrl = '{{ Request::fullUrl()  }}' ;
    fullUrl = '{{url('/')}}' ;

   currentUrl =  '{{ Request::url() }}'  ;

    </script>

@if(Session::has('message'))

    <script>
        $("#myModal").modal("show");
          $("#unsubZainJoResult").modal("show");
        if( currentUrl != fullUrl ){
            $('#myModal').on('hidden.bs.modal', function () {
                location.href = "{{url('/index?operator_id=1')}}"  ;
            })
        }

        $('.unsubModel').on('hidden.bs.modal', function () {
            location.href = "{{url('')}}"  ;
        })


        $('.myModalOreddoSub').on('hidden.bs.modal', function () {
            location.href = "{{url('')}}"  ;
        })
        
        
         $('#unsubZainJoResult').on('hidden.bs.modal', function () {
                location.href = "{{url('/zainjordan')}}"  ;
            })


    </script>

@endif


@if(Session::has('error'))
  {{--  <link rel="stylesheet" type="text/css" href="{{url('home/css/bootstrap-ar.css')}}">--}}
   <script>
        $("#myModal").modal("show");
        if( currentUrl != fullUrl ){
            $('#myModal').on('hidden.bs.modal', function () {
                location.href = "{{url('')}}"  ;
            })
        }


        $('.unsubModel').on('hidden.bs.modal', function () {
            location.href = "{{url('')}}"  ;
        })
   </script>

@endif





</body>
</html>





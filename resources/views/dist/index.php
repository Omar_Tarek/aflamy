<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta content="IE=edge" http-equiv="x-ua-compatible">
    <meta content="initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no" name="viewport">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta content="yes" name="apple-touch-fullscreen">
    <title>خدمة وفرلى - برعاية ايفاز</title>
    <base href="/">
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <link href="styles.9c1fdc43a0e19366d996.bundle.css" rel="stylesheet" />
</head>

<body>
    <app-root>
        <div class="loader-app">
            <h3>وفرلى</h3>
            <div class="ld ld-ring ld-spin-fast huge"></div>
        </div>
    </app-root>
    <script type="text/javascript" src="inline.453d20e048cb7a084099.bundle.js"></script>
    <script type="text/javascript" src="polyfills.3ae08eaf6129a55130cb.bundle.js"></script>
    <script type="text/javascript" src="scripts.bd1cb65ee364091658c0.bundle.js"></script>
    <script type="text/javascript" src="vendor.e18b5d334108fae7ca1e.bundle.js"></script>
    <script type="text/javascript" src="main.b6e819a60f476fe27e3e.bundle.js"></script>
</body>

</html>
@extends('template')
@section('page_title')
		Advertisments
@stop
@section('content')
    @include('errors')
    <div class="row">
        <div class="col-md-12">
            <div class="box box-blue">
                <div class="box-title">
                    @if($ad != null)
                        <h3><i class="fa fa-table"></i>Update Ads.</h3>
                    @else
                        <h3><i class="fa fa-table"></i>Add Ads.</h3>
                    @endif
                    <div class="box-tool">
                        <a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
                        <a data-action="close" href="#"><i class="fa fa-times"></i></a>
                    </div>
                </div>
                <div class="box-content">
                    @if($ad != null)
                        {!! Form::model($ad,['url'=>'advertisments/'.$ad->id,'class'=>'form-horizontal','files'=>'true','method'=>'PATCH']) !!}
                        @include('ads.form',['buttonAction'=>'Save','required'=>' (optional)'])
                    @else
                        {!! Form::open(['url'=>'advertisments','class'=>'form-horizontal','files'=>'true','method'=>'POST']) !!}
                        @include('ads.form',['buttonAction'=>'Add','required'=>' *'])
                    @endif
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

@stop

@section('script')

    <script>
        $('#ad').addClass('active');
        $('#ad-create').addClass('active');
    </script>
@stop
@extends('template')
@section('page_title')
		Advertisments
@stop
@section('content')
<div class="row">
		<div class="col-md-12">
			<div class="box box-black">
				<div class="box-title">
					<h3><i class="fa fa-table"></i>Advertisments</h3>
					<div class="box-tool">
						<a data-action="collapse" href="#"><i class="fa fa-chevron-up"></i></a>
						<a data-action="close" href="#"><i class="fa fa-times"></i></a>
					</div>
				</div>
				<div class="box-content">
                    <div class="btn-toolbar pull-right">
                        <div class="btn-group">
                            <a class="btn btn-circle show-tooltip" title="Add Ads." href="{{url('advertisments/create')}}" data-original-title="Add new record"><i class="fa fa-plus"></i></a>
							<a  id="delete_button" onclick="delete_selected('ads')" class="btn btn-circle btn-danger show-tooltip" title="@lang('messages.template.delete_many')" href="#"><i class="fa fa-trash-o"></i></a>
                        </div>
                    </div>
                    <br><br>
					<div class="table-responsive">
						<table id="example" class="table table-striped dt-responsive" cellspacing="0" width="100%">
							<thead>
							<tr>
                                <th style="width:18px"><input type="checkbox" onclick="select_all('ads',true)" ></th>
								<th>Link</th>
								<th>Image</th>
								<th>Operator</th>
                                <th>published date</th>
								<th class="visible-md visible-lg" style="width:130px">Action</th>
							</tr>
							</thead>
					 		<tbody> 
							@foreach($ads as $ad)
								<tr class="table-flag-blue">
								    <th><input type="checkbox" name="selected_rows[]" class="select-all-karim" value="{{$ad->id}}" onclick="collect_selected(this)"></th>
									<td><input type="text" value="{{$ad->link}}" /></td>
									<td><img src="{{url($ad->image)}}" width="100" height="100" class="img-circle" /></td>
									<td>{{$ad->operator->name}}</td>
                                    <td>{{date('d-m-Y',strtotime($ad->published_date))}}</td> 
									<td class="visible-md visible-lg">
										<div class="btn-group">
											<a class="btn btn-sm show-tooltip" title="Edit" href="{{url('advertisments/'.$ad->id.'/edit')}}" data-original-title="Edit"><i class="fa fa-edit"></i></a>
											<a class="btn btn-sm btn-danger show-tooltip" title="Delete" onclick="return confirm('Are you sure you want to delete this ?');" href="{{url('advertisments/'.$ad->id.'/delete')}}" data-original-title="Delete"><i class="fa fa-trash-o"></i></a>
										</div>
									</td>
								</tr>
							@endforeach
							</tbody> 
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop



@section('script')

    <script>
        $('#ad').addClass('active');
        $('#ad-index').addClass('active');
    </script>
@stop
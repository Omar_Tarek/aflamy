

<div class="form-group">
    {!! Form::label('link','Ad Link *',['class'=>'col-sm-3 col-lg-2 control-label']) !!}
    <div class="col-sm-9 col-lg-10 controls">
        {!! Form::text('link',null,['class'=>'form-control input-lg','required'=>'required','placeholder'=>'Ad Link']) !!}
    </div>
</div>




<div class="form-group">
    <label class="col-sm-3 col-lg-2 control-label">Select Operator</label>
    <div class="col-sm-9 col-md-10 controls">
        <select class="form-control chosen-rtl" name="operator_id" required>
            @foreach ($operators as $operator)
                <option value="{{ $operator->id }}" @if ($ad && $ad->operator_id == $operator->id) selected @endif>{{ $operator->name }}</option>
            @endforeach
        </select>
        <br/>
    </div>
</div>
           
<div class="form-group">
    {!! Form::label('image','Ad Image'.$required,['class'=>'col-sm-3 col-lg-2 control-label']) !!}
    <div class="col-sm-9 col-lg-10 controls">
        <div class="fileupload fileupload-new" data-provides="fileupload">
            <div class="fileupload-new img-thumbnail" style="width: 200px; height: 150px;">
                @if($ad == null)
                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+file" alt="" />
                @else
                    @if(file_exists($ad->image))
                        <img src="{{url($ad->image)}}" alt="" />
                    @else
                        <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+file" alt="" />
                    @endif
                @endif

            </div>
            <div class="fileupload-preview fileupload-exists img-thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
            <div>
                                    <span class="btn btn-default btn-file"><span class="fileupload-new">Upload</span>
                                        <span class="fileupload-exists">Change</span>
                                        @if($ad == null)
                                            {!! Form::file('image',["accept"=>"image/*",'required'=>'required']) !!}
                                        @else
                                            {!! Form::file('image') !!}
                                        @endif
                                        </span>
                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
            </div>
            <span class='label label-important'>NOTE!</span>
            <span>Only extension supported jpg, png, and jpeg</span>
        </div>
    </div>

</div>


<div class="form-group">
    <label class="col-sm-3 col-lg-2 control-label">Show that Ad. at date *</label>
    <div class="col-sm-5 col-lg-3 controls">
        <div class="input-group date date-picker" data-date="12-02-2012" data-date-format="YYYY-MM-DD" data-date-viewmode="years">
            <span class="input-group-addon"><i class="fa fa-calendar"></i></span> 
            @if($ad)
            {!! Form::text('published_date',date('d-m-Y',strtotime($ad->published_date)),['class'=>'form-control date-picker','required' => 'required', 'size'=>'16']) !!}
            @else 
            {!! Form::text('published_date',\Carbon\Carbon::now()->format('d-m-Y'),['class'=>'form-control date-picker','required' => 'required', 'size'=>'16']) !!}                
            @endif
        </div>
    </div>
</div>

  

<div class="form-group">
    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
        {!! Form::submit($buttonAction,['class'=>'btn btn-primary']) !!}
    </div>
</div>

 
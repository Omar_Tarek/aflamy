/*global $, console ,swiper, fancybox*/
$(document).ready(function () {
    'use strict';

    // trigger owl carousel 
    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 1000,
        autoplayHoverPause: true,
        nav: true,
        dots: false,
        navText: ["<i class='fa fa-angle-left fa-lg'>", "<i class='fa fa-angle-right fa-lg'>"],
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })

    // 3shaan lma tdooos 3lla submit btn ... mi3ml4 action 8eer 2noo yfta7 el model 
    /*   $('form').submit(function(e){
     e.preventDefault();
     });*/

    // foucs on input tag
    if ($(window).width() < 575) {

        $(window).click(function (e) {
            $('.strip-layer').show();
            if ($(e.target).hasClass('num')) {
                $('.strip-layer').hide();
            }

        });
    }


    // for select code of zain or oradoo
    var e = document.getElementById("state");
    $(e).change(function () {        
         $('.span-code').html(this.value);
    });

    

});

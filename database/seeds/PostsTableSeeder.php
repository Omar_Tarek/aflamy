<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('posts')->delete();
        
        \DB::table('posts')->insert(array (
            0 => 
            array (
                'id' => 10,
                'operator_id' => 1,
                'product_id' => 2,
                'show_date' => '2018-05-08',
                'active' => 1,
                'created_at' => '2018-05-08 11:43:09',
                'updated_at' => '2018-05-08 11:43:09',
                'featured' => 0,
            ),
            1 => 
            array (
                'id' => 11,
                'operator_id' => 1,
                'product_id' => 3,
                'show_date' => '2018-05-11',
                'active' => 1,
                'created_at' => '2018-05-08 11:43:31',
                'updated_at' => '2018-05-08 11:43:31',
                'featured' => 0,
            ),
            2 => 
            array (
                'id' => 12,
                'operator_id' => 1,
                'product_id' => 4,
                'show_date' => '2018-05-11',
                'active' => 1,
                'created_at' => '2018-05-08 11:43:43',
                'updated_at' => '2018-05-08 11:43:43',
                'featured' => 1,
            ),
        ));
        
        
    }
}

<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_name' => 'أكشن',
                'created_at' => '2017-10-23 12:30:27',
                'updated_at' => '2017-10-23 13:19:29',
            ),
            1 => 
            array (
                'id' => 2,
                'category_name' => 'كوميدي',
                'created_at' => '2017-10-23 12:30:33',
                'updated_at' => '2017-10-23 13:19:33',
            ),
            2 => 
            array (
                'id' => 3,
                'category_name' => 'رعب',
                'created_at' => '2017-10-23 12:30:37',
                'updated_at' => '2017-10-23 13:19:37',
            ),
            3 => 
            array (
                'id' => 4,
                'category_name' => 'مسرحيات',
                'created_at' => '2017-10-23 12:30:45',
                'updated_at' => '2017-10-23 12:30:45',
            ),
            4 => 
            array (
                'id' => 5,
                'category_name' => 'فيديو كليب',
                'created_at' => '2017-10-23 12:30:50',
                'updated_at' => '2017-10-23 12:30:50',
            ),
        ));
        
        
    }
}

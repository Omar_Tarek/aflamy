<?php

use Illuminate\Database\Seeder;

class NotifyTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('notify')->delete();
        
        \DB::table('notify')->insert(array (
            0 => 
            array (
                'id' => 2,
                'complete_url' => 'http://aflamyweb.com/redirect?action=3&mnc=102&msisdn=96566782477&opsid=2&status=RS',
                'action' => '3',
                'msisdn' => '96566782477',
                'status' => 'RS',
                'created_at' => '2018-05-10 14:13:46',
                'updated_at' => '2018-05-10 14:13:46',
            ),
        ));
        
        
    }
}

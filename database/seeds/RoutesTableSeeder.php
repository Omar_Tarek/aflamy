<?php

use Illuminate\Database\Seeder;

class RoutesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('routes')->delete();
        
        \DB::table('routes')->insert(array (
            0 => 
            array (
                'id' => 1,
                'method' => 'get',
                'route' => 'roles',
                'controller_name' => 'RoleController',
                'created_at' => '2018-02-18 07:42:09',
                'updated_at' => '2018-02-18 07:42:09',
                'function_name' => 'index',
            ),
            1 => 
            array (
                'id' => 2,
                'method' => 'get',
                'route' => 'roles/new',
                'controller_name' => 'RoleController',
                'created_at' => '2018-02-18 07:42:10',
                'updated_at' => '2018-02-18 07:42:10',
                'function_name' => 'create',
            ),
            2 => 
            array (
                'id' => 3,
                'method' => 'post',
                'route' => 'roles',
                'controller_name' => 'RoleController',
                'created_at' => '2018-02-18 07:42:10',
                'updated_at' => '2018-02-18 07:42:10',
                'function_name' => 'store',
            ),
            3 => 
            array (
                'id' => 4,
                'method' => 'get',
                'route' => 'roles/{id}/edit',
                'controller_name' => 'RoleController',
                'created_at' => '2018-02-18 07:42:10',
                'updated_at' => '2018-02-18 07:42:10',
                'function_name' => 'edit',
            ),
            4 => 
            array (
                'id' => 5,
                'method' => 'post',
                'route' => 'roles/{id}/update',
                'controller_name' => 'RoleController',
                'created_at' => '2018-02-18 07:42:10',
                'updated_at' => '2018-02-18 07:42:10',
                'function_name' => 'update',
            ),
            5 => 
            array (
                'id' => 6,
                'method' => 'get',
                'route' => 'roles/{id}/delete',
                'controller_name' => 'RoleController',
                'created_at' => '2018-02-18 07:42:10',
                'updated_at' => '2018-02-18 07:42:10',
                'function_name' => 'destroy',
            ),
            6 => 
            array (
                'id' => 7,
                'method' => 'get',
                'route' => 'roles/{id}/view_access',
                'controller_name' => 'RoleController',
                'created_at' => '2018-02-18 07:42:10',
                'updated_at' => '2018-02-18 07:42:10',
                'function_name' => 'view_access',
            ),
            7 => 
            array (
                'id' => 8,
                'method' => 'get',
                'route' => 'users',
                'controller_name' => 'UserController',
                'created_at' => '2018-02-18 07:44:09',
                'updated_at' => '2018-02-18 07:44:09',
                'function_name' => 'index',
            ),
            8 => 
            array (
                'id' => 9,
                'method' => 'get',
                'route' => 'users/new',
                'controller_name' => 'UserController',
                'created_at' => '2018-02-18 07:44:16',
                'updated_at' => '2018-02-18 07:44:16',
                'function_name' => 'create',
            ),
            9 => 
            array (
                'id' => 10,
                'method' => 'post',
                'route' => 'users',
                'controller_name' => 'UserController',
                'created_at' => '2018-02-18 07:44:16',
                'updated_at' => '2018-02-18 07:44:16',
                'function_name' => 'store',
            ),
            10 => 
            array (
                'id' => 11,
                'method' => 'get',
                'route' => 'users/{id}/edit',
                'controller_name' => 'UserController',
                'created_at' => '2018-02-18 07:44:16',
                'updated_at' => '2018-02-18 07:44:16',
                'function_name' => 'edit',
            ),
            11 => 
            array (
                'id' => 12,
                'method' => 'post',
                'route' => 'users/{id}/update',
                'controller_name' => 'UserController',
                'created_at' => '2018-02-18 07:44:16',
                'updated_at' => '2018-02-18 07:44:16',
                'function_name' => 'update',
            ),
            12 => 
            array (
                'id' => 13,
                'method' => 'get',
                'route' => 'users/{id}/delete',
                'controller_name' => 'UserController',
                'created_at' => '2018-02-18 07:44:16',
                'updated_at' => '2018-02-18 07:44:16',
                'function_name' => 'destroy',
            ),
            13 => 
            array (
                'id' => 14,
                'method' => 'get',
                'route' => 'user_profile',
                'controller_name' => 'UserController',
                'created_at' => '2018-02-18 07:44:16',
                'updated_at' => '2018-02-18 07:44:16',
                'function_name' => 'profile',
            ),
            14 => 
            array (
                'id' => 15,
                'method' => 'post',
                'route' => 'user_profile/updatepassword',
                'controller_name' => 'UserController',
                'created_at' => '2018-02-18 07:44:16',
                'updated_at' => '2018-02-18 07:44:16',
                'function_name' => 'UpdatePassword',
            ),
            15 => 
            array (
                'id' => 16,
                'method' => 'post',
                'route' => 'user_profile/updateprofilepic',
                'controller_name' => 'UserController',
                'created_at' => '2018-02-18 07:44:16',
                'updated_at' => '2018-02-18 07:44:16',
                'function_name' => 'UpdateProfilePicture',
            ),
            16 => 
            array (
                'id' => 17,
                'method' => 'post',
                'route' => 'user_profile/updateuserdata',
                'controller_name' => 'UserController',
                'created_at' => '2018-02-18 07:44:16',
                'updated_at' => '2018-02-18 07:44:16',
                'function_name' => 'UpdateNameAndEmail',
            ),
            17 => 
            array (
                'id' => 18,
                'method' => 'get',
                'route' => 'setting',
                'controller_name' => 'SettingController',
                'created_at' => '2018-02-18 07:45:44',
                'updated_at' => '2018-02-18 07:45:44',
                'function_name' => 'index',
            ),
            18 => 
            array (
                'id' => 19,
                'method' => 'get',
                'route' => 'setting/new',
                'controller_name' => 'SettingController',
                'created_at' => '2018-02-18 07:45:44',
                'updated_at' => '2018-02-18 07:45:44',
                'function_name' => 'create',
            ),
            19 => 
            array (
                'id' => 20,
                'method' => 'post',
                'route' => 'setting',
                'controller_name' => 'SettingController',
                'created_at' => '2018-02-18 07:45:44',
                'updated_at' => '2018-02-18 07:45:44',
                'function_name' => 'store',
            ),
            20 => 
            array (
                'id' => 21,
                'method' => 'get',
                'route' => 'setting/{id}/edit',
                'controller_name' => 'SettingController',
                'created_at' => '2018-02-18 07:45:44',
                'updated_at' => '2018-02-18 07:45:44',
                'function_name' => 'edit',
            ),
            21 => 
            array (
                'id' => 22,
                'method' => 'post',
                'route' => 'setting/{id}/update',
                'controller_name' => 'SettingController',
                'created_at' => '2018-02-18 07:45:44',
                'updated_at' => '2018-02-18 07:45:44',
                'function_name' => 'update',
            ),
            22 => 
            array (
                'id' => 23,
                'method' => 'get',
                'route' => 'setting/{id}/delete',
                'controller_name' => 'SettingController',
                'created_at' => '2018-02-18 07:45:44',
                'updated_at' => '2018-02-18 07:45:44',
                'function_name' => 'destroy',
            ),
            23 => 
            array (
                'id' => 24,
                'method' => 'get',
                'route' => 'advertisments',
                'controller_name' => 'AdsController',
                'created_at' => '2018-02-18 07:47:09',
                'updated_at' => '2018-02-18 07:47:09',
                'function_name' => 'index',
            ),
            24 => 
            array (
                'id' => 25,
                'method' => 'get',
                'route' => 'advertisments/create',
                'controller_name' => 'AdsController',
                'created_at' => '2018-02-18 07:47:09',
                'updated_at' => '2018-02-18 07:47:09',
                'function_name' => 'create',
            ),
            25 => 
            array (
                'id' => 26,
                'method' => 'post',
                'route' => 'advertisments',
                'controller_name' => 'AdsController',
                'created_at' => '2018-02-18 07:47:09',
                'updated_at' => '2018-02-18 07:47:09',
                'function_name' => 'store',
            ),
            26 => 
            array (
                'id' => 27,
                'method' => 'get',
                'route' => 'advertisments/{id}/edit',
                'controller_name' => 'AdsController',
                'created_at' => '2018-02-18 07:47:09',
                'updated_at' => '2018-02-18 07:47:09',
                'function_name' => 'edit',
            ),
            27 => 
            array (
                'id' => 28,
                'method' => 'patch',
                'route' => 'advertisments/{id}',
                'controller_name' => 'AdsController',
                'created_at' => '2018-02-18 07:47:09',
                'updated_at' => '2018-02-18 07:47:09',
                'function_name' => 'update',
            ),
            28 => 
            array (
                'id' => 29,
                'method' => 'get',
                'route' => 'advertisments/{id}/delete',
                'controller_name' => 'AdsController',
                'created_at' => '2018-02-18 07:47:09',
                'updated_at' => '2018-02-18 07:47:09',
                'function_name' => 'destroy',
            ),
            29 => 
            array (
                'id' => 30,
                'method' => 'get',
                'route' => 'operators',
                'controller_name' => 'OperatorsController',
                'created_at' => '2018-02-18 07:49:22',
                'updated_at' => '2018-02-18 07:49:22',
                'function_name' => 'index',
            ),
            30 => 
            array (
                'id' => 31,
                'method' => 'get',
                'route' => 'operators/create',
                'controller_name' => 'OperatorsController',
                'created_at' => '2018-02-18 07:49:22',
                'updated_at' => '2018-02-18 07:49:22',
                'function_name' => 'create',
            ),
            31 => 
            array (
                'id' => 32,
                'method' => 'post',
                'route' => 'operators',
                'controller_name' => 'OperatorsController',
                'created_at' => '2018-02-18 07:49:22',
                'updated_at' => '2018-02-18 07:49:22',
                'function_name' => 'store',
            ),
            32 => 
            array (
                'id' => 33,
                'method' => 'get',
                'route' => 'operators/{id}/edit',
                'controller_name' => 'OperatorsController',
                'created_at' => '2018-02-18 07:49:22',
                'updated_at' => '2018-02-18 07:49:22',
                'function_name' => 'edit',
            ),
            33 => 
            array (
                'id' => 34,
                'method' => 'patch',
                'route' => 'operators/{id}',
                'controller_name' => 'OperatorsController',
                'created_at' => '2018-02-18 07:49:22',
                'updated_at' => '2018-02-18 07:49:22',
                'function_name' => 'update',
            ),
            34 => 
            array (
                'id' => 35,
                'method' => 'delete',
                'route' => 'operators/{id}/delete',
                'controller_name' => 'OperatorsController',
                'created_at' => '2018-02-18 07:49:22',
                'updated_at' => '2018-02-18 07:49:22',
                'function_name' => 'destroy',
            ),
            35 => 
            array (
                'id' => 36,
                'method' => 'get',
                'route' => 'categories',
                'controller_name' => 'CategoriesController',
                'created_at' => '2018-02-18 07:50:33',
                'updated_at' => '2018-02-18 07:50:33',
                'function_name' => 'index',
            ),
            36 => 
            array (
                'id' => 37,
                'method' => 'get',
                'route' => 'categories/create',
                'controller_name' => 'CategoriesController',
                'created_at' => '2018-02-18 07:50:33',
                'updated_at' => '2018-02-18 07:50:33',
                'function_name' => 'create',
            ),
            37 => 
            array (
                'id' => 38,
                'method' => 'post',
                'route' => 'categories',
                'controller_name' => 'CategoriesController',
                'created_at' => '2018-02-18 07:50:33',
                'updated_at' => '2018-02-18 07:50:33',
                'function_name' => 'store',
            ),
            38 => 
            array (
                'id' => 39,
                'method' => 'get',
                'route' => 'categories/{id}/edit',
                'controller_name' => 'CategoriesController',
                'created_at' => '2018-02-18 07:50:33',
                'updated_at' => '2018-02-18 07:50:33',
                'function_name' => 'edit',
            ),
            39 => 
            array (
                'id' => 40,
                'method' => 'patch',
                'route' => 'categories/{id}',
                'controller_name' => 'CategoriesController',
                'created_at' => '2018-02-18 07:50:33',
                'updated_at' => '2018-02-18 07:50:33',
                'function_name' => 'update',
            ),
            40 => 
            array (
                'id' => 41,
                'method' => 'delete',
                'route' => 'categories/{id}',
                'controller_name' => 'CategoriesController',
                'created_at' => '2018-02-18 07:50:34',
                'updated_at' => '2018-02-18 07:58:08',
                'function_name' => 'destroy',
            ),
            41 => 
            array (
                'id' => 42,
                'method' => 'get',
                'route' => 'brands',
                'controller_name' => 'BrandsController',
                'created_at' => '2018-02-18 07:51:55',
                'updated_at' => '2018-02-18 07:51:55',
                'function_name' => 'index',
            ),
            42 => 
            array (
                'id' => 43,
                'method' => 'get',
                'route' => 'brands/create',
                'controller_name' => 'BrandsController',
                'created_at' => '2018-02-18 07:51:55',
                'updated_at' => '2018-02-18 07:51:55',
                'function_name' => 'create',
            ),
            43 => 
            array (
                'id' => 44,
                'method' => 'post',
                'route' => 'brands',
                'controller_name' => 'BrandsController',
                'created_at' => '2018-02-18 07:51:55',
                'updated_at' => '2018-02-18 07:51:55',
                'function_name' => 'store',
            ),
            44 => 
            array (
                'id' => 45,
                'method' => 'get',
                'route' => 'brands/{id}/edit',
                'controller_name' => 'BrandsController',
                'created_at' => '2018-02-18 07:51:55',
                'updated_at' => '2018-02-18 07:51:55',
                'function_name' => 'edit',
            ),
            45 => 
            array (
                'id' => 46,
                'method' => 'patch',
                'route' => 'brands/{id}',
                'controller_name' => 'BrandsController',
                'created_at' => '2018-02-18 07:51:55',
                'updated_at' => '2018-02-18 07:51:55',
                'function_name' => 'update',
            ),
            46 => 
            array (
                'id' => 47,
                'method' => 'delete',
                'route' => 'brands/{id}',
                'controller_name' => 'BrandsController',
                'created_at' => '2018-02-18 07:51:55',
                'updated_at' => '2018-02-18 07:58:19',
                'function_name' => 'destroy',
            ),
            47 => 
            array (
                'id' => 48,
                'method' => 'get',
                'route' => 'posts',
                'controller_name' => 'PostsController',
                'created_at' => '2018-02-18 07:53:59',
                'updated_at' => '2018-02-18 07:53:59',
                'function_name' => 'index',
            ),
            48 => 
            array (
                'id' => 49,
                'method' => 'get',
                'route' => 'posts/create',
                'controller_name' => 'PostsController',
                'created_at' => '2018-02-18 07:53:59',
                'updated_at' => '2018-02-18 07:53:59',
                'function_name' => 'create',
            ),
            49 => 
            array (
                'id' => 50,
                'method' => 'post',
                'route' => 'posts',
                'controller_name' => 'PostsController',
                'created_at' => '2018-02-18 07:53:59',
                'updated_at' => '2018-02-18 07:53:59',
                'function_name' => 'store',
            ),
            50 => 
            array (
                'id' => 51,
                'method' => 'get',
                'route' => 'posts/{id}/edit',
                'controller_name' => 'PostsController',
                'created_at' => '2018-02-18 07:53:59',
                'updated_at' => '2018-02-18 07:53:59',
                'function_name' => 'edit',
            ),
            51 => 
            array (
                'id' => 52,
                'method' => 'patch',
                'route' => 'posts/{id}',
                'controller_name' => 'PostsController',
                'created_at' => '2018-02-18 07:53:59',
                'updated_at' => '2018-02-18 07:53:59',
                'function_name' => 'update',
            ),
            52 => 
            array (
                'id' => 53,
                'method' => 'delete',
                'route' => 'posts/{id}',
                'controller_name' => 'PostsController',
                'created_at' => '2018-02-18 07:53:59',
                'updated_at' => '2018-02-18 07:58:00',
                'function_name' => 'destroy',
            ),
            53 => 
            array (
                'id' => 54,
                'method' => 'get',
                'route' => 'products',
                'controller_name' => 'ProductsController',
                'created_at' => '2018-02-18 07:57:47',
                'updated_at' => '2018-02-18 07:57:47',
                'function_name' => 'index',
            ),
            54 => 
            array (
                'id' => 55,
                'method' => 'get',
                'route' => 'products/create',
                'controller_name' => 'ProductsController',
                'created_at' => '2018-02-18 07:57:47',
                'updated_at' => '2018-02-18 07:57:47',
                'function_name' => 'create',
            ),
            55 => 
            array (
                'id' => 56,
                'method' => 'post',
                'route' => 'products',
                'controller_name' => 'ProductsController',
                'created_at' => '2018-02-18 07:57:47',
                'updated_at' => '2018-02-18 07:57:47',
                'function_name' => 'store',
            ),
            56 => 
            array (
                'id' => 57,
                'method' => 'get',
                'route' => 'products/{id}/edit',
                'controller_name' => 'ProductsController',
                'created_at' => '2018-02-18 07:57:47',
                'updated_at' => '2018-02-18 07:57:47',
                'function_name' => 'edit',
            ),
            57 => 
            array (
                'id' => 58,
                'method' => 'patch',
                'route' => 'products/{id}',
                'controller_name' => 'ProductsController',
                'created_at' => '2018-02-18 07:57:47',
                'updated_at' => '2018-02-18 07:57:47',
                'function_name' => 'update',
            ),
            58 => 
            array (
                'id' => 59,
                'method' => 'delete',
                'route' => 'products/{id}',
                'controller_name' => 'ProductsController',
                'created_at' => '2018-02-18 07:57:47',
                'updated_at' => '2018-02-18 07:57:47',
                'function_name' => 'destroy',
            ),
            59 => 
            array (
                'id' => 60,
                'method' => 'post',
                'route' => 'product/restore/{id}',
                'controller_name' => 'ProductsController',
                'created_at' => '2018-02-18 07:57:47',
                'updated_at' => '2018-02-18 07:57:47',
                'function_name' => 'restore',
            ),
            60 => 
            array (
                'id' => 61,
                'method' => 'post',
                'route' => 'product/delete-forever/{id}',
                'controller_name' => 'ProductsController',
                'created_at' => '2018-02-18 07:57:47',
                'updated_at' => '2018-02-18 07:57:47',
                'function_name' => 'deleteForever',
            ),
            61 => 
            array (
                'id' => 62,
                'method' => 'get',
                'route' => 'products/{id}/posts',
                'controller_name' => 'ProductsController',
                'created_at' => '2018-02-18 07:57:47',
                'updated_at' => '2018-02-18 07:57:47',
                'function_name' => 'get_posts',
            ),
            62 => 
            array (
                'id' => 63,
                'method' => 'get',
                'route' => 'dashboard',
                'controller_name' => 'DashboardController',
                'created_at' => '2018-02-18 07:59:55',
                'updated_at' => '2018-02-18 07:59:55',
                'function_name' => 'index',
            ),
            63 => 
            array (
                'id' => 64,
                'method' => 'get',
                'route' => 'dashboard/create',
                'controller_name' => 'DashboardController',
                'created_at' => '2018-02-18 07:59:55',
                'updated_at' => '2018-02-18 07:59:55',
                'function_name' => 'create',
            ),
            64 => 
            array (
                'id' => 65,
                'method' => 'get',
                'route' => 'dashboard',
                'controller_name' => 'DashboardController',
                'created_at' => '2018-02-18 07:59:55',
                'updated_at' => '2018-02-18 07:59:55',
                'function_name' => 'store',
            ),
            65 => 
            array (
                'id' => 66,
                'method' => 'get',
                'route' => 'dashboard/{id}/edit',
                'controller_name' => 'DashboardController',
                'created_at' => '2018-02-18 07:59:55',
                'updated_at' => '2018-02-18 07:59:55',
                'function_name' => 'edit',
            ),
            66 => 
            array (
                'id' => 67,
                'method' => 'patch',
                'route' => 'dashboard/{id}',
                'controller_name' => 'DashboardController',
                'created_at' => '2018-02-18 07:59:55',
                'updated_at' => '2018-02-18 07:59:55',
                'function_name' => 'update',
            ),
            67 => 
            array (
                'id' => 68,
                'method' => 'delete',
                'route' => 'dashboard/{id}',
                'controller_name' => 'DashboardController',
                'created_at' => '2018-02-18 07:59:55',
                'updated_at' => '2018-02-18 07:59:55',
                'function_name' => 'destroy',
            ),
            68 => 
            array (
                'id' => 69,
                'method' => 'get',
                'route' => 'index',
                'controller_name' => 'FrontEndController',
                'created_at' => '2018-02-18 08:03:15',
                'updated_at' => '2018-02-18 08:03:15',
                'function_name' => 'index',
            ),
            69 => 
            array (
                'id' => 70,
                'method' => 'get',
                'route' => 'get_brand',
                'controller_name' => 'FrontEndController',
                'created_at' => '2018-02-18 08:03:16',
                'updated_at' => '2018-02-18 08:03:16',
                'function_name' => 'get_brand_products',
            ),
            70 => 
            array (
                'id' => 71,
                'method' => 'get',
                'route' => 'get_product',
                'controller_name' => 'FrontEndController',
                'created_at' => '2018-02-18 08:03:16',
                'updated_at' => '2018-02-18 08:03:16',
                'function_name' => 'get_product',
            ),
            71 => 
            array (
                'id' => 72,
                'method' => 'get',
                'route' => 'get_category',
                'controller_name' => 'FrontEndController',
                'created_at' => '2018-02-18 08:03:16',
                'updated_at' => '2018-02-18 08:03:16',
                'function_name' => 'products_by_category',
            ),
            72 => 
            array (
                'id' => 73,
                'method' => 'get',
                'route' => 'search',
                'controller_name' => 'FrontEndController',
                'created_at' => '2018-02-18 08:03:16',
                'updated_at' => '2018-02-18 08:03:16',
                'function_name' => 'search_view',
            ),
            73 => 
            array (
                'id' => 74,
                'method' => 'get',
                'route' => 'search_result',
                'controller_name' => 'FrontEndController',
                'created_at' => '2018-02-18 08:03:16',
                'updated_at' => '2018-02-18 08:03:16',
                'function_name' => 'search',
            ),
            74 => 
            array (
                'id' => 75,
                'method' => 'get',
                'route' => 'terms',
                'controller_name' => 'FrontEndController',
                'created_at' => '2018-02-18 08:03:16',
                'updated_at' => '2018-02-18 08:03:16',
                'function_name' => 'terms',
            ),
            75 => 
            array (
                'id' => 76,
                'method' => 'get',
                'route' => 'login',
                'controller_name' => 'FrontEndController',
                'created_at' => '2018-02-18 08:03:16',
                'updated_at' => '2018-02-18 08:03:16',
                'function_name' => 'login',
            ),
            76 => 
            array (
                'id' => 77,
                'method' => 'get',
                'route' => 'logout',
                'controller_name' => 'FrontEndController',
                'created_at' => '2018-02-18 08:03:16',
                'updated_at' => '2018-02-18 08:03:16',
                'function_name' => 'logout',
            ),
            77 => 
            array (
                'id' => 78,
                'method' => 'get',
                'route' => 'register',
                'controller_name' => 'FrontEndController',
                'created_at' => '2018-02-18 08:03:16',
                'updated_at' => '2018-02-18 08:03:16',
                'function_name' => 'register',
            ),
            78 => 
            array (
                'id' => 79,
                'method' => 'get',
                'route' => 'buildroutes',
                'controller_name' => 'RouteController',
                'created_at' => '2018-02-18 08:09:12',
                'updated_at' => '2018-02-18 08:09:12',
                'function_name' => 'buildroutes',
            ),
            79 => 
            array (
                'id' => 80,
                'method' => 'get',
                'route' => 'posts/{id}',
                'controller_name' => 'PostsController',
                'created_at' => '2018-02-18 08:14:26',
                'updated_at' => '2018-02-18 08:14:26',
                'function_name' => 'show',
            ),
            80 => 
            array (
                'id' => 81,
                'method' => 'get',
                'route' => 'posts/allData',
                'controller_name' => 'PostsController',
                'created_at' => '2018-09-19 09:05:34',
                'updated_at' => '2018-09-19 09:05:34',
                'function_name' => 'allData',
            ),
            81 => 
            array (
                'id' => 82,
                'method' => 'get',
                'route' => 'products/allData',
                'controller_name' => 'ProductsController',
                'created_at' => '2018-09-19 12:52:13',
                'updated_at' => '2018-09-19 12:52:13',
                'function_name' => 'allData',
            ),
        ));
        
        
    }
}

<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('settings')->delete();
        
        \DB::table('settings')->insert(array (
            0 => 
            array (
                'id' => 3,
                'key' => 'facebook',
                'value' => 'facebook.com',
                'created_at' => '2017-10-23 13:02:29',
                'updated_at' => '2017-10-23 13:02:29',
            ),
            1 => 
            array (
                'id' => 4,
                'key' => 'twitter',
                'value' => 'twitter.com',
                'created_at' => '2017-10-23 13:03:04',
                'updated_at' => '2017-10-23 13:03:04',
            ),
            2 => 
            array (
                'id' => 7,
                'key' => 'logo',
                'value' => 'settings_images/59edf4aecee5b.png',
                'created_at' => '2017-10-23 13:06:57',
                'updated_at' => '2017-10-23 13:54:54',
            ),
            3 => 
            array (
                'id' => 8,
                'key' => 'Website title',
                'value' => 'Aflamy
',
                'created_at' => '2017-10-23 13:43:46',
                'updated_at' => '2017-10-23 14:22:06',
            ),
            4 => 
            array (
                'id' => 9,
                'key' => 'pageLength',
                'value' => '2',
                'created_at' => '2018-09-19 10:34:36',
                'updated_at' => '2018-09-19 10:34:36',
            ),
        ));
        
        
    }
}

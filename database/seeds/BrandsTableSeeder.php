<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('brands')->delete();
        
        \DB::table('brands')->insert(array (
            0 => 
            array (
                'id' => 1,
                'brand_name' => 'العدل جروب',
                'created_at' => '2017-10-23 12:32:11',
                'updated_at' => '2017-10-23 12:32:11',
            ),
            1 => 
            array (
                'id' => 2,
                'brand_name' => 'السبكي',
                'created_at' => '2017-10-23 12:32:15',
                'updated_at' => '2017-10-23 12:32:15',
            ),
            2 => 
            array (
                'id' => 3,
                'brand_name' => 'ماستر للإنتاج الفني',
                'created_at' => '2017-10-23 12:32:29',
                'updated_at' => '2017-10-23 12:32:29',
            ),
            3 => 
            array (
                'id' => 4,
                'brand_name' => 'العربيه للانتاج والتوزيع',
                'created_at' => '2017-10-23 12:33:06',
                'updated_at' => '2017-10-23 12:33:06',
            ),
        ));
        
        
    }
}

<?php

use Illuminate\Database\Seeder;

class MigrationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('migrations')->delete();
        
        \DB::table('migrations')->insert(array (
            0 => 
            array (
                'migration' => '2018_05_10_123022_create_ads_table',
                'batch' => 0,
            ),
            1 => 
            array (
                'migration' => '2018_05_10_123022_create_advertising_urls_table',
                'batch' => 0,
            ),
            2 => 
            array (
                'migration' => '2018_05_10_123022_create_brands_table',
                'batch' => 0,
            ),
            3 => 
            array (
                'migration' => '2018_05_10_123022_create_categories_table',
                'batch' => 0,
            ),
            4 => 
            array (
                'migration' => '2018_05_10_123022_create_msisdns_table',
                'batch' => 0,
            ),
            5 => 
            array (
                'migration' => '2018_05_10_123022_create_notify_table',
                'batch' => 0,
            ),
            6 => 
            array (
                'migration' => '2018_05_10_123022_create_operators_table',
                'batch' => 0,
            ),
            7 => 
            array (
                'migration' => '2018_05_10_123022_create_password_resets_table',
                'batch' => 0,
            ),
            8 => 
            array (
                'migration' => '2018_05_10_123022_create_permissions_table',
                'batch' => 0,
            ),
            9 => 
            array (
                'migration' => '2018_05_10_123022_create_posts_table',
                'batch' => 0,
            ),
            10 => 
            array (
                'migration' => '2018_05_10_123022_create_products_table',
                'batch' => 0,
            ),
            11 => 
            array (
                'migration' => '2018_05_10_123022_create_role_has_permissions_table',
                'batch' => 0,
            ),
            12 => 
            array (
                'migration' => '2018_05_10_123022_create_role_route_table',
                'batch' => 0,
            ),
            13 => 
            array (
                'migration' => '2018_05_10_123022_create_roles_table',
                'batch' => 0,
            ),
            14 => 
            array (
                'migration' => '2018_05_10_123022_create_routes_table',
                'batch' => 0,
            ),
            15 => 
            array (
                'migration' => '2018_05_10_123022_create_settings_table',
                'batch' => 0,
            ),
            16 => 
            array (
                'migration' => '2018_05_10_123022_create_user_has_permissions_table',
                'batch' => 0,
            ),
            17 => 
            array (
                'migration' => '2018_05_10_123022_create_user_has_roles_table',
                'batch' => 0,
            ),
            18 => 
            array (
                'migration' => '2018_05_10_123022_create_users_table',
                'batch' => 0,
            ),
            19 => 
            array (
                'migration' => '2018_05_10_123024_add_foreign_keys_to_ads_table',
                'batch' => 0,
            ),
            20 => 
            array (
                'migration' => '2018_05_10_123024_add_foreign_keys_to_msisdns_table',
                'batch' => 0,
            ),
            21 => 
            array (
                'migration' => '2018_05_10_123024_add_foreign_keys_to_posts_table',
                'batch' => 0,
            ),
            22 => 
            array (
                'migration' => '2018_05_10_123024_add_foreign_keys_to_products_table',
                'batch' => 0,
            ),
            23 => 
            array (
                'migration' => '2018_05_10_123024_add_foreign_keys_to_role_has_permissions_table',
                'batch' => 0,
            ),
            24 => 
            array (
                'migration' => '2018_05_10_123024_add_foreign_keys_to_role_route_table',
                'batch' => 0,
            ),
            25 => 
            array (
                'migration' => '2018_05_10_123024_add_foreign_keys_to_user_has_permissions_table',
                'batch' => 0,
            ),
            26 => 
            array (
                'migration' => '2018_05_10_123024_add_foreign_keys_to_user_has_roles_table',
                'batch' => 0,
            ),
        ));
        
        
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call('UserTableSeeder');

        Model::reguard();
      //  $this->call('BrandsTableSeeder');
       // $this->call('UsersTableSeeder');
    //    $this->call('RolesTableSeeder');
     //   $this->call('PermissionsTableSeeder');
     //   $this->call('UserHasPermissionsTableSeeder');
     //   $this->call('UserHasRolesTableSeeder');
    //    $this->call('RoleHasPermissionsTableSeeder');
     //   $this->call('PasswordResetsTableSeeder');
      //  $this->call('CategoriesTableSeeder');
       // $this->call('MigrationsTableSeeder');
     //   $this->call('OperatorsTableSeeder');
     //   $this->call('ProductsTableSeeder');
      //  $this->call('PostsTableSeeder');
        $this->call('SettingsTableSeeder');
        $this->call('RoutesTableSeeder');
     //   $this->call('AdsTableSeeder');
     //   $this->call('AdvertisingUrlsTableSeeder');
     //   $this->call('MsisdnsTableSeeder');
      //  $this->call('NotifyTableSeeder');
      //  $this->call('RoleRouteTableSeeder');
    }
}

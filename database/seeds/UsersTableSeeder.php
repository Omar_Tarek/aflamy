<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'super admin',
                'email' => 'emad@ivas.com.eg',
                'password' => '$2y$10$jtaQJWnfUWxx6mY45Gtkq.fimTUF7eLiihl07D8W49irdYmxF9TKi',
                'profile_img' => '',
                'remember_token' => NULL,
                'created_at' => '0000-00-00 00:00:00',
                'updated_at' => '2018-05-02 12:03:08',
            ),
        ));
        
        
    }
}

<?php

use Illuminate\Database\Seeder;

class OperatorsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('operators')->delete();
        
        \DB::table('operators')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Egypt',
                'operator_image' => 'operators_images/59ede6dcae8df.png',
                'created_at' => '2017-10-23 12:55:56',
                'updated_at' => '2017-10-23 12:55:56',
            ),
        ));
        
        
    }
}

<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('products')->delete();
        
        \DB::table('products')->insert(array (
            0 => 
            array (
                'id' => 2,
                'product_image' => 'products_images/59edeb3e400a6.jpg',
                'category_id' => 3,
                'brand_id' => 1,
                'created_at' => '2017-10-23 12:55:12',
                'updated_at' => '2017-10-23 13:19:55',
                'title' => 'video one',
                'video' => 'videos/59ede6b07a644.mp4',
            ),
            1 => 
            array (
                'id' => 3,
                'product_image' => 'products_images/59edeb5408a57.jpg',
                'category_id' => 1,
                'brand_id' => 1,
                'created_at' => '2017-10-23 13:15:00',
                'updated_at' => '2017-10-23 13:15:00',
                'title' => 'video 2',
                'video' => 'videos/59edeb5408f27.mp4',
            ),
            2 => 
            array (
                'id' => 4,
                'product_image' => 'products_images/59edf547bdc37.jpg',
                'category_id' => 1,
                'brand_id' => 1,
                'created_at' => '2017-10-23 13:15:30',
                'updated_at' => '2017-10-23 13:57:27',
                'title' => 'video 3',
                'video' => 'videos/59edeb7274609.mp4',
            ),
        ));
        
        
    }
}

<?php

use Illuminate\Database\Seeder;

class MsisdnsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('msisdns')->delete();
        
        \DB::table('msisdns')->insert(array (
            0 => 
            array (
                'id' => 7,
                'msisdn' => '96566782477',
                'operator_id' => 50,
                'ooredoo_notify_id' => 2,
                'ads_ur_id' => NULL,
                'transaction_id' => NULL,
                'ad_company' => 'DF',
                'final_status' => 1,
                'created_at' => '2018-05-10 14:13:46',
                'updated_at' => '2018-05-10 14:13:46',
            ),
            1 => 
            array (
                'id' => 8,
                'msisdn' => '96596565867860',
                'operator_id' => 50,
                'ooredoo_notify_id' => NULL,
                'ads_ur_id' => 88,
                'transaction_id' => NULL,
                'ad_company' => 'DF',
                'final_status' => NULL,
                'created_at' => '2018-05-10 14:15:26',
                'updated_at' => '2018-05-10 14:15:26',
            ),
        ));
        
        
    }
}

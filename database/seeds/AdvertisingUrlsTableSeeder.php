<?php

use Illuminate\Database\Seeder;

class AdvertisingUrlsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('advertising_urls')->delete();
        
        \DB::table('advertising_urls')->insert(array (
            0 => 
            array (
                'id' => 84,
                'adv_url' => '',
                'transaction_id' => '',
                'msisdn' => NULL,
                'operatorId' => NULL,
                'operatorName' => NULL,
                'status' => NULL,
                'created_at' => '2018-05-10 14:13:50',
                'updated_at' => '2018-05-10 14:13:50',
                'publisherId_macro' => '',
                'mobrain_token' => NULL,
                'ads_compnay_name' => 'DF',
            ),
            1 => 
            array (
                'id' => 85,
                'adv_url' => '',
                'transaction_id' => '',
                'msisdn' => NULL,
                'operatorId' => NULL,
                'operatorName' => NULL,
                'status' => NULL,
                'created_at' => '2018-05-10 14:13:54',
                'updated_at' => '2018-05-10 14:13:54',
                'publisherId_macro' => '',
                'mobrain_token' => NULL,
                'ads_compnay_name' => 'DF',
            ),
            2 => 
            array (
                'id' => 86,
                'adv_url' => '',
                'transaction_id' => '',
                'msisdn' => NULL,
                'operatorId' => NULL,
                'operatorName' => NULL,
                'status' => NULL,
                'created_at' => '2018-05-10 14:13:59',
                'updated_at' => '2018-05-10 14:13:59',
                'publisherId_macro' => '',
                'mobrain_token' => NULL,
                'ads_compnay_name' => 'DF',
            ),
            3 => 
            array (
                'id' => 87,
                'adv_url' => '',
                'transaction_id' => '',
                'msisdn' => NULL,
                'operatorId' => NULL,
                'operatorName' => NULL,
                'status' => NULL,
                'created_at' => '2018-05-10 14:14:53',
                'updated_at' => '2018-05-10 14:14:53',
                'publisherId_macro' => '',
                'mobrain_token' => NULL,
                'ads_compnay_name' => 'DF',
            ),
            4 => 
            array (
                'id' => 88,
                'adv_url' => '',
                'transaction_id' => '',
                'msisdn' => '96596565867860',
                'operatorId' => 50,
                'operatorName' => 'ooredoo_kuwait',
                'status' => NULL,
                'created_at' => '2018-05-10 14:15:26',
                'updated_at' => '2018-05-10 14:15:26',
                'publisherId_macro' => NULL,
                'mobrain_token' => NULL,
                'ads_compnay_name' => 'DF',
            ),
            5 => 
            array (
                'id' => 89,
                'adv_url' => '',
                'transaction_id' => '',
                'msisdn' => NULL,
                'operatorId' => NULL,
                'operatorName' => NULL,
                'status' => NULL,
                'created_at' => '2018-05-10 14:15:36',
                'updated_at' => '2018-05-10 14:15:36',
                'publisherId_macro' => '',
                'mobrain_token' => NULL,
                'ads_compnay_name' => 'DF',
            ),
            6 => 
            array (
                'id' => 90,
                'adv_url' => '',
                'transaction_id' => '',
                'msisdn' => NULL,
                'operatorId' => NULL,
                'operatorName' => NULL,
                'status' => NULL,
                'created_at' => '2018-05-10 14:15:56',
                'updated_at' => '2018-05-10 14:15:56',
                'publisherId_macro' => '',
                'mobrain_token' => NULL,
                'ads_compnay_name' => 'DF',
            ),
            7 => 
            array (
                'id' => 91,
                'adv_url' => '',
                'transaction_id' => '',
                'msisdn' => NULL,
                'operatorId' => NULL,
                'operatorName' => NULL,
                'status' => NULL,
                'created_at' => '2018-05-10 14:15:58',
                'updated_at' => '2018-05-10 14:15:58',
                'publisherId_macro' => '',
                'mobrain_token' => NULL,
                'ads_compnay_name' => 'DF',
            ),
        ));
        
        
    }
}
